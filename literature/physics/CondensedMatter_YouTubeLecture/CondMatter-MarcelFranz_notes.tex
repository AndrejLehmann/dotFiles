% Created 2021-05-19 Wed 23:43
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usemintedstyle{rainbow_dash}
\setcounter{secnumdepth}{2}
\usepackage{mathtools}
\author{Andrej Lehmann}
\date{\today}
\title{Notes to the Lecture of Condensed Matter by Marcel Franz (YouTube)}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Notes to the Lecture of Condensed Matter by Marcel Franz (YouTube)},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Lecture 2: Second Quantization}
\label{sec:orge90d3d1}
\subsection{Field Operators \& Matrix Elements of an Operator}
\label{sec:org2e7a0d1}
\begin{align*}
 \hat \psi(\bold{r}) & = \sum_b \psi_b(\bold{r}) c_b \\
 \hat \psi^{\dagger}(\bold{r}) & = \sum_a \psi_a(\bold{r}) c^{\dagger}_a \\
 \hat O & = \sum_{a,b} \langle a \vert O \vert b \rangle c_{a}^{\dagger} c_{b} = \int d^3r \; \hat \psi^{\dagger}(\bold{r}) O(\bold{r}) \hat \psi(\bold{r}) = \sum_{a,b} \int d^3r \; \psi_b(\bold{r}) O(\bold{r}) \psi_a(\bold{r}) c^{\dagger}_b c_a \\
 & \Rightarrow \int d^3r \; \psi_b(\bold{r})  O(\bold{r}) \psi_a(\bold{r}) = \langle a \vert O \vert b \rangle
\end{align*}

\subsection{Degenerate Electron Gas: Jellium}
\label{sec:org1185bc8}
Consider a cube of length \(L\) with a background of positive charge with density \(n(\bold{x})\), electrons inside at positions \(\bold{r}_i\), total zero charge of the system and periodic boundary conditions (\(k_i = \frac{2\pi}{L} n_i, \; n_i = 0, \pm 1, \pm 2, \dots\)).
In thermodynamic limit \(L \to \infty\) with density should remain the same.
\begin{align*}
& \hat H = \hat H_{e} + \hat H_b + \hat H_{e\text{-}b} \\
& \hat H_{e} = \sum_{i=1}^N \frac{p_i^2}{2m} + \frac{e^2}{2} \sum_{i \neq j} \frac{\mathrm e^{-\gamma | \bold{r_i} - \bold{r_j} | }}{| \bold{r}_{i} - \bold{r}_{i} |} \\
& \hat H_b = \frac{e}{2} \int \int d\bold{x} \; d\bold{x'} \frac{n(\bold{x}) n(\bold{x'}) \mathrm e^{-\eta | \bold{x} - \bold{x'} |}}{| \bold{x} - \bold{x'}| } \\
& \hat H_{e\text{-}b} = -e^2 \sum_{i=1}^N \int d\bold{x} \frac{ n(\bold{x}) \mathrm e^{-\gamma | \bold{x} - \bold{r_i} | }}{| \bold{x} - \bold{r}_{i} |}
\end{align*}
\(\hat H_b\) is the background.
The second sum in \(\hat H_{e}\) is the screened Coulomb (aka. Yukawa) potential.
It is used to avoid the divergences that arise for the bare Coulomb potential.
\(\gamma \to 0\) at the end of the calculation.
Similarly \(\mathrm e^{-\eta |\bold{r} - \bold{r'}|}\) is used in \(\hat H_b\)
We use plane waves as basis
\begin{align*}
\psi_{\bold{k}, \lambda}(\bold{x}) = \frac{1}{\sqrt{V}} \mathrm e^{i \bold{k} \bold{x}} \eta_{\lambda}, \; \eta_{\uparrow} = \binom{1}{0}, \; \eta_{\downarrow} = \binom{0}{1} \; .
\end{align*}
We assume constant background charge \(n(\bold{x})=\text{const}\) and calculate
\begin{align*}
    & \hat H_{b} = \frac{e^2}{2} \left( \frac{N}{V} \right)^2 \int \int d\bold{x} \, d\bold{x'} \frac{\mathrm e^{-\eta | \bold{x} - \bold{x'} |}}{| \bold{x} - \bold{x'}| } = \frac{e^{2}}{2} \frac{N^2}{V} \frac{4 \pi}{\gamma^2} \\
    & \hat H_{e\text{-}b} = -e^2 \left( \frac{N}{V} \right) \sum_{i=1}^N \int d\bold{x} \, \frac{\mathrm e^{-\gamma | \bold{x} - \bold{r_i} | }}{| \bold{x} - \bold{r}_{i} |} = -e^{2} \frac{N^2}{V} \frac{4 \pi}{\gamma^2} \\
    & \hat H = \hat H_{e} - \frac{e^{2}}{2} \frac{N^2}{V} \frac{4 \pi}{\gamma^2} \\
    & \langle \bold{k}_1 \lambda_1 \vert \hat T_{e} \vert \bold{k}_2 \lambda_2 \rangle = \frac{1}{2mV} \int d\bold{x} \, \mathrm e^{-i \bold{k}_1 \bold{x}} \eta^{\dagger}_{\lambda_1} (-\hbar^2 \nabla^2) \; \mathrm e^{i \bold{k}_2 \bold{x}} \eta_{\lambda_2} = \frac{\hbar^2 \, \bold{k}_2^2}{2mV} \; \delta_{\lambda_{1} \lambda_2} \int d\bold{x} \; \mathrm e^{i(\bold{k}_2 - \bold{k}_{1}) \, \bold{x}} \\
    & \;\;\qquad\qquad\qquad =  \frac{\hbar^2 \, \bold{k}_2^2}{2mV} \; \delta_{\lambda_{1} \lambda_2} \; \delta_{k_{1} k_2} \\
    & \hat T_{e} = \sum_{\bold{k}, \lambda} \frac{\hbar^2 \; \bold{k}^2 }{2m} \; c^{\dagger}_{\bold{k}, \lambda} \, c_{\bold{k}, \lambda}
\end{align*}
and similarly we can calculate \(\hat V\)
\begin{align*}
\hat V_{e} = \frac{e^2}{2V} \sum_{\bold{k}_1, \lambda_1, \bold{k}_2, \lambda_2, \atop \bold{k}_3, \lambda_3, \bold{k}_4, \lambda_4} \, \delta_{\lambda_1 \lambda_3} \, \delta_{\lambda_2 \lambda_4} \, \delta_{\bold{k}_1+\bold{k}_2, \bold{k}_3+\bold{k}_4} \frac{4\pi}{(\bold{k}_1 - \bold{k}_3)^2 + \gamma^2} \, c^{\dagger}_{\bold{k}_1\lambda_1} \, c^{\dagger}_{\bold{k}_2\lambda_2} \, c_{\bold{k}_4 \lambda_4} \, c_{\bold{k}_3 \lambda_3}
\end{align*}
where the \$\(\delta\)\$-terms represent conservations.
Note that the expressions for \(\hat H_b\) and \(\hat H_{e\text{-}b}\) are yet divergent for \(\gamma \to 0\).
Let's introduce new variables where conservations are more obvious \(\bold{k}_1 \to \bold{k}+\bold{q}\), \(\bold{k}_2 \to \bold{p}-\bold{q}\), \(\bold{k}_3 \to \bold{k}\), \(\bold{k}_4 \to \bold{p}\) and we also rename \(\lambda_1 \to \alpha\), \(\lambda_2 \to \beta\).
For the interaction term we then can rewrite as
\begin{align*}
\hat V_{e} = \frac{e^2}{2V} \sum_{\bold{k}, \bold{p}, \bold{q}} \frac{4\pi}{\bold{q}^2+\gamma^{2}} \; c^{\dagger}_{\bold{k}+\bold{q}, \alpha} \; c^{\dagger}_{\bold{p}-\bold{q},\beta} \; c_{\bold{p}\beta} \; c_{\bold{k}\alpha} \, .
\end{align*}
When \(\gamma \to 0\) \(\hat V\) diverges only for \(\bold{q} = 0\).
So we can put \(\gamma\) to 0 if we single out the terms with \(\bold{q} = 0\)
\begin{align*}
\hat V_{e} = \frac{e^2}{2V} \sum_{\bold{k}, \bold{p}, \bold{q} \neq 0, \atop \alpha, \beta} \frac{4\pi}{\bold{q}^2} \; c^{\dagger}_{\bold{k}+\bold{q}, \alpha} \; c^{\dagger}_{\bold{p}-\bold{q},\beta} \; c_{\bold{k},\beta} \; c_{\bold{p},\alpha} \; + \frac{e^2}{2V} \sum_{\bold{k}, \bold{p}, \atop \alpha, \beta} \frac{4\pi}{\gamma^{2}} \; c^{\dagger}_{\bold{k} \alpha} \; c^{\dagger}_{\bold{p}\beta} \; c_{\bold{p}\beta} \; c_{\bold{k}\alpha} \;.
\end{align*}
and we can rewrite the second sum using the commutation relations
\begin{align*}
\frac{e^2}{2V} \sum_{\bold{k}, \bold{p}, \atop \alpha, \beta} \frac{4\pi}{\gamma^{2}} \; c^{\dagger}_{\bold{k} \alpha} \; c^{\dagger}_{\bold{p},\beta} \; c_{\bold{p},\beta} \; c_{\bold{k},\alpha} = \frac{-e^2}{2V} \frac{4\pi}{\gamma^{2}} \sum_{\bold{k}, \bold{p}, \atop \alpha, \beta} \hat n_{\bold{k}\alpha} - \hat n_{\bold{k}\alpha} \hat n_{\bold{p}\beta} = \frac{e^2}{2V} \frac{4\pi}{\gamma^{2}} \hat N(\hat N - 1) \; .
\end{align*}
Note that \(\hat N\) is a good quantum number, since \([\hat H, \hat N]=0\) so \(N\) is conserved.
One can see that since \(\hat H\) does not change the total number of electrons.
So we can replace \(\hat N\) with \(N\).
And for the divergent terms we obtain
\begin{align*}
(\frac{e^2}{2} \frac{N^2}{V} \frac{4\pi}{\gamma^2} - \frac{e^2}{2} \frac{N}{V} \frac{4\pi}{\gamma^2}) - \frac{e^2}{2} \frac{N^2}{V} \frac{4\pi}{\gamma^2} = \frac{e^2}{2} \frac{N}{V} \frac{4\pi}{\gamma^2} \; .
\end{align*}
In order to consider the system in the thermodynamic limit \(V,\,N \to \infty\), we look at \(\hat H/{N}\) (since \(\hat H \to \infty\) in the thermodynamic limit).
And we see that the term with \(\gamma\) hat to become zero.
Finally for \(\hat H\) we obtain
\begin{align*}
    \hat H = \sum_{\bold{k}, \lambda} \frac{\hbar^2 \; \bold{k}^2 }{2m} \; c^{\dagger}_{\bold{k}, \lambda} \, c_{\bold{k}, \lambda} + \frac{e^2}{2V} \sum_{\bold{k}, \bold{p}, \bold{q} \neq 0, \atop \alpha, \beta} \frac{4\pi}{\bold{q}^2} \; c^{\dagger}_{\bold{k}+\bold{q}, \alpha} \; c^{\dagger}_{\bold{p}-\bold{q},\beta} \; c_{\bold{k},\beta} \; c_{\bold{p},\alpha} \; .
\end{align*}
The Interaction term can be depicted as a diagram with vertex \(4\pi/{\bold{q}^2}\), 2 incoming electrons with momentum \(\bold{k}\) and \(\bold{p}\) and 2 outgoing electrons with momentum \(\bold{k}+\bold{q}\) and \(\bold{p}-\bold{q}\).
Lets introduce dimensionless variables.
First we define average distance between electrons \(\bold{r}_0\): \(V/{N}=4\pi/{3}\cdot \bold{r}_0^3 \Leftrightarrow \bold{r}_0 = (3/{4\pi} \cdot V/{N})^{1/{3}}\)
And recall Bohr radius \(a_0 = \hbar^2/{m\,e^2} \approx 0.5 \mathrm{A}^{\circ}\).
So we can define dimensionless distance \(\bold{r}_s = \bold{r}_0/a_0\) which values for metals are empirically determined to be typically from 2 to 6.
With \(\bold{r}_0\) we can define dimensionless volume \(\bar V = V/{\bold{r}_0^3\) and dimensionless momenta \(\bar \bold{k} = \bold{r}_0 \bold{k}\)
\end{document}
