%\documentclass[12pt,twocolumn,aps,pra,superscriptaddress,floatfix,showpacs,longbibliography]{revtex4-1}
\documentclass[12pt]{article}
\usepackage[onehalfspacing]{setspace}

%%% packages
\usepackage{lmodern} % removes font size restrictions http://ctan.org/pkg/lm
\usepackage{physics}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{txfonts}
\usepackage{lipsum}
\usepackage{color}
\usepackage{wasysym}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{appendix}
\usepackage{etoolbox} % for additional refs as footnotes



%%% for diagrams
%\usepackage{tikz}
%\usetikzlibrary{calc}
%\usetikzlibrary{decorations.pathmorphing}
%\usetikzlibrary{decorations.pathreplacing}
%\usetikzlibrary{decorations.markings}
%\usetikzlibrary{shapes.geometric}
%\usetikzlibrary{positioning}
%\usetikzlibrary{fit}
%\usetikzlibrary{snakes}
%\usetikzlibrary{arrows}
%\usetikzlibrary{3d}



\usepackage{mathrsfs} %curly letters by \mathscr{}

%%% more readable commands
\newcommand{\pr}[1]{\!\left(#1\right)}
\newcommand{\of}[1]{\!\left(#1\right)}
\newcommand{\sbr}[1]{\!\left[#1\right]}
\newcommand{\cbr}[1]{\!\left{#1\right}}
\newcommand{\e}{\mathrm{e}} % to indicate e is const
\renewcommand{\i}{\mathrm{i}} % to indicate e is const


\begin{document}

\title{\textbf{Notes on The Theory of Brillouin Zones and Electronic States in Crystals by H.Jones}}
\author{Andrej Lehmann}
\date{\today}
\maketitle
\clearpage


\section{Introduction}

\subsection{The Origin of the Brillouin Zone Concept}

The central problem of Brillouin-zone theory was not the construction
(it follows immediately from the Bravais lattice),
but the determination of the energy and the wave function at each point of momentum space within the zone.
Bloch in his paper used linear combination of atomic wave functions aka tight-binding approximation.



\subsection{The Single Electron Model}

\textbf{Theory in this book:} stationary states of a single electron
,i.e. a set of stationary states for any single electron
while all electrons are distributed amongst these states according to Fermi-Dirac statistics.
The individual states are determined self consistently in the Hartree sense
so as to be compatible with the final distribution of all electrons.
So the interaction is accounted for in an average way.
This is similar to the model of an atom, where electrons are distributed amongst a set of hydrogen atom states (also states of a single electron).
In the metal theory the exchange and correlation interactions are neglected,
analog to exchange and configuration interaction in a atom.
Still the classification of states is exact.
Also the theory provides a picture in terms of which experimental results are interpreted successfully.



\section{The One-Dimentional Periodic Potential}

\subsection{Solutions of the Schroedinger Equation}\label{SolSchroedinger}

\textbf{Goal:} general properties of the wave function.\\
\\
To solve:
\begin{equation} \label{eq:Schroedinger}
  \dv[2]{\psi}{x} + \frac{2m}{\hbar^{2} } \pr{\varepsilon - V\of{x}} \psi = 0
\end{equation}
\begin{equation}
  \pr{ x + n \, a  } = V\of{x}
\end{equation}
So basically we have a problem of the form
\begin{equation}\label{eq:toSolve}
  \dv[2]{\psi}{x} + f \of{x} \psi = 0 \, , \;\;
  f \pr{x + n a } = f \of{x}
\end{equation}
It's second order linear equation, so there are two independent solutions $\psi_{1} \of{x} \, , \psi_{2} \of{x}$
and the general solution is a linear combination of these.
The periodicity of $f\of{x} $ implies: if $ \psi\of{x} $ solves \eqref{eq:toSolve} then $ \psi\of{ x + a } $ also solves \eqref{eq:toSolve} (as can be seen by substituting $ x + a  = y $).
And since any solution of \eqref{eq:toSolve} can be expressed as a linear combination of $ \psi_{1} $ and $ \psi_{2} $
\begin{align}
  \psi_{1}\of{x + a} &= A \, \psi_{1}\of{x} + B \, \psi_{2}\of{x} \\
  \psi_{2}\of{x + a} &= C \, \psi_{1}\of{x} + D \, \psi_{2}\of{x}
\end{align}
By $\psi_{1} \cdot$ \! \eqref{eq:toSolve} and $\psi_{2} \cdot$ \! \eqref{eq:toSolve}
\begin{align}
  \label{eq:interim1}
  \psi_{1} \dv[2]{\psi_{2} }{x} + f\of{x} \psi_{1} \psi_{2} &= 0 \\
  \label{eq:interim2}
  \psi_{2} \dv[2]{\psi_{1} }{x} + f\of{x} \psi_{2} \psi_{1} &= 0
\end{align}
\eqref{eq:interim1} $-$ \eqref{eq:interim2} yields
\begin{equation}\label{eq:interim3}
  \psi_{1} \dv[2]{\psi_{2} }{x} - \psi_{2} \dv[2]{\psi_{1} }{x} = 0 \,
\end{equation}
Integrating \eqref{eq:interim3} by parts it is found that the determinant
\begin{equation*}\label{eq:det}
  W_{\psi_{1} \psi_{2}}\of{x}  = \mqty| \psi_{1} & \psi_{2} \\ \, \psi_{1}' & \psi_{2}' \, | = \text{const}
\end{equation*}
\begin{align*}
  &\text{const} = W_{\psi_{1} \psi_{2}}\of{x} = W_{\psi_{1} \psi_{2}}\of{x + a} \\
    &= \mqty|\, A \, \psi_{1} + B \, \psi_{2} & C \, \psi_{1} + D \, \psi_{2} \\ \, A \, \psi_{1}' + B \, \psi_{2}' & C \, \psi_{1}' + D \, \psi_{2}' \,| \\
    &= \mqty|\, \psi_{1} & \psi_{2} \\ \psi_{1}' & \psi_{2}' \,| \cdot \mqty|\,A & C \\ B & C\,| = W_{\psi_{1} \psi_{2}}\of{x} \\
    &= \mqty|\, \psi_{1} & \psi_{2} \\ \psi_{1}' & \psi_{2}' \,| \\
\end{align*}
So
\begin{equation}
  \label{eq:interm4}
  A\,D - B\,C = 1
\end{equation}
Now the Ansatz $ \psi\of{x + a} = \lambda \, \psi\of{x} $ gives
\begin{align}
  \label{eq:interim3}
  A \, \psi_{1} + B \, \psi_{2} = \lambda \, \psi_{1} \\
  \label{eq:interim4}
  C \, \psi_{1} + D \, \psi_{2} = \lambda \, \psi_{2}
\end{align}
Solving e.g. \eqref{eq:interim3} for $ \psi_{2} $, inserting it in \eqref{eq:interim4} we find
\begin{equation*}
  \mqty|\, A - \lambda & B \\ C & D - \lambda \,| = 0
\end{equation*}
then using \eqref{eq:interim4}
\begin{equation*}
  \lambda^2 - \of{A + D} \lambda + 1 = 0
\end{equation*}
and expressing $ \lambda = \e^{\pm \alpha} $ leads to
\begin{equation*}
  \frac{\e^{\,\alpha} + \e^{-\alpha}}{2} = \cosh(\alpha) = \frac{1}{2} \pr{A + D}\,.
\end{equation*}
Now we can see that if $ \frac{1}{2} \pr{A+D} > 1 $ then $ \alpha \in \mathbb{R} $
and the independent solutions are
\begin{equation}
 \label{eq:solUnbounded}
 \begin{aligned}
  \psi_{1}\of{x + a} &= \e^{\,\alpha} \psi_{1}\of{x} \\
  \psi_{2}\of{x + a} &= \e^{-\alpha} \psi_{2}\of{x}
\end{aligned}
\end{equation}

On the other hand if $ \frac{1}{2} \pr{A+D} < 1 $ then $ \alpha$ is imaginary
and on writing $ \alpha = ika$ with $ k \in \mathbb{R} $ the independent solutions are
\begin{equation}
 \label{eq:solBounded}
 \begin{aligned}
  \psi_{1}\of{x + a} &= \e^{\,ika} \psi_{1}\of{x} \\
  \psi_{2}\of{x + a} &= \e^{-ika} \psi_{2}\of{x}
\end{aligned}
\end{equation}
Now we see there are two types of solutions for \eqref{eq:toSolve}
unbounded \eqref{eq:solUnbounded} and bounded for all $x$ \eqref{eq:solBounded}.
The value of $\varepsilon$ in $\,\pr{\varepsilon - V\of{x}}$ determines the type of the solution
(as can be seen e.g. for $\,f\of{x} = c^2\,$ and $\psi_{1,2} = \e^{ \pm \i cx}$ vs $\,f\of{x} = -c^2$ and $\psi_{1,2} = \e^{ \pm cx}$, $\,c = \text{const}$)
But only the solution of type \eqref{eq:solBounded} can be considered as wave functions with physical meaning.
\eqref{eq:solBounded} can be equivalently written as
\begin{equation}\label{eq:Bloch}
  \begin{aligned}
    \psi_{1} \of{x} &= \e^{ikx}\,u_{1} \of{x} ,\, &u_{1}\of{x+na} &= u_{1}\of{x}\\
    \psi_{2} \of{x} &= \e^{-ikx}\,u_{2} \of{x} ,\, &u_{2}\of{x+na} &= u_{2}\of{x}
  \end{aligned}
\end{equation}
\textbf{Conclusion:} states of a particle in a periodic field of indefinite extension have wave functions of type \eqref{eq:Bloch}
and for certain values of $ \varepsilon$ no states can occur.
The constant $k$ is called wave number since it is $2\pi$ times of reciprocal of a wave length
(periodic boundary conditions\big[\ref{appx:periodicBoundCond}\big] imply $k = \frac{2\pi\,n}{N a}$).



\subsection{The Relation between the Eigenvalues and the Wave-Number}

Choose the origin such that $ V\of{-x} = V\of{x} $.
So the two solutions of \eqref{eq:Schroedinger} are symmetric $ p_{1}\of{x} $ and antisymmetric $ p_{2} \of{x} $\big[\ref{appx:solWithEvenPot}\big], which are orthogonal and lineary independent.
A general solution $ \psi\of{x} $ is then a linear combination of them
\begin{equation}\label{eq:solSymPot}
  \psi\of{x} = A p_{1}\of{x} + B p_{2}\of{x} \,.
\end{equation}
Boundary conditions \eqref{eq:solBounded} must apply.
So
\begin{equation}\label{eq:interim5}
\begin{aligned}
  \psi\of{a/2} &= \e^{\i ka} \psi\of{-a/2} \\
  \psi'\of{a/2} &= \e^{\i ka} \psi'\of{-a/2}
\end{aligned}
\end{equation}
Applying this to \eqref{eq:solSymPot},
using the symmetry properties
\begin{equation}\label{eq:symp1p2}
\begin{aligned}
  p_{1} \of{x} &= p_{1}\of{-x}, \;\;\; p_{1}'\of{x}  = -p_{1}'\of{-x}, \\
  p_{2} \of{x} &= -p_{2}\of{-x}, \, p_{2}'\of{x} = p_{2}'\of{-x}
\end{aligned}
\end{equation}
and eliminating $ A$ and $ B$
we get
\begin{equation}
\pr{1 - \e^{ \i ka}}^2 p_{1}\of{a/2} p_{2}'\of{a/2}  = \pr{1 + \e^{ \i ka}}^2 p_{2} \of{a/2} p_{1}'\of{a/2} \, .
\end{equation}
Now using $ W_{p_{1} p_{2}} \of{x}= p_{1}\of{x} p_{2}'\of{x} - p_{2}\of{x} p_{1}'\of{x} $ and with writing short $ p_{1} \of{a/2} p_{2}' \of{a/2} = p_{1} p_{2}' $, $ p_{2} \of{a/2} p_{1}' \of{a/2} = p_{2} p_{1}' $  we get
\begin{equation*}
  W_{p_{1} p_{2}} = 2 \cdot \e^{ \i ka} \, \sbr{ p_{2} p_{1}' + p_{1} p_{2}' } + \e^{ \i ka} - W_{p_{1} p_{2}}
\end{equation*}
and deviding by $ W_{p_{1} p_{2}} \e^{ \i ka} $
\begin{equation*}
\begin{aligned}
  \e^{ \i ka} - \e^{-\i ka}  = 2 \cdot \frac{ \sbr{p_{2} p_{1}' + p_{1} p_{2}' } }{W_{p_{1} p_{2}}} &= 2 \cdot \frac{ p_{1} p_{2}' - p_{2} p_{1}' + 2 p_{2} p_{1}' }{W_{p_{1} p_{2}}} = 2 \cdot \sbr{1 + \frac{2 p_{2} p_{1}'}{W_{p_{1} p_{2}}}} \\
                                                                                                             &= 2 \cdot \frac{p_{2} p_{1}' - p_{1} p_{2}' + 2 p_{1} p_{2}'}{W_{p_{1} p_{2}}} = 2 \cdot \sbr{-1 + \frac{2 p_{1} p_{2}'}{W_{p_{1} p_{2}}}} \;.
\end{aligned}
\end{equation*}
So finally we get the relation
\begin{equation}\label{eq:relationEpsK}
  \cos{\of{k a} } = 1 + \frac{2}{W} p_{2} \of{a/2} p_{1}'\of{a/2} = -1 + \frac{2}{W_{p_{1} p_{2}}} p_{1}\of{a/2} p_{2}'\of{a/2} \, .
\end{equation}
For given $V\of{x}$ $p_{1}$ and $p_{2}$ are determined by $\varepsilon$ in the equation
\begin{equation}\label{eq:d2x2p12}
  p_{1,2}'' + \frac{2m}{\hbar^2} \pr{\varepsilon - V\of{x}} p_{1,2} = 0
\end{equation}
E.g. by expending the quantities
\begin{align*}
  V\of{x} &= V_{0} + \pr{\dv[2]{V}{x}_{0}} \frac{x^2}{2!} + ... \\
  p_{1}\of{x} &= 1 + \frac{1}{2} b x^2 + ... \\
  p_{2}\of{x} &= x + \frac{1}{3!} c x^3 + ...
\end{align*}
by \eqref{eq:d2x2p12} it follows
\begin{equation*}
  b\,\of{\varepsilon} = c\,\of{\varepsilon} = \frac{2m}{\hbar^2}\of{V_{0} - \varepsilon}
\end{equation*}
so $p_{1}\of{\varepsilon}$ and $p_{2}\of{\varepsilon}$.

\textbf{Conclusion:} Since $p_{1}$ and $p_{2}$ are determined by the value of $\varepsilon$ for a given $V\of{x}$, \eqref{eq:relationEpsK}, derived by setting the origin so that $V\of{x} = V\of{-x}$, is a relation between $\varepsilon$ and $k$,
which can be used to determine the eigenvalues.



\subsection{General Properties of the Eigenvalues and the Wave Functions}

For $k = 0$ by using \eqref{eq:symp1p2} we can find that
\begin{equation}\label{eq:Adp1dxBp2}
  A p_{1}'\of{a/2} = 0 \quad \text{and} \quad B p_{2}\of{a/2} = 0\,.
\end{equation}
So if $p_{2}\of{a/2} = 0$ and as in general $p_{1}'\of{a/2} \neq 0$, $A=0$ and $\psi$ is antisymmetrical.
Analogically if $p_{1}'\of{a/2} = 0$ and as in general $p_{2}\of{a/2} \neq 0$, $B = 0$ and $\psi$ is symmetrical.
Since the two different boundary conditions $p_{2}\of{a/2} = 0$ and $p_{1}'\of{a/2} = 0$ lead to different values of $\varepsilon$, at $k=0$ the states are non-degenerate and are either symmetrical or antisymmetrical.
But if the $V\of{x}$ is such (e.g. $V\of{x}$ = const) that $p_{1}'\of{a/2} = 0 = p_{2}\of{a/2}$, then $A \neq 0$, $B \neq 0$
and neither of the degenerate states are wholly symmetrical or antisymmetrical.
This is called accidental degeneracy.\\
Analogically for $k = \pm \pi/a$
\begin{equation}\label{eq:Ap1Bdp2dx}
  A p_{1}\of{a/2} = 0 \quad \text{and} \quad B p_{2}'\of{a/2} = 0
\end{equation}
Thus if $p_{1}\of{a/2} = 0$, $B=0$, $\psi$ is symmetrical and if $p_{2}'\of{a/2} = 0$, $A=0$, $ \psi$ is antisymmetrical.
Again the accidental degeneracy when $p_{1}\of{a/2} = 0 = p_{2}'\of{a/2}$ is possible.\\
\textbf{Conclusion:} Apart from accidental degeneracy at $k = 0$ and $k = \pm \pi/a$ all states are in general non-degenerate and at each point either symmetrical or antisymmetrical.\\
Furthermore for $k a \neq 0,2 \pi , \pm \pi$ we can see by rewriting \eqref{eq:relationEpsK} to
\begin{align*}
  \frac{W_{p_{1} p_{2}}}{2} \sbr{ \cos{ka} - 1} &= p_{2}\of{a/2} p_{1}'\of{a/2} \;\; \pr{\;= 0 \;\text{for}\; ka = 0, 2 \pi \, }\\
  \frac{W_{p_{1} p_{2}}}{2} \sbr{ \cos{ka} + 1} &= p_{1}\of{a/2} p_{2}'\of{a/2} \;\; \pr{\;= 0 \;\text{for}\; ka = \pm \pi  \, }
\end{align*}
non of $p_{1}\of{a/2}$, $p_{1}'\of{a/2}$, $p_{2}\of{a/2}$, $p_{2}'\of{a/2}$ can vanish.
Also by rewriting \eqref{eq:solSymPot} using \eqref{eq:interim5} and \eqref{eq:symp1p2} into
\begin{align}\label{eq:interim6}
  \frac{A}{B} \frac{p_{1}\of{a/2}}{p_{2}\of{a/2}} &= \frac{- \sbr{1 + \e^{\i ka}}}{ \sbr{1 - \e^{\i ka}}} = - \i \cot \pr{ka/2} \;\; \pr{\;= 0 \;\text{for}\; ka = 0, 2 \pi  \, }\\
  \frac{A}{B} \frac{p_{1}'\of{a/2}}{p_{2}'\of{a/2}} &= \frac{- \sbr{1 - \e^{\i ka}}}{ \sbr{1 + \e^{\i ka}}} = - \i \tan \pr{ka/2} \;\; \pr{\;= 0 \;\text{for}\; ka = 0, 2 \pi  \, }
\end{align}
we see that $A$ and $B$ also can not vanish.
Thus these states have no particular symmetry.\\
\textbf{Conclusion:} For $0 < \abs{k} < \pi/a$ we deduce that solutions \eqref{eq:solSymPot} have no particular symmetry.\\
Now we show that $ \varepsilon\of{k}$ has extrema only at the symmetry points $k = 0, \pm \pi /a$.
We start with the well known relation (no normalization) for determination of the average velocity in a state $k$
\begin{equation}\label{eq:depsdk}
  \dv{\varepsilon}{k} \int\limits_{-a/2}^{a/2} \abs{ \psi_{k}}^2 \mathop{d x} =
  \frac{\hbar^2}{2m \i} \int\limits_{-a/2}^{a/2} \pr{\psi^{*}_{k} \dv{\psi_{k}}{x} - \psi_{k} \dv{\psi^{*}_{k}}{x}} \mathop{d x} \;.
\end{equation}
With the ansatz \eqref{eq:solSymPot} we get
\begin{equation*}
  \psi^{*}_{k} \dv{\psi_{k}}{x} - \psi_{k} \dv{\psi^{*}_{k}}{x} = \pr{A^{*}B - B^{*}A}W_{p_{1} p_{2}} \; .
\end{equation*}
And from \eqref{eq:interim6} we can see $A/B$ is pure imaginary so $A^{*}B = -B^{*}A$.
Thus \eqref{eq:depsdk} can be rewritten as
\begin{equation*}
  \dv{\varepsilon}{k} = \frac{a \hbar^2}{m \i} W_{p_{1} p_{2}} \, \Biggm/ \int\limits_{-a/2}^{a/2} \pr{ \abs{A}^2 p_{1}^2 + \abs{B}^2 p_{2}^2} \mathop{d x} \; .
\end{equation*}
And since it has been shown above that for $ 0 < \abs{k} < \pi /a$ \, $A,B \neq 0$,
we can see that there is no extrema in this range. \\
\textbf{Conclusion:} Since extrema are only at the band-centre and the band-edges,
in a ``one-dimensional semi-conductor'' holes and electrons (excitations) can only form at the centre or the edge of the band.









\clearpage
\appendix
\appendixpage
\addappheadtotoc

\section{Periodic Boundary Conditions}\label{appx:periodicBoundCond}

``We adopt this (periodic) boundary condition under the assumption that the bulk properties of the solid will not depend on the choice of boundary condition,
which can therefore be dictated by analytical convenience''\cite{AshcroftMermin}

``If the crystal is very large, we expect the precise form of these (boundary conditions) not to effect the physical description of properties over the bulk of the crystal.
We may then choose (boundary) conditions which are most simple mathematically.
These are the ‘cyclic’ or ‘periodic’ boundary conditions...'' \cite{JonesMarch}



\section{Solutions to a Schroedinger Equation with even Potential}\label{appx:solWithEvenPot}

The Schroedinger equation \eqref{eq:Schroedinger} with $ V\of{x} = V\of{-x} $ is satisfied by $ \alpha \psi\of{x} $ and $ \alpha \psi\of{-x}  $.
Normalization of $ \psi$ requires $ \abs{ \alpha } = 1 $.
So $ \alpha = \pm 1$ and $ \psi\of{x} = \pm \psi\of{-x} $.

\bibliographystyle{plain}
\bibliography{BZ_HJones__notes}


\end{document}
