#+TITLE: Collective charge Excitations of strongly correlated electrons, vertex corrections, and gauge invariance - NOTES
#+STARTUP: overview
#+STARTUP: latexpreview
#+TAGS: collectiveChargeExcitations longWavelengthChargeExcitations susceptibility WardIdentity zeroSoundMode plasmons

* Calculation Procedure for Charge Susceptibility
The goal is to calculate charge response via charge susceptibility $\chi_{\omega}(\bold{q}) \coloneqq - \langle n_{\omega}(\bold{q}) n_{\omega}(\bold{q}) \rangle$ .

| quantity| description                 |
|---------+-----------------------------|
| $\chi^{0}_{\nu \omega}(\bold{q})$ | bubble                      |
| $\chi^0_{\nu \omega}$    | impurity bubble             |
| $\widetilde{\chi}^{\;0}_{\nu \omega}(\bold{q})$ | nonlocal part of the bubble |
| $\Gamma^{-1}_{\omega}(\bold{q})$ | reducible vertex function   |
| $\chi_{\omega}(\bold{q})$  | charge susceptibility       |

\begin{align*}
&\chi^{0}_{\nu \omega}(\bold{q}) = \frac{1}{N_k} \sum_{\bold{k}} G_{\nu+\omega}(\bold{k} + \bold{q}) \; G_{\nu}(\bold{k}) \\
&\chi^0_{\nu \omega} = g_{\nu} g_{\nu + \omega} \\
&\widetilde{\chi}^{\;0}_{\nu \omega}(\bold{q}) = \chi^0_{\nu \omega}(\bold{q}) - \chi^0_{\nu \omega} \\
&\left[ \Gamma^{-1}_{\omega}(\bold{q}) \right]_{\nu \nu'} = \left[ \gamma^{-1} \right]_{\nu \nu'} + T \; \widetilde{\chi}^{\;0}_{\nu \omega}(\bold{q}) \; \delta_{\nu \nu'} \\
&\chi_{\omega}(\bold{q}) = 2 T \sum_{\nu} \chi^0_{\nu \omega}(\bold{q}) - 2T \sum_{\nu \nu'} \chi^0_{\nu \omega}(\bold{q}) \; \Gamma_{\nu \nu'}(\bold{q}) \; \chi^0_{\nu \omega}(\bold{q})
\end{align*}
It is advantageous that the calculation of $\Gamma_{\omega}(\bold{q})$ does not require the calculation of the irreducible impurity vertex $\gamma^{\text{irr}}$ , like in the formulation $\Gamma_{\nu \nu'}(\bold{q}) = \Gamma^{\text{irr}_{\nu \nu' \omega}} - T \sum_{\nu''} \Gamma_{\nu \nu'' \omega} \; \chi^0_{\nu'' \omega}(\bold{q}) \; \Gamma_{\nu \nu'}(\bold{q})$ with the solution $\left[ \Gamma^{-1}_{\omega}(\bold{q}) \right]_{\nu \nu'} = \left[ \Gamma^{\text{\,irr} \; -1} \right]_{\nu \nu'} + T \; \chi^{\;0}_{\nu \omega}(\bold{q}) \; \delta_{\nu \nu'}$ where in DMFT $\Gamma^{\text{\,irr}} = \gamma^{\text{\,irr}}$.
This avoids unphysical singularities in the low-frequency behavior of $\gamma^{\text{irr}}$ , which occur in the proximity of the metal-insulator transition.
