%\documentclass[12pt,twocolumn,aps,pra,superscriptaddress,floatfix,showpacs,longbibliography]{revtex4-1}
\documentclass[12pt]{article}
\usepackage[onehalfspacing]{setspace}

%%% packages
\usepackage{lmodern} % removes font size restrictions http://ctan.org/pkg/lm
\usepackage{physics}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{txfonts}
\usepackage{lipsum}
\usepackage{color}
\usepackage{wasysym}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{appendix}
\usepackage{etoolbox} % for additional refs as footnotes
\usepackage{enumitem}
\usepackage{mathrsfs} %curly letters by \mathscr{}



%%% diagrams
\usepackage{tikz}
\usetikzlibrary{arrows ,automata ,positioning}
%\usetikzlibrary{calc}
%\usetikzlibrary{decorations.pathmorphing}
%\usetikzlibrary{decorations.pathreplacing}
%\usetikzlibrary{decorations.markings}
%\usetikzlibrary{shapes.geometric}
%\usetikzlibrary{positioning}
%\usetikzlibrary{fit}
%\usetikzlibrary{snakes}
%\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
%\usetikzlibrary{3d}



%%% more readable commands
\newcommand{\pr}[1]{\!\left(#1\right)}
\newcommand{\of}[1]{\!\left(#1\right)}
\newcommand{\sbr}[1]{\!\left[#1\right]}
\newcommand{\cbr}[1]{\!\{#1\}}
\newcommand{\e}{\mathrm{e}} % to indicate e is const
\renewcommand{\i}{\mathrm{i}} % to indicate e is const


\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section] % theorem numbering depend on section
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition} % definition numbering depend on theorem numbers
\newtheorem{exmp}[thm]{Example} % example numbering depend on theorem numbers


\setlist{nosep}



\begin{document}

\title{\textbf{Notes on Introduction of the Theory of Computation by Michael Sipser}}
\author{Andrej Lehmann}
\date{\today}
\maketitle
\clearpage


\section{Preface}

\subsection{To the Student}

\begin{tabular}{ l | l}
  \textbf{theory} & \textbf{practical example} \\ \hline
  grammar  & Designing a new programming language \\
  finite automata, regular expressions & string searching and pattern matching \\
  NP-completeness & computation time
\end{tabular} \\ \\
Example of a big question: \emph{How to factorize a big number?}
No answer for all cases within the lifetime of the universe!
Important for cryptography.


\section{Introduction}

\subsection{Automata, Computability, and Complexity}

This 3 areas is the focus of this book.
They are linked by the question:
\emph{What are the fundamentals capabilities and limitations of computer?}
This question is interpreted differently in each area.

\subsubsection{Complexity Theory}

Easy problem: sorting.\\
Hard problem: Scheduling (e.g. no two classes take place in the same room at the same time). May require centuries of computation time for thousand classes.

\emph{What make some problems computationally hard and others easy?}\\
Schemes for classifying problems according to their computational difficulty.
Using this scheme, we can demonstrate a method for giving evidence that certain problems are computationally hard, even if we are unable to prove that they are.\\ \\
Strategies:\\
a) Find which aspect of the problem is at the root of the difficulty.
Alter it so that the problem is more easily solvable.\\
b) Settle for approximate solution.\\
c) Is the problem hard only in the worst case situation?
Then settle with a solution that is only slow occasionally.\\
d) Randomized computation.\\

Cryptography: requires computational problems that are hard, rather than easy.



\subsubsection{Computability Theory}

In the first half of the 20. century, Kurt Goedel, Alan Turing and Alonzo Church discovered that certain basic problems cannot be solved by computers.
e.g.: Determine whether a mathematical statement is true or false.
This result came as a surprise since a computer lies strictly within the realm of mathematics.
Development of theoretical models of computers helped to construct actual computers.

Computability and complexity are closely related.
Classification of problems as solvable or not vs. classification as easy or hard.



\subsubsection{Automata Theory}

Definition and properties of mathematical models of computation.
E.g. \emph{finite automaton} is used in text processing, compilers, and hardware design.
Or \emph{context-free grammar} is used in programming languages and artificial intelligence.

Precise definition of a \emph{computer} is required.
Concepts relevant to other nontheoretical areas of computer science.

\section{Regular Languages}

Central question for basis: \emph{What is a computer?}\\
Simples model of a computer: \textbf{finite automaton}.



\subsection{Finite Automata}

Good models for computer with an extremely limited amount of memory.
Example: controller for an automatic door, elevator.\\
An automaton can be represented as a graph: Nodes = states, edges = inputs for transitions.
E.g. of an automatic door controller in Fig.\ref{fig:autoDoorGraph}.
\begin{figure}\label{fig:autoDoorGraph}
\centering
%\resizebox{250.0pt}{!}{
\begin{tikzpicture}
\begin{scope}[every node/.style={circle,thick,draw,minimum size = 2.1cm}]
    \node (C) at (0,0) {CLOSED};
    \node (O) at (5,0) {OPEN};
\end{scope}
\begin{scope}[>={Stealth[black]},
              every node/.style={fill=white,rectangle,align=center},
              every edge/.style={draw=black,very thick}]
    \path [->] (O) edge[bend right=30] node {neither} (C);
    \path [->] (C) edge[bend right=30] node {front} (O);
    \tikzstyle{LabelStyle}=[above left]
    \path [->] (C) edge[loop above] node[left=12.0pt] {rear \\ both \\ neither} (C);
    \path [->] (O) edge[loop above] node[right=12.0pt] {rear \\ both \\ front} (O);
\end{scope}
\end{tikzpicture}
%}
\caption{State graph for an automatic door controller.}
\end{figure}
The notation front, rear, both, neighter denotes whether a person is standing in front or rear of the door. \\
Probabilistic counterpart: \textbf{Markov chains}.



\subsubsection{Formal Definition of a Finite Automaton}

\begin{defn}\label{def:finiteAutomaton}
  A \emph{\textbf{finite automaton}} is a 5-tuple $ \pr{Q, \Sigma, \delta, q_{0}, F}$, where
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $Q$ is a finite set called the \emph{\textbf{states}},
    \item $ \Sigma$ is a finite set called the \emph{\textbf{alphabet}},
    \item $ \delta : Q \times \Sigma \rightarrow Q$ is the \emph{\textbf{transition}},
    \item $q_{0} \in Q$ is the \emph{\textbf{start state}},
    \item $F \subseteq Q$ is the \emph{\textbf{set of accept states}} (\emph{\textbf{final states}}).
  \end{enumerate}
\end{defn}
\begin{defn}\label{def:language}
  \emph{A} is the \emph{\textbf{language}} \emph{L} \emph{\textbf{ of machine M}} if \emph{A} is the set of all strings that machine M accepts, $L\of{M} = A$.
\end{defn}
We say ``A machine recognizes the language'' and ``A machine accepts the string'',
since a machine recognizes only one language and accepts several strings.
If the machine accepts no strings, it still recognizes one language,
the empty language $\varnothing$.



\subsubsection{Formal Definition of Computation}

\begin{defn}
  Let $M = \pr{Q, \Sigma, \delta , q_{0}, F}$ be a finite automaton and let $w = w_{1} w_{2}...w_{n}$ be a string where $ w_{\i} \in \Sigma$. Then $M$ \emph{\textbf{accepts}} $w$ if a sequence $r_{0}, r_{1},...,r_{n} \in Q$ exists with:
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $r_{0} = q_{0}$ (start in the start state)
    \item $ \delta\of{r_{\i}, w_{i+1}} = r_{i+1}$, for $i = 0,...,n-1$ (transitions)
    \item $r_{n} \in F$ (ends in accept state)
  \end{enumerate}
  M \emph{\textbf{recognizes language}} $A$ if $A = \{w \, | \text{ $M$ accepts $w$}\}$.
\end{defn}
\begin{defn}\label{def:regLang}
  A language is called a \emph{\textbf{regular language}} if some finite automaton recognizes it.
\end{defn}



\subsubsection{Designing Finite Automata}

Put yourself in the place of the machine you are trying to design (reader as automaton).
Figure out what you need to remember about the string as you are reading it.
Remember all of string you receive it impossible,
since you have a limited memory and the input can be very long.\\
E.g.: $ \Sigma = \{0,1\}\,$, $A = \{w\,|\,\text{number of 1 is odd}\}$.
Don't remember all 1s, but just whether the number of 1s is currently even or odd.
So the necessary informations are:
\begin{enumerate}[label=\textbf{\arabic*}.]
  \item even so far
  \item odd so far
\end{enumerate}
Now assign a state to each of this possibilities
and accordingly the transitions.
And the result is the automaton in \ref{fig:automatonOdd1s}.

\begin{figure}\label{fig:automatonOdd1s}
\centering
%\resizebox{250.0pt}{!}{
\begin{tikzpicture}
\begin{scope}[every node/.style={circle,thick,draw,minimum size = 1.5cm}]
    \node (even)[minimum size=1.9cm] at (0,0) {even};
    \node (odd) at (5,0) {odd};
    \node (odd2)[minimum size=1.9cm] at (5,0) {};
\end{scope}
\begin{scope}[>={Stealth[black]},
              every node/.style={fill=white,rectangle,align=center},
              every edge/.style={draw=black,very thick}]
    \path [->] (even) edge[bend right=30] node {1} (odd);
    \path [->] (odd) edge[bend right=30] node {1} (even);
    \tikzstyle{LabelStyle}=[above left]
    \path [->] (odd2) edge[loop above] node[left=12.0pt] {0} (odd2);
    \path [->] (even) edge[loop above] node[right=12.0pt] {0} (even);
\end{scope}
\end{tikzpicture}
%}
\caption{Automaton that recognized the language of odd 1s.}
\end{figure}



\subsubsection{The Regular Operations}

In the theory of computation the objects are languages
and regular Operation are Operation to manipulate them.
\begin{defn}\label{def:operLang}
  Let $A$ and $B$ be languages.
  \begin{itemize}
    \item \textbf{Union:} $A \cup B = \{x \,|\, x\in A \text{or} \in B \}$
    \item \textbf{Concatenation:} $A \circ B = \{xy \,|\, x \in A \,\text{and}\, y \in B\}$
    \item \textbf{Star:} $A^{*} = \{x_{1} x_{2} \dots x_{k} \,|\, k \geq 0 \,\text{and each}\, x_{\i} \in A \}$ \\
      Attaches any number of strings together. ``Any number'' also includes the empty string $\varepsilon$. \, E.g.: \,$A = \{a, b\}$, \, $A^{*} = \{ \varepsilon, a, b, aa, ab, ba, bb, aaa, \dots\}$.
    \end{itemize}
\end{defn}
\begin{defn}
  A set is \textbf{closed} under an operation if performance of that operation on members of the set always produces a member of that set.
\end{defn}
\begin{thm}\label{thm:regLangClosed}
  The class of \textbf{regular languages} is \textbf{closed} under the operations \ref{def:operLang}.
\end{thm}
For the proof of \ref{thm:regLangClosed} see \cite{sipserIntroTheoComp} (Regular Operations).



\subsection{Nondeterminism}

DFA every state has \emph{exactly} one transition arrow for each symbol in the alphabet.
Furthermore an NFA can have an arrow with $ \varepsilon $ as a label.
When processing multiple possible transitions,
computation of NFA is carried out by splitting the NFA in accordingly multiple copies
and following all possibilities in parallel.
When the transition can not be processed, the copy dies along with the branch.
When NFA is in a state with an $ \varepsilon$ arrow, NFA splits in multiple copies.
Each copy following an arrow with $ \varepsilon$ and one for the current state.
If at the end one the copies is in an accept state, the NFA accepts the input string.
Thus nondeterminism is kind of parallel computation.
Also can the nondeterministic computation be viewed as tree.
Every NFA can be converted into equivalent DFA.
This is useful because NFAs are easier to construct than DFAs.



\subsection{Formal Definition of NFA}

\begin{defn}\label{def:NFA}
  A \emph{\textbf{nondeterministic finite automaton}} is a 5-tuple $ \pr{Q, \Sigma, \delta, q_{0}, F}$, where
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $Q$ is a finite set called the \emph{\textbf{states}},
    \item $ \Sigma$ is a finite set called the \emph{\textbf{alphabet}},
    \item $ \delta : Q \times \Sigma \rightarrow \mathcal{P}\of{Q}$ is the \emph{\textbf{transition}} (note the power set),
    \item $q_{0} \in Q$ is the \emph{\textbf{start state}},
    \item $F \subseteq Q$ is the \emph{\textbf{set of accept states}} (\emph{\textbf{final states}}).
  \end{enumerate}
\end{defn}
\begin{defn}
  Let $N = \pr{Q, \Sigma, \delta , q_{0}, F}$ be an NFA and let $w = w_{1} w_{2}...w_{n}$ be a string where $ w_{\i} \in \Sigma_{ \varepsilon}$. Then $N$ \emph{\textbf{accepts}} $w$ if a sequence $r_{0}, r_{1},...,r_{n} \in Q$ exists with:
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $r_{0} = q_{0}$ (start in the start state)
    \item $ \delta\of{r_{\i}, w_{i+1}} = r_{i+1}$, for $i = 0,...,n-1$ (set of allowable next states)
    \item $r_{n} \in F$ (ends in accept state)
  \end{enumerate}
  M \emph{\textbf{recognizes language}} $A$ if $A = \{w \, | \text{ $M$ accepts $w$}\}$.
\end{defn}



\subsubsection{Equivalence of NFAs and DFAs}

NFAs and DFAs recognize the same class of languages.
(NFAs are not more "powerful" than DFAs.)\\
Two machines are \textbf{\emph{equivalent}} if they recognize the same language.
\begin{thm}
  Every NFA has an equivalent DFA.
\end{thm}
Can be proven by construction.
Ansatz: Let $N = (Q, \Sigma, \delta, q_{0}, F)$ be NFA.
Construct the DFA $M = (Q', \Sigma, \delta', q_{0}', F')$ with $Q' = \mathcal{P}\of{Q}$.
\begin{thm}
  A language is regular if and only if some nondeterministic finite automaton recognizes it.
\end{thm}
This can be proven by using the def. \ref{def:regLang}.\\
With NFAs one can proof the closure languages unter \ref{def:operLang}. To do so one constructs a NFA from one or two DFAs according to operation. (Sipser Fig. 1.46 - Fig .1.50)



\subsection{Regular Expressions}

We use the regular operations to build up expressions describing languages,
which are called \textbf{regularexpressions}. E.g.
\begin{equation*}
  (0 \cup 1)\,0^{*}
\end{equation*}
are strings that start with $0$ or $1$ and end with any number of 0s.
They form a language.

\begin{defn}
  $R$ is a \textbf{regular expression} if $R$ is
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $a$ for some $a$ in the alphabet $\Sigma\,\;\pr{R=\cbr{a}}\,$,
    \item $ \varepsilon\,\; \pr{R= \cbr{ \varepsilon}}\,$,
    \item $ \pr{R_{1} \cup R_{2}}$, where $R_{1}$ and $R_{2}$ are regular expressions,
    \item $ \pr{R_{1} \circ R_{2}}$, where $R_{1}$ and $R_{2}$ are regular expressions,
    \item $ \pr{R_{1}}^{*}$, where $R_{1}$ is a regular expression.
  \end{enumerate}
\end{defn}
Seemingly, we are in danger of circulardefinition, which would be invalid.
However, $R_{1}$ and $R_{2}$ always are smaller than $R$.
Thus we actually are defining regular expressions in terms of smaller regular expressions and thereby avoiding circularity.
A definition of this type is called an \textbf{inductive definition}.\\
Special cases:
\begin{itemize}
  \item $R \cup \varnothing = R$
  \item $R \varepsilon = R$
  \item $1^{*} \O = \O$
  \item $ \O^{*} = \cbr{ \varepsilon}$
\end{itemize}
When we want to distinguish between a regular expression $R$ and the language that it describes, we write $L\of{R}$) to be the language of $R$.


Regular expressions are useful tools in the design of compilers for programming languages.
Elemental objects in a programming language, called tokens, such as the variable names and constants, may be described with regular expressions.
For example, a numerical constant that may include a fractional part and/or a sign may be described as a member of the language
\begin{equation*}
  \pr{+ \cup - \cup \varepsilon} \pr{D^{+} \cup D^{+}.D^{*} \cup D^{*}.D^{+}}
\end{equation*}
where $D = \cbr{0,1,2,3,4,5,6,7,8,9}$ is the alphabet of decimal digits.
Examples of generated strings are: $72\,,\,3.14159\,,\,+7.\,,\,-.01$ .
Once the syntax of the tokens of the programming language have been described with regular expressions,
automatic systems can generate the \textbf{lexical analyzer},
the part of a compiler that initially processes the input program.



\subsection{Equivalence with Finite Automata}

Regular expressions and finite automata are equivalent in their descriptive power.
Any regular expression can be converted into a finite Automaton that recognizes the language it describes, and vice versa.
In order to construct a NFA from a regular expression one can conveniently use $ \varepsilon$-edges.\\
E.g. $(ab \cup a)^{*}$\\
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (1) {};
    \node[state, accepting] (2)[right of=1] {};
    \path (1) edge[above] node{a} (2);
\end{tikzpicture} \quad $a$\\
\bigbreak
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (1) {};
    \node[state, accepting] (2)[right of=1] {};
    \path (1) edge[above] node{b} (2);
\end{tikzpicture} \quad $b$\\
\bigbreak
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (1) {};
    \node[state] (2)[right of=1] {};
    \node[state] (3)[right of=2] {};
    \node[state, accepting] (4)[right of=3] {};
    \path (1) edge[above] node{a} (2);
    \path (2) edge[above] node{$\varepsilon$} (3);
    \path (3) edge[above] node{b} (4);
\end{tikzpicture} \quad $ab$\\
\bigbreak
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (0) {};
    \node[state] (1)[above right of=0] {};
    \node[state] (2)[right of=1] {};
    \node[state] (3)[right of=2] {};
    \node[state, accepting] (4)[right of=3] {};
    \node[state] (5)[below right of=0] {};
    \node[state, accepting] (6)[right of=5] {};
    \path (0) edge[above] node{$\varepsilon$} (1);
    \path (0) edge[above] node{$\varepsilon$} (5);
    \path (1) edge[above] node{a} (2);
    \path (2) edge[above] node{$\varepsilon$} (3);
    \path (3) edge[above] node{b} (4);
    \path (5) edge[above] node{a} (6);
\end{tikzpicture}\\
\quad $ab \cup a$\\
\bigbreak
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial, accepting] (00) {};
    \node[state] (0)[right of=00] {};
    \node[state] (1)[above right of=0] {};
    \node[state] (2)[right of=1] {};
    \node[state] (3)[right of=2] {};
    \node[state, accepting] (4)[right of=3] {};
    \node[state] (5)[below right of=0] {};
    \node[state, accepting] (6)[right of=5] {};
    \path (00) edge[above] node{$\varepsilon$} (0);
    \path (0) edge[above] node{$\varepsilon$} (1);
    \path (0) edge[above] node{$\varepsilon$} (5);
    \path (1) edge[above] node{a} (2);
    \path (2) edge[above] node{$\varepsilon$} (3);
    \path (3) edge[above] node{b} (4);
    \path (5) edge[above] node{a} (6);
    \path (4) edge[above, bend left] node{$\varepsilon$} (0);
    \path (6) edge[above, bend right] node{$\varepsilon$} (0);
  \end{tikzpicture}\\
\quad $(ab \cup a)^{*}$\\
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (00) {};
    \node[state] (0)[right of=00] {};
    \node[state] (1)[above right of=0] {};
    \node[state] (2)[right of=1] {};
    \node[state] (3)[right of=2] {};
    \node[state] (4)[right of=3] {};
    \node[state] (5)[below right of=0] {};
    \node[state] (6)[right of=5] {};
    \node[state] (7)[below of=5] {};
    \node[state, accepting] (8)[right=3cm of 7] {};
    \path (00) edge[above] node{$\varepsilon$} (0);
    \path (0) edge[above] node{$\varepsilon$} (1);
    \path (0) edge[above] node{$\varepsilon$} (5);
    \path (1) edge[above] node{a} (2);
    \path (2) edge[above] node{$\varepsilon$} (3);
    \path (3) edge[above] node{b} (4);
    \path (5) edge[above] node{a} (6);
    \path (4) edge[above, bend left] node{$\varepsilon$} (0);
    \path (6) edge[above, bend right] node{$\varepsilon$} (0);
    \path (00) edge[above, bend left] node{$\varepsilon$} (7);
    \path (4) edge[above, bend left] node{$\varepsilon$} (7);
    \path (6) edge[above, bend left] node{$\varepsilon$} (7);
    \path (7) edge[below] node{a} (8);
  \end{tikzpicture}\\
\quad $(ab \cup a)^{*} a $\\
\bigbreak
\begin{thm}
  A language is regular if and only if some regular expression describes it.
\end{thm}
For building an NFA from a regular expression one can use a recipe.
Build the NFA from the smallest subexpression by connecting states with $ \varepsilon$ for $ \cdot$ , splitting the states with $ \varepsilon$ for $ \cup$ and returning to some previews state for $*$ .

\begin{defn}
  A \textbf{generalized nondeterministic finite automaton}, a GNFA, is a NFA wherein the trasition arrows may have any regular expression as labels.
  A GNFA may have several different ways to process the same input sting, since it is nondeterministic.
  It accepts its input if its processing can cause the GNFA to be in an accept state at the end of the input.
\end{defn}
For convenience the GNFA should have the following properties:
\begin{itemize}
  \item The start state has only outgoing arrows to every other state.
  \item GNFA has only 1 accept state.
  \item The accept state has only ingoing arrows from every other state.
  \item The accept state $ \neq$ start state.
  \item Except the start and accept states, one arrow goes from every state to every other state and also from each state to itself.
\end{itemize}
The formal definition for GNFA is then given by
\begin{defn}
  A \textbf{generalized nondeterministic finite automaton} is 5-tuple $ \pr{Q, \Sigma , \delta , q_{start}, q_{accept}}$ , where
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $Q$ is the finite set of states,
    \item $ \Sigma$ is the input alphabet,
    \item $ \delta : \pr{Q- \cbr{q_{accept}}} \times \pr{Q - \cbr{ q_{start}}} \to \mathcal{R}$,
      \item $ q_{start}$ is the start state,
      \item $ q_{accept}$ is the accept state.
  \end{enumerate}
\end{defn}
\begin{defn}
  A GNFA accepts a string $ w \in \Sigma^{*}$ if $w = w_{1} w_{2} \dots w_{k}$, where each $ w_{i} \in \Sigma^{*}$ and a sequence of states $ q_{0}, q_{1}, \dots, q_{k}$ exists such that
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $ q_{0} = q_{start}$ is the start state,
    \item $q_{k} = q_{accept}$ is the accept state,
    \item for each $i$, we have $ w_{i} \in L\of{R_i}$, where $R_i= \delta\of{q _{i-1},q_i}$;
      in other words, $R_i$ is the expression on the arrow from $ q_{i-1}$ to $q_i$
  \end{enumerate}
\end{defn}
In order to convert a DFA into a regular expression we construct first a GDFA from the DFA by adding a new start and accept state and additional transition arrows as necessary.
Then we rip off one state after another and replace it with an arrow like following\\
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (i) {$q_i$};
    \node[state, accepting] (rip)[above right of=i] {$q_{rip}$};
    \node[state, accepting] (j)[below right of=rip] {$q_{j}$};
    \path (i) edge[below] node{$R_{ij}$} (j);
    \path (i) edge[left, bend left] node{$R_{ir}$} (rip);
    \path (rip) edge[right, bend left] node{$R_{rj}$} (j);
    \path (rip) edge[above, loop] node{$R_{rr}$} (rip);
\end{tikzpicture} \quad
\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=2.5cm, semithick]
    \node[state, initial] (i) {$q_i$};
    \node[state, accepting] (j)[right=3cm of i] {$q_{j}$};
    \path (i) edge[above] node{$R_{ir}\,R_{rr}^{\,*}\,R_{rj} \cup R_{ij}$} (j);
\end{tikzpicture}
\bigbreak
\begin{thm}
  \textbf{Pumping lemma} \quad If $A$ is a regular language, then there is a number $p$ (the pumping length) where, if $s$ is any string in $A$ of length at least $p$, then $s$ may be divided into three pieces, $s = xyz$, satisfying the following conditions:
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item for each $i \ge 0$, $xy^{i}z \in A$,
    \item $ \abs{u} > 0$, and
    \item $ \abs{xy} \le p$ \, .
  \end{enumerate}
\end{thm}



\section{Context-Free Languages}

\textbf{Context-free grammars}, a more powerful method of describing languages (e.g. account for recursive structures).
They are used in specification and compilation of programming languages.
Designers of compilers and interpreters for programming languages often start by obtaining a grammar for the language.
A \textbf{parser} in a compiler or interpreter extracts the meaning of a program prior to generating the compiled code or interpreting.
There are tools to automatically generate the parser from the grammar.


All strings generated by the grammar $G$ constitute the \textbf{language of the grammar} $L\of{G}$.
Any language that can be generated by some context-free grammar is called a \textbf{context-free language} (CFL).

\begin{defn}
  A \textbf{context-free grammar} is a $4$-tuple $(V, \Sigma , R , S)$\,, where
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item $V$ is a finite set called the \textbf{variables},
    \item $ \Sigma$ is a finite set, disjoint from $V$, called the \textbf{terminals},
    \item $R$ is a finite set of \textbf{rules}, with each rule being a variable and a string of variables and terminals, and
    \item $S \in V$ is the start variable.
  \end{enumerate}
\end{defn}
\begin{defn}
  If $u,v,w$ are strings of variables and terminals, and $A \to w$ is a rule of the grammar, we say that $uAv$ \textbf{yields} $uwv$,
  written $uAv \implies uwv$.\\
  Say that $u$ \textbf{derives} $v$, written $u \xRightarrow{*} v$, if $u=v$ or if a sequence $u_{1}, u_2, \dots u_k$ exists for $k \ge 0$ and
  \begin{equation*}
    u \implies u_1 \implies u_2 \implies \dots \implies u_k \implies v \,.
  \end{equation*}
\end{defn}
\begin{defn}
  The \textbf{language of the grammar} is \,$\cbr{w \in \Sigma^{*} | S \xRightarrow{*}w}$ \, .
\end{defn}
\bigbreak
E.g.:\\
$G = \pr{V, \Sigma, R, \expval{EXPR}}$ with $V=\cbr{\expval{EXPR},\expval{TERM},\expval{FACTOR}}$ and $ \Sigma = \cbr{a,+, \times, (,)}$\,.
And the rules are:
\begin{align*}
  \expval{EXPR} &\to \expval{EXPR}+\expval{TERM} | \expval{TERM}\\
  \expval{TERM} &\to \expval{TERM} \times \expval{FACTOR} | \expval{FACTOR}\\
  \expval{FACTOR} &\to \expval{EXPR} | \;a
\end{align*}
One of the possible string generated with $G$ is $a+a \times a$.
The corresponding parse tree is:
\bigbreak
\begin{tikzpicture}[sibling distance=6em,
  every node/.style = {shape=rectangle, rounded corners,
  draw, align=center}]
  \node {$\expval{EXPR}$}
    child { node {$\expval{EXPR}$}
      child { node {$\expval{TERM}$}
        child { node {$\expval{FACTOR}$}
          child { node {$a$}}
        }
      }
    }
    child { node {}
      child { node {}
        child { node {}
          child { node {$+$}}
        }
      }
    }
    child [missing]
    child { node {$\expval{TERM}$}
      child { node {$\expval{TERM}$}
        child { node {$\expval{FACTOR}$}
          child { node {$a$}}
        }
      }
        child { node {}
          child { node {}
            child { node {$\times$}}
          }
        }
        child { node {$\expval{FACTOR}$}
            child { node {}
              child { node {$a$}}
            }
        }
    };
\end{tikzpicture}\\
Note that $G$ preserves the operation order $ \times$ before $+$.



\subsection{Ambiguity}

Sometimes a grammar can generate the same string in several different ways with several different parse trees and thus several different meanings.
Compare the sentence "The girl touches the boy with the flower." when grouping "The girl touches the boy" vs. "the boy with the flower".
This may be a problem when a unique interpretation is desired.
If a grammar generates the same string in several different ways, we say that the string is derived \emph{ambiguously} and the grammar is \emph{ambiguous}.

\begin{defn}
  A derivation of a string $w$ in a grammar $G$ is a \textbf{leftmost derivation} if at every step the leftmost remaining variable is the one replaced.
\end{defn}

\begin{defn}
  A string $w$ is derived \textbf{ambiguously} in context-free grammar $G$ if it has two or more different leftmost derivations.
  Grammar $G$ is \textbf{ambiguous} if it generates some string ambiguously.
\end{defn}

\begin{defn}
  If a context-free language can be generated only by ambiguous grammars, we say the \textbf{language} is \textbf{inherently ambiguous}.
\end{defn}



\subsection{Chomsky Normal Form}

\begin{defn}
  A context-free grammar is in \textbf{Chomsky normal form} if every rule is of the form
  \begin{align*}
    A &\to BC \\
    A &\to a
  \end{align*}
\end{defn}

\begin{thm}
  Any context-free language is generated by a context-free grammar in Chomsky normal form.
\end{thm}

We can convert any grammar $G$ into Chomsky normal form by adding a new start variable, then eliminating all $ \varepsilon$ rules $A \to \varepsilon$, all unit rules $A \to B$ and converting the remaining rules into the proper form.
E.g.:\\
From
\begin{align*}
  S &\to ASA \\
  A \to B \,|\, S
\end{align*}
to
\begin{align*}
  S_{0} \to S
  S &\to ASA \\
  A \to B \,|\, S \;.
\end{align*}
From
\begin{align*}
  S &\to aB\\
  B &\to \varepsilon
\end{align*}
to
\begin{align*}
  S \to aB \,|\, a \; .
\end{align*}
From
\begin{align*}
  A &\to B \,|\, S \\
  B &\to b
\end{align*}
to
\begin{align*}
  A \to B \,|\, S \,|\, b \;.
\end{align*}
From
\begin{align*}
  S_{0} \to ASA\,|\, aB
\end{align*}
to
\begin{align*}
  S_{0} &\to AA_1\,|\, aB \\
  A_{1} &\to SA \;.
\end{align*}



\subsection{Pushdown Automata}

\textbf{Deterministic/Nondeterministic pushdown automata} (DPDA/NPDA) are constructed from a NFA/DFA with a \textbf{stack}.
DPDA and NPDA are \emph{not} equivalent in power.
Here we study only NPDA.
Stack acts like additional (unlimited) memory.
This allows to recognize some nonregular languages (like $ \cbr{0^{n}1^{n}\,|\,n \ge 0}$).
PDA are as powerfull as CFG.
\begin{thm}
  A language is context free if and only if some pushdown automaton recognizes it.
\end{thm}
One can construct a NFA for a given Grammar by the following scheme

\begin{tikzpicture}[->, >=stealth, shorten >=1.5pt, node distance=3.5cm, semithick]
  \node[state, initial] (s) {$q_{start}$};
    \node[state] (l)[right of=s] {$q_{loop}$};
    \node[state,accepting] (a)[right of=l] {$q_{accept}$};
    \path (s) edge[below] node{$ \varepsilon , \varepsilon \to S\$ $} (l);
    \path (l) edge[loop,above] node{$ \varepsilon , A \to w $ for $A \to w$ ;
      $a$, $a \to \varepsilon$ for $a \in \Sigma$} (l);
    \path (l) edge[below] node{$ \varepsilon , \$ \to \varepsilon $} (a);
\end{tikzpicture}



\section{Pumping Lemma for Context-Free Languages}

\begin{thm}
  \textbf{Pumping lemma} \quad If $A$ is a context-free language, then there is a number $p$ (the pumping length) where, if $s$ is any string in $A$ of length at least $p$, then $s$ may be divided into five pieces, $s = uvxyz$, satisfying the following conditions:
  \begin{enumerate}[label=\textbf{\arabic*}.]
    \item for each $i \ge 0$, $uv^{i}xy^{i}z \in A$,
    \item $ \abs{vy} > 0$, and
    \item $ \abs{vxy} \le p$ \, .
  \end{enumerate}
\end{thm}



\bibliographystyle{plain}
\bibliography{TheoComp__notes}


\end{document}
