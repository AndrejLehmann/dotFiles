#+BEGIN_HTML
  <!-- book/_sidebar.md -->
#+END_HTML

- [[/][Home]]
- [[file:preface.md][Preface]]
- [[file:chapter1.md][Chapter 01 - Introduction to Lisp]]
- [[file:chapter2.md][Chapter 02 - A Simple Lisp Program]]
- [[file:chapter3.md][Chapter 03 - Overview of Lisp]]
- [[file:chapter4.md][Chapter 04 - GPS: The Genera Problem Solver]]
- [[file:chapter5.md][Chapter 05 - ELIZA: Dialog with a Machine]]
- [[file:chapter6.md][Chapter 06 - Building Software Tools]]
- [[file:chapter7.md][Chapter 07 - STUDENT: Solving Algebra Word
  Problems]]
- [[file:chapter8.md][Chapter 08 - Symbolic Mathematics: A
  Simplification Program]]
- [[file:chapter9.md][Chapter 09 - Efficiency issues]]
- [[file:chapter10.md][Chapter 10 - Low-Level Efficiency Issues]]
- [[file:chapter11.md][Chapter 11 - Logic Programming]]
- [[file:chapter12.md][Chapter 12 - Compiling Logic Programs]]
- [[file:chapter13.md][Chapter 13 - Object-Oriented Programming]]
- [[file:chapter14.md][Chapter 14 - Knowledge Representation and
  Reasoning]]
- [[file:chapter15.md][Chapter 15 - Symbolic Mathematics with Canonical
  Forms]]
- [[file:chapter16.md][Chapter 16 - Expert Systems]]
- [[file:chapter17.md][Chapter 17 - Line-Diagram Labeling by Constraint
  Satisfaction]]
- [[file:chapter18.md][Chapter 18 - Search and the Game of Othello]]
- [[file:chapter19.md][Chapter 19 - Introduction to Natural Language]]
- [[file:chapter20.md][Chapter 20 - Unification Grammars]]
- [[file:chapter21.md][Chapter 21 - A Grammar of English]]
- [[file:chapter22.md][Chapter 22 - Scheme: An Uncommon Lisp]]
- [[file:chapter23.md][Chapter 23 - Compiling Lisp]]
- [[file:chapter24.md][Chapter 24 - ANSI Common Lisp]]
- [[file:chapter25.md][Chapter 25 - Troubleshooting]]
- [[file:code.md][Code Highlighting Test]]
