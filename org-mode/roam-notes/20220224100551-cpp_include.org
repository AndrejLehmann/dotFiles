:PROPERTIES:
:ID:       00c19027-1285-490d-bc8b-fdb257f05c34
:END:
#+title: Cpp: src_cpp{#include}
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:cpp:

src_cpp{#include} copies and pastes the code.
When quoted like in src_cpp{#include "myfile.h"} compiler looks for myfile.h relative to the current file, while when in angular brackets like in src_cpp{#include <iostream>} the compiler looks in include-directories.
But when quoted if the compiler does not find in the relative directory, it will look in include-directories.

C standard libraries usually have '.h' extension, C++ standard libraries usually do not.
This is why src_cpp{#include <iostream>} does not have an '.h' at the end.
