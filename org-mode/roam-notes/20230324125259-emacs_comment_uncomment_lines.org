:PROPERTIES:
:ID:       619028a4-2b49-498e-aba3-70d5de87ef6f
:END:
#+title: Emacs: comment/uncomment lines
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Select lines visually and press M-;
That executes comment-dwim

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
