:PROPERTIES:
:ID:       edf3dac2-9228-4716-819e-f67dd790609c
:END:
#+title: C++: header files
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:cpp:

* Header files [cite:@cpp-header-files-cherno]

Function has to be declared in the file where it is used.

=#pragma once= can be used to ensure that a header file is included only once in a /single/ translation unit (e.g. file).
This is similar to =#ifndef=, =#define=, =#endif= header guards.
This is e.g. needed when =struct= or =class= is defined in a header file.
Otherwise one might get a redefinition  error.

#+begin_export latex
    \clearpage
#+end_export

* References
#+print_bibliography:
