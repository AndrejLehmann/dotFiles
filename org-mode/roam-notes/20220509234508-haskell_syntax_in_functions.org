:PROPERTIES:
:ID:       dcfd28b8-f5a1-48bd-82e3-622c0df6de37
:END:
#+title: Haskell: Syntax in Functions
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* Pattern matching [cite:@learn-you-a-haskell]

#+begin_src haskell :session  :exports both
:{
sayNum :: (Integral a) => a -> String
sayNum 1 = "One!"
sayNum 2 = "Two!"
sayNum 3 = "Three!"
sayNum x = "Not between 1 and 3"
:}
sayNum 2
#+end_src

#+RESULTS:
: Two!

The patterns will be checked from top to bottom and when it conforms to a pattern, the corresponding function body will be used.
The last pattern matches anything and binds it to x.

#+begin_src haskell :session :exports both
sayNum 4
#+end_src

#+RESULTS:
: Not between 1 and 3

Compare with

#+begin_src haskell :session :exports both
:{
sayNum :: (Integral a) => a -> String
sayNum x = "Not between 1 and 3"
sayNum 1 = "One!"
sayNum 2 = "Two!"
sayNum 3 = "Three!"
:}
sayNum 2
#+end_src

#+RESULTS:
: Not between 1 and 3

So the order of patterns is important.
Consider also the following example

#+begin_src haskell :exports both
:{
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)
:}
factorial 4
#+end_src

#+RESULTS:
: 24

If we wrote the line with the base case after the rekursion line, it would catch all numbers, including 0 and our calculation would never terminate.

** in tuples

#+begin_src haskell :exports both
:{
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)
:}
addVectors (1, 2) (4, 5)
#+end_src

#+RESULTS:
| 5 | 7 |

#+begin_src haskell :exports both
:{
first :: (a, b, c) -> a
first (x, _, _) = x

second :: (a, b, c) -> b
second (_, y, _) = y

third :: (a, b, c) -> c
third (_, _, z) = z
:}
#+end_src

** in lists and list comprehensions

#+begin_src haskell :exports both
xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]
[a+b | (a,b) <- xs]
#+end_src

#+RESULTS:
| 4 | 7 | 6 | 8 | 11 | 4 |

A pattern like src_haskell{x:xs} will bind the head of the list to src_haskell{x} and the rest of it to src_haskell{xs}, even if there's only one element so xs ends up being an empty list.
If you want to bind, say, the first three elements to variables and the rest of the list to another variable, you can use something like src_haskell{x:y:z:rest}.
It will only match against lists that have three elements or more.

#+begin_src haskell :exports both
x:y:z:rest = [1,2,3,4,5,6]
rest
#+end_src

#+RESULTS:
| 4 | 5 | 6 |

One more example

#+begin_src haskell :exports both
:{
tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x:[]) = "The list has one element: " ++ show x
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:rest) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y
:}
#+end_src

This function is safe because it takes care of the empty list, a singleton list, a list with two elements and a list with more than two elements.
Note that src_haskell{(x:[])} and src_haskell{(x:y:[])} could be rewritten as src_haskell{[x]} and src_haskell{[x,y]} (because its syntatic sugar, we don't need the parentheses).
We can't rewrite src_haskell{(x:y:_)} with square brackets because it matches any list of length 2 or more.

If you want to reference the whole list after pattern matching, you can use src_haskell{@}

#+begin_src haskell :exports both
:{
fstLetter :: String -> String
fstLetter "" = "Empty string, whoops!"
fstLetter all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x] ++ "."
:}
fstLetter "Dracula"
#+end_src

#+RESULTS:
: The first letter of Dracula is D.


#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
