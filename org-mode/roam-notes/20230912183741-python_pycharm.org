:PROPERTIES:
:ID:       5f1b4dd3-11e8-4bff-b94c-9db67933cf65
:END:
#+title: Python: pycharm
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:python:tools:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* short cuts

|-------------+-----------------------------------------+--------------------------------------------------------------------------------------|
| command     | contex                                  | description                                                                          |
|-------------+-----------------------------------------+--------------------------------------------------------------------------------------|
| S-SPC (S S) |                                         | search everywhere                                                                    |
| C-B         | on function call                        | jump to declaration of a method                                                      |
| C-B         | on function definition                  | show usages as list                                                                  |
| C-B         | on declaration of usage                 | show usages in find window                                                           |
| S-ESC       | when window is open                     | close window                                                                         |
| A-3         | when find window was closed             | open find window again                                                               |
| C-F12       |                                         | open file structure as list                                                          |
| A-7         |                                         | open file structure in window                                                        |
| C-E         |                                         | open recent files                                                                    |
| C-S-E       |                                         | code in recently visited files                                                       |
| C-F8        |                                         | set break point                                                                      |
| C-N         |                                         | search classes                                                                       |
| C-S-N       |                                         | search for a file                                                                    |
| C-A-S-N     |                                         | search for a method or global variable                                               |
| C-S-F       |                                         | find in files                                                                        |
| A-W         | when searching                          | consider the search string as the whole word                                         |
| C-S-R       |                                         | replace in files                                                                     |
| C-/         |                                         | single line comment                                                                  |
| C-S-/       |                                         | block comment                                                                        |
| C-W         |                                         | expand code selction                                                                 |
|-------------+-----------------------------------------+--------------------------------------------------------------------------------------|
| S-F9        |                                         | run debuger                                                                          |
| C-S-a       | when debuging                           | add to watches                                                                       |
| F7          | when debuging                           | step in                                                                              |
| <- / ->     | if nested function calls, when debuging | select function call                                                                 |
| C-A-F8      | when debuging                           | quick evaluate current/selected expression                                           |
| C-F5        | when debuging                           | rerun program                                                                        |
| F8          | when debuging                           | step over                                                                            |
| F9          | when debuging                           | resume program execution                                                             |
| A-F9        | when debuging                           | execute the program up the current line (it also shows the returned/assigned values) |
| C-F2        | when debuging                           | finish debuging                                                                      |
|-------------+-----------------------------------------+--------------------------------------------------------------------------------------|
| C-S-´       |                                         | git checkout                                                                         |
| C-K         |                                         | git commit                                                                           |
| C-S-K       |                                         | git push                                                                             |
| A-9         |                                         | show git tool window                                                                 |
| A-M         |                                         | git amend                                                                            |
| C-T         |                                         | open update project diaglog window                                                   |
| A-S         |                                         | git squash                                                                           |
|-------------+-----------------------------------------+--------------------------------------------------------------------------------------|


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
