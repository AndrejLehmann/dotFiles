:PROPERTIES:
:ID:       54ff614b-c6db-4eda-9658-805604f4017b
:END:
#+title: C#: arrays vs. lists
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* arrays

- fixed size
- store items of the same type
- lower-level data structure
- faster when accessing elements

* lists

- dynamic size
- store items of different types
- items can be accessed by a foreach loop
- only allocate memory as needed
- faster when adding or removing elements


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
