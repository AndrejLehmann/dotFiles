:PROPERTIES:
:ID:       536f01a5-2d62-4843-902a-79e1b9852167
:END:
#+title: Cpp: conditions and branching
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:cpp:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* performance

Branching has an overhead since it results in jumping to different part of memory, where the instructions are stored, and then jumping back.

* checks

src_cpp{if} statements can also be performed on src_cpp{int} since src_cpp{0} means src_cpp{false} and any other value means src_cpp{true}

#+begin_src cpp
#include <iostream>

int main(){
    if (-5) std::cout << "(-5) means true";
    else  std::cout << "(-5) means false";
}
#+end_src

#+RESULTS:
: (-5) means true

And also if something is null.

#+begin_src cpp
#include <iostream>

int main(){
    const char* ptr = nullptr;
    if (ptr) std::cout << "ptr is not null";
    else  std::cout << "ptr is null";
}
#+end_src

#+RESULTS:
: ptr is nullptr

or

#+begin_src cpp
#include <iostream>

int main(){
    if ('\0') std::cout << "'\\0' means true";
    else  std::cout << "'\\0' means false";
}
#+end_src

#+RESULTS:
: '\0' means false

* else if short cut

#+begin_src cpp
else if {
    // code
}
#+end_src

is short for

#+begin_src cpp
else {
    if {
        // code
    }
}
#+end_src
