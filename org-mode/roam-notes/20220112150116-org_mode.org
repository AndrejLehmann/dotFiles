:properties:
:ID:       0a545615-867e-452d-b695-ff22c8eb5f23
:end:
#+title: org-mode
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:latex:orgmode:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* commands, actions, packages, modes etc.

- Meta key = option key (mac)

** org mode

[[https://blog.tecosaur.com/tmio/2021-07-31-citations.html][blog]]
[[https://orgmode.org/manual/Citations.html][orgmode.org]]
[[https://orgmode.org/worg/org-tutorials/][org tutorials]]
[[https://orgmode.org/org.html][org manual]]

|-----------------+---------------------+-----------------------------------------------------------------|
| command         | alternative command | description                                                     |
|-----------------+---------------------+-----------------------------------------------------------------|
| SPC m .         | SPC m g g           | org-goto                                                        |
| SPC m s h       | META h              | promote subtree                                                 |
| SPC m s l       | META l              | demote subtree                                                  |
| SPC m s k       | META k              | move subtree up                                                 |
| SPC m s j       | META j              | move subtree down                                               |
| SPC m s n       |                     | narrow to subtree (hide others)                                 |
| SPC m s N       |                     | widen the subtree (show whole buffer)                           |
| S-TAB           |                     | org-cycle                                                       |
| SPC m t         |                     | org-todo                                                        |
| SPC m p u/d     | S-up/down           | Set TODO priority                                               |
| C-RET           |                     | continue list                                                   |
| SPC n l         |                     | store link (e.g. for inserting a link)                          |
| SPC m l l       |                     | insert a link                                                   |
| g s SPC         |                     | find string and enumerate findings by (home row) letters        |
| SPC m '         |                     | edit (e.g. code snipppet) in a extra buffer                     |
| C-c C-c         |                     | finish editing                                                  |
| org-sparse-tree |                     | shows only the specified part (e.g. TODO, tag) of the text file |
|-----------------+---------------------+-----------------------------------------------------------------|


|------------------------------------------+---------------------------------------|
| action                                   | description                           |
|------------------------------------------+---------------------------------------|
| add to link after file name '::<number>' | link to a specific line in other file |
|------------------------------------------+---------------------------------------|

*** tables and spread sheets

[[https://orgmode.org/worg/org-tutorials/org-spreadsheet-intro.html][spreadsheet]]
[[https://orgmode.org/worg/org-tutorials/tables.html][tables]]
[[https://orgmode.org/org.html#The-Spreadsheet][spreadsheets in all details]]
[[https://orgmode.org/org.html#Tables][tables in all details]]

|------------------+---------------------------------------------------------------|
| command          | description                                                   |
|------------------+---------------------------------------------------------------|
| org-table-shrink | Shrink all columns with a width cookie in the table at point. |
| org-table-expand | Expand all columns in the table at point.                     |
| C-c ?            | Info about the field                                          |
|------------------+---------------------------------------------------------------|

|------------------------+-------------------------------|
| action                 | description                   |
|------------------------+-------------------------------|
| in a column type <num> | width of a column when shrunk |
|------------------------+-------------------------------|

** evil-snipe

Like f/F commands in vim but with extended functionality by searching for 2 characters instead of one.

|-------------+------------------------------------|
| command     | description                        |
|-------------+------------------------------------|
| s <2 chars> | jump to this 2 characters forward  |
| S <2 chars> | jump to this 2 characters backward |
| ;           | jump to next occurrence            |
| ,           | jump to previous occurrence        |
|-------------+------------------------------------|

** avy

avy package shows all occurrences of the typed characters and labels them with letters for jumping, deleting, correcting, ...

|----------------------------+--------------------------------------------|
| command                    | description                                |
|----------------------------+--------------------------------------------|
| g s SPC <chars> <letters>  | jump                                       |
| g s SPC <char> x <letters> | delete selected word and jump there        |
| g s SPC <char> X <letters> | delete selected word without jumping there |
| g s SPC <char> i <letters> | correct with ispell                        |
| g s SPC <char> y <letters> | yank and past selected word to cursor      |
| g s SPC <char> t <letters> | teleport selected word to cursor           |
|----------------------------+--------------------------------------------|

| variable        | values | description                                     |
|-----------------+--------+-------------------------------------------------|
| avy-all-windows | t,nil  | whether to search in all open windows on screen |

** org priorities cookies config

#+begin_src elisp
(after! org
  (setq org-priority-faces '(<ascii> :foreground "#<hex>")
                            (<ascii> :foreground "#<hex>")
                            (<ascii> :foreground "#<hex>")
                            (<ascii> :foreground "#<hex>")))
#+end_src

*** org fancy priorities

org-fancy-priorities package for nicer display of priority cookies without modifying the text file.

#+begin_src elisp
(def-package! org-fancy-priorities
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list '("<uni code>" "<string>" "<uni code>")))
#+end_src

** ledger

|----------------------------+-----------------------------------------------------|
| command                    | description                                         |
|----------------------------+-----------------------------------------------------|
| =(ledger-schedule-upcoming)= | opens buffer for copy-pasting recurrent transaction |
|----------------------------+-----------------------------------------------------|

* org-bibtex

compare with org-roam-bibtex

[[http://gewhere.github.io/org-bibtex][blog]]
[[https://vimeo.com/99167082][video]]

* ox-bibtex
[[https://aliquote.org/post/org-and-bibtex/][blog]]

* latex
** [[https://github.com/cdominik/cdlatex][cdlatex]]
** [[https://kitchingroup.cheme.cmu.edu/blog/2016/11/07/Better-equation-numbering-in-LaTeX-fragments-in-org-mode/][numbering equations]]
** [[https://stackoverflow.com/questions/13003895/exporting-inline-code-to-html-in-org-mode][export inline code]]
If you write inline code like src_haskell{1+1} orgmode will try to evaluate it when exporting.
That might be usefull for something like: I'm using Org mode version src_elisp{(org-version)}.
But in order just to export add the property "[:exports code]" to the inline code like src_python[:exports code]{print(1+1)} or =#+PROPERTY: header-args :exports code= to the top of the file
Otherwise one can for generally denoting code enclose it in '='.

** [[https://github.com/politza/pdf-tools][pdf-tools]]

| Syncing with Auctex              |           |
|----------------------------------+-----------|
| jump to PDF location from source | C-c C-g   |
| jump source location from PDF    | C-mouse-1 |

* [[http://ehneilsen.net/notebook/orgExamples/org-examples.html][examples and cookbook]]
* [[https://blog.tecosaur.com/tmio/2021-07-31-citations.html][cite in org-mode]]

| SPC m @   | choose citations         |
| C-M-j     | insert choosen citations |
* [[id:4cf16305-5a54-4ba0-a870-b947ab56e42f][figures]]
* [[id:e7fb724f-ee87-4337-9bac-e8600ae0c6a9][plots]]
* [[id:b47e2af0-7117-4c69-ac27-4f2dc1e1fb3c][writing papers]]
* Code blocks
** Edit code snippet in a extra buffer
=SPC m '=
** [[id:38ed71f2-3515-41fd-b076-66b44f0dc19f][Python: basics for scientific computations]]
** [[id:6de36bf1-0d76-4f3b-b270-ed1399ff4741][Python: programming in emacs]]
** In latex-pdf-file [cite: @code-blocks-in-latex-pdf-stackoverflow]

1. Install pygments: $ pip install Pygments
2. In config.el:
    #+begin_src elisp
    (require 'ox-latex)
    (add-to-list 'org-latex-packages-alist '("" "minted"))
    (setq org-latex-listings 'minted)

    (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
            "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
            "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
    #+end_src
3. In org-file: #+LaTeX_HEADER: \usemintedstyle{default}
   For the list of styles: $ pygmentize -L styles

** [[id:20b6d804-85af-4a3f-934a-76979832ebca][Haskell: usage in org-mode]]
* [[https://github.com/org-roam/org-roam][org-roam]]

compare with [[org-brain]]

** [[https://github.com/org-roam/org-roam-bibtex][org-roam-bibtex]]
** [[https://ianjones.us/own-your-second-brain#org31cfb44][set Up]]
** [[https://github.com/org-roam/org-roam-ui/issues/52][org-roam-ui - display graph and text side by side]]
** [[https://github.com/Kungsgeten/org-brain][org-brain]]
compare with [[org-roam]]
** [[https://jblevins.org/projects/deft/][deft]]
* [[https://github.com/yjwen/org-reveal/][presentations with org-reveal]]
* hyperlinks [cite: @org-mode-links-hyperlinks-and-more]

[[https://orgmode.org/manual/External-Links.html][list of external links]]

short cut: SPC m l l

** link to a section
[[*latex][demo]]

** link to another file

1. SPC m l l
2. type: file
3. choose file
4. type description

[[file:20210727000905-green_s_functions.org][demo]]

*** link to a section

1. [[link to another file][make a link to the file]]
2. Edit
   1. place cursor on the text of the link
   2. SPC m l l
   3. type: ::<name of the section>

[[file:20210727000905-green_s_functions.org::Green's functions in condensed matter theory][demo]]

*** link to a line

1. [[link to another file][make a link to the file]]
2. Edit
   1. place cursor on the text of the link
   2. SPC m l l
   3. type: ::<line number>

[[file:20210727000905-green_s_functions.org::33][demo]]

*** link to a named object in another file src_orgmode{#+name:}

1. [[link to another file][make a link to the file]]
2. Edit
   1. place cursor on the text of the link
   2. SPC m l l
   3. type: ::<name>

[[file:20210727000905-green_s_functions.org::eq:spectral-function-in-terms-of-Sigma-for-free-fermions][demo]]

** link text to code execution
1. type text
2. select text visually
3. SPC m l l
4. type programming language (e.g. elisp)
5. type the code

E.g.: [[elisp:org-agenda][org-agenda]]

** store a link [cite: @org-mode-linkgin-to-word-and-bookmarks]

short cut: SPC n l
That link will be in the list when pressed SPC m l l

* mobile
[[https://organice.200ok.ch][organice]]
[[https://orgmode.org/manual/Org-Mobile.html][org mobile]]
[[https://orgro.org][orgro]]

* link to a page in pdf

[[https://github.com/fuxialexander/org-pdftools][org-pdftools]]

Installed with: M-x pdf-tools-install

#+begin_example org
[[pdf:~/file.pdf::3][Link to page 3]]
#+end_example

* [[https://github.com/dtaht/Gnugol/blob/master/doc/index.org][gnugol]]
Displays google search results in orgmode.
* [[id:046f654d-be6b-4217-b3d1-3a2617fa01cc][ledger]]
[[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-ledger.html][Using Ledger for Accounting in Org-mode with Babel]]

* [[https://github.com/rksm/org-ai][OpenAI]]
* [[https://github.com/org-noter/org-noter][org-noter]]
Org-noter’s purpose is to let you create notes that are kept in sync when you scroll through the document, but that are external to it - the notes themselves live in an Org-mode file.
