:PROPERTIES:
:ID:       d1d1ca20-a0ee-4164-8a07-d11942dde9ab
:END:
#+title: Electric potential
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :physics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Electric potential [cite:@electric-potential]

In the electrostatic the electric potential of a point charge is
\begin{align*}
    \Phi(\vec{r}) = \frac{q}{4 \, \pi \, \varepsilon_0 \, \left| \vec{r} \right| }
\end{align*}
Relation to static electric field is
\begin{align*}
    -\vec \nabla \Phi = \vec E
\end{align*}
In the electrodynamic this changes to
\begin{align*}
    - \vec \nabla \Phi = \vec E + \frac{\partial \vec A}{\partial t}
\end{align*}
where $\vec{A}$ is the [[id:3f1cd661-0bd7-4e58-a81b-2019814595bf][Vector potential]].

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
