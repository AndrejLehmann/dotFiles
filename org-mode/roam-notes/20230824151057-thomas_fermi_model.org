:PROPERTIES:
:ID:       405c19db-01c9-48cb-b237-d2f35faaad72
:END:
#+title: Thomas-Fermi model
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Thomas–Fermi model [cite:@Thomas-Fermi-model-wiki]

/The Thomas–Fermi (TF) model is a quantum mechanical theory for the electronic structure of many-body systems./
/It stands separate from wave function theory as being formulated in terms of the electronic density alone and as such is viewed as a precursor to modern density functional theory./
/The Thomas–Fermi model is correct only in the limit of an infinite nuclear charge./
/Using the approximation for realistic systems yields poor quantitative predictions, even failing to reproduce some general features of the density such as shell structure in atoms and Friedel oscillations in solids./
/This model is based upon local approximation of the distribution of electrons in an atom./
/Although electrons are distributed nonuniformly in an atom, an approximation was made that the electrons are distributed uniformly in each small volume element $\Delta V$ (i.e. locally) but the electron density $n(\vec{r})$ can still vary from one small volume element to the next./


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
