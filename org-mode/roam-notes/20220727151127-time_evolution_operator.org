:PROPERTIES:
:ID:       00b129e7-8d69-4a5e-834a-2287c172ddf9
:END:
#+title: Time evolution operator
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :quantum-mechanics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export



\begin{align*}
    &\i \hbar \frac{\partial}{\partial t} \ket{\psi(t)} = \hat{H} \ket{\psi(t)}\\
    &\i \hbar \frac{\partial}{\partial t} \hat{U}(t)\ket{\psi(0)} = \hat{H} \hat{U}(t)\ket{\psi(0)}\\
    &\i \hbar \frac{\partial}{\partial t} \hat{U}(t) = \hat{H} \hat{U}(t) \qquad \text{since $\ket{\psi(0)}$ can be arbitrary} \\
\end{align*}

* If $H$ does not depend on time

\begin{align*}
    \hat{U}(t) = \e ^{-\i \hat{H} t / \hbar } \qquad \text{if $\hat{H}$ is not time dependend}
\end{align*}


#+begin_export latex
  \clearpage
#+end_export

* If $H$ depends on time

Total time evolution operator (can also be seen as defining equation for $H$):
\begin{align*}
   \hat U(t,t_0) &= \hat T \left[ \exp \left( -\frac{i}{\hbar}\int_{t_0}^t \hat H(t^\prime)dt^\prime\right) \right] \\
                 &\approx 1 - \frac{i \, \hat H_0(t-t_0)}{\hbar} - \frac{i}{\hbar} \int_{t_0}^t \hat H_I(t^\prime) \d t^{\prime} \quad \text{for small $\hat H_I$}\\
\end{align*}
The time-ordering operation $\hat{T}$ can be omitted if $\left[ \hat{H}(t), \hat{H}(t') \right] = 0$.

* References
#+print_bibliography:
