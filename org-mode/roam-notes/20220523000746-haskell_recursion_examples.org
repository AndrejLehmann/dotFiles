:PROPERTIES:
:ID:       29bc7e39-11bb-41e0-b917-302566b572b7
:END:
#+title: Haskell: Recursion examples
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:

#+begin_src haskell :exports both
:{
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let smallerSorted = quicksort [a | a <- xs, a <= x]
        biggerSorted  = quicksort [a | a <- xs, a > x]
    in  smallerSorted ++ [x] ++ biggerSorted
:}
quicksort "the quick brown fox jumps over the lazy dog"
#+end_src

#+RESULTS:
:         abcdeeefghhijklmnoooopqrrsttuuvwxyz

Infinite recursion

#+begin_src haskell :exports both
:{
repeat' :: a -> [a]
repeat' x = x : repeat' x
:}
take 3 $ repeat' 5
#+end_src

#+RESULTS:
| 5 | 5 | 5 |
