:properties:
:ID:       71e60fd7-fc36-40b9-87dd-56e24ef7a431
:end:
#+title: Data Tools and Resources
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :data-science:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

- Tableau Public
- Gephi
- Cytoscape
- D3.js
- svelte
- observable
- RAW graphics
- Visualisingdata.com
- flowingdata.com
- Data Visualization Society
- https://www.datacamp.com/

* [[file:~/gems-backup/org-mode/data-science/road-plan.org][Data science - road plan]]
* [[https://work.caltech.edu/telecourse.html][Lecture - Learning from Data]]
** [[file:~/gems-backup/org-mode/data-science/Learning-from-Data-Solutions/][Solutions]]
