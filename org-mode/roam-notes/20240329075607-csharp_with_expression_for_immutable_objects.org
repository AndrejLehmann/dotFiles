:PROPERTIES:
:ID:       8dd51788-389a-4421-82a3-5c85dc51d4eb
:END:
#+title: C#: =with= expression for immutable objects
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

You can use a =with= expression to create a copy of an immutable object with new values in selected properties.

#+begin_src csharp
Person person2 = person1 with { FirstName = "John" };
#+end_src

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
