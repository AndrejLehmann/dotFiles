:PROPERTIES:
:ID:       ebbecdfa-597d-4cd5-abe0-c41e7cffb91e
:END:
#+title: Friedel oscillations
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:experiments:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Friedel oscillations - Wikipedia [cite:@Friedel-oscillations-wiki]

/Friedel oscillations are a quantum mechanical analog to electric charge screening of charged species in a pool of ions./
/Whereas electrical charge screening utilizes a point entity treatment to describe the make-up of the ion pool, Friedel oscillations describing fermions in a Fermi fluid or Fermi gas require a quasi-particle or a scattering treatment./
/Such oscillations depict a characteristic exponential decay in the fermionic density near the perturbation followed by an ongoing sinusoidal decay resembling sinc function./

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
