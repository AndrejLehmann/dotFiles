% Created 2022-07-06 Wed 18:26
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}}
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{2nd quantisation for Mo's bachelor thesis}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={2nd quantisation for Mo's bachelor thesis},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.6)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\tikzset{every picture/.style={line width=0.11mm}}
\newcommand{\oPerpSymbol}{\begin{tikzpicture}[scale=0.134]
  \draw (0,-0.5)--(0,1); \draw (-0.866,-0.5)--(0.866,-0.5);
  \draw (0,0) circle [radius=1];
\end{tikzpicture}}
\newcommand{\oPerp}{\mathbin{\raisebox{-1pt}{\oPerpSymbol}}}

\clearpage \tableofcontent \clearpage

\section{src}
\label{sec:org5403b9f}
\href{file:///Users/andrejthealien/gems-backup/literature/physics/TheoDerKondensierterMaterie\_Potthoff/CondensedMatterPh\_Potthoff.pdf}{Condensed Matter Theory - Potthoff}

\section{Occupation number representation}
\label{sec:org2242413}

\begin{itemize}
\item Define
\begin{align*}
    \ket{n_1, n_2, ...\, , n_d} = \sqrt{\frac{N!}{\prod_{\alpha} n_{\alpha}!}} \ket{\underbrace{\phi_1 ...\, \phi_1}_{n_1 \; \text{times}} \; \underbrace{\phi_2 ...\, \phi_2}_{n_2 \; \text{times}} ...\, \underbrace{\phi_d ...\, \phi_d}_{n_d \; \text{times}}}^{\!-}
\end{align*}
where \(\ket{\phi_1 ...\, \phi_d}^-\) is a Slater-determinant and \(\sqrt{N!/\prod_{\alpha} n_{\alpha}!}\) is the normalization factor with \(N\) being the total number of particles.

\item \(\ket{n_1, ...\,, n_d}\) is a complete ONB of the antisymmetric \(N\) -particle Hilbert space \(\mathcal{H}^-_N\).

\item If we do not fix \(N\), the number of particles, we can construct Hilbert space of all \(N=0,N=1,N=2,\dots\) states, the so called Fock space, via orthogonal direct sum
\begin{align*}
    \mathcal{H}^- = \oPerp_{N=0,1,2,...} \mathcal{H}_N^-
\end{align*}
In particular \(\mathcal{H}^-_0\) is the one dimensional Hilbert space of the vacuum state \(\ket{0}\).

\item Define \(c^{\dagger}\) and \(c\)
\begin{align*}
    & c^{\dagger}_{\alpha} \ket{n_1, ...\,, n_{\alpha}, ...\,, n_d} = (-1)^{N_{\alpha}} \sqrt{n_{\alpha}+1}\ket{n_1, ...\,,  n_{\alpha}+1, ...\,, n_d} \\
    & c_{\alpha} \ket{n_1, ...\,, n_{\alpha}, ...\,, n_d} = \left(c_{\alpha}^{\dagger}\right)^{\dagger} \ket{n_1, ...\,, n_{\alpha}, ...\,} = (-1)^{N_{\alpha}} \sqrt{n_{\alpha}}\ket{n_1, ...\,,  n_{\alpha}-1, ...\,, n_d}
\end{align*}
where \(N_{\alpha} = \sum_{\beta=1}^{\alpha-1} n_{\beta}\) is the number of particles before \(\alpha\) -th particle and \((-1)^{N_{\alpha}}\) then implies the anticommutation relations
\begin{align*}
    & [c_{\alpha}, c_{\beta}] = [c^{\dagger}_{\alpha}, c^{\dagger}_{\beta}] = 0 \\
    & [c_{\alpha}, c^{\dagger}_{\beta}] = \delta_{\alpha \beta}
\end{align*}
which respect the antisymmetric property of \(\ket{\phi_1 ...\, \phi_d}^{\!-}\).
Note that \(c^{\dagger}_{\alpha}: \mathcal{H}_N^- \to \mathcal{H}_{N+1}^-\), \(c_{\alpha}: \mathcal{H}_N^- \to \mathcal{H}_{N-1}^-\) and can be interpreted as creation and annihilation of a particle in the state \(\alpha\).
We can also write a state in the Fock space as in terms of a vacuum state and creation operators
\begin{align*}
    \ket{n_1, ...\,, n_d} = \frac{\left(c^{\dagger}\right)^{n_1}}{\sqrt{n_1!}} ...\, \frac{\left(c^{\dagger}\right)^{n_d}}{\sqrt{n_d!}} \ket{0}
\end{align*}
\end{itemize}

\clearpage

\section{2nd quantization}
\label{sec:org3f52c19}
\begin{itemize}
\item Consider one-paricle operator
\begin{align*}
    A = \sum_{i=1}^N A_1^{(i)}
\end{align*}
where \(A_1^{(i)}\) acts on the \(i\) -th particle.
An example is the kitetic energy of the Hamilton operator \(\sum_{i=1}^N \vec{p}_i^2/2m\).

\item We can insert the unity operators \(\mathbf{1}_{\mathcal{H}^-_N}\) of \(\mathcal{H}^-_N\)
\begin{align*}
    A &= \sum_{\alpha_1, \,...\,, \alpha_N} \sum_{\beta_1, \,...\,, \beta_N} \ket{\phi_{\alpha_1} ...\, \phi_{\alpha_N}}^{\!-} \; ^-\!\bra{\phi_{\alpha_1} ...\, \phi_{\alpha_N}} A \ket{\phi_{\beta_1} ...\, \phi_{\beta_N}}^{\!-} \; ^-\!\bra{\phi_{\beta_1} ...\, \phi_{\beta_N}} \\
      &= N \sum_{\alpha_1, \,...\,, \alpha_N} \sum_{\beta_1, \,...\,, \beta_N} \ket{\phi_{\alpha_1} ...\, \phi_{\alpha_N}}^{\!-} \; \bra{\phi_{\alpha_1}} A_1 \ket{\phi_{\beta_1}} \delta_{\alpha_2 \beta_2} ...\, \delta_{\alpha_N \beta_N} \; ^-\!\bra{\phi_{\beta_1} ...\, \phi_{\beta_N}} \\
      &= N \sum_{\alpha_1, \beta_1} \sum_{\alpha_2, \,...\,, \alpha_N} \frac{1}{\sqrt{N}} c^{\dagger}_{\alpha_1} \ket{\phi_{\alpha_2} ...\, \phi_{\alpha_N}}^{\!-}\; \bra{\phi_{\alpha_1}} A_1 \ket{\phi_{\beta_1}} \; ^-\!\bra{\phi_{\alpha_2} ...\, \phi_{\alpha_N}} \frac{1}{\sqrt{N}} c_{\beta_1}\\
      &= \sum_{\alpha_1, \beta_1} \mathbf{1}_{\mathcal{H}^{-}_{N-1}} c^{\dagger}_{\alpha_1} \bra{\phi_{\alpha_1}} A_1 \ket{\phi_{\beta_1}} c_{\beta_1}\\
\end{align*}
where we used in the second equation that each term in \(\bra{\phi_{\alpha_1}} A^{(1)}_1 \ket{\phi_{\beta_1}} \delta_{\alpha_2 \beta_2} ...\, \delta_{\alpha_N \beta_N} + ...\,  + \delta_{\alpha_2 \beta_2} ...\, \delta_{\alpha_{N-1} \beta_{N-1}}\bra{\phi_{\alpha_N}} A^{(N)}_1 \ket{\phi_{\beta_N}}\) has equal contribution and thus we omitted the upper particle index in the operator \(A_1^{(i)}\).
Thus
\begin{align*}
    A = \sum_{\alpha \beta} \bra{\phi_{\alpha}} A_1 \ket{\phi_{\beta}} c^{\dagger}_{\alpha} c_{\beta}
\end{align*}
that can be interpreted as annihilating a particle in the state \(\beta\) and creating a particle in the state \(\alpha\) with transition amplitude \(\bra{\phi_{\alpha}} A_1 \ket{\phi_{\beta}}\).
\item Similarly we can also derive the a two-paricle operator
\begin{align*}
    \frac{1}{2} \sum_{i,j=1,\,...\,N}^{i\neq j} A_2^{(ij)} = \frac{1}{2} \sum_{\alpha,\beta,\gamma,\delta} \bra{\phi_{\alpha} \phi_{\beta}} A_2 \ket{\phi_{\gamma} \phi_{\delta}} c^{\dagger}_{\alpha} c^{\dagger}_{\beta} c_{\delta} c_{\gamma}
\end{align*}
Note the order of indices of the creation and annihlation operators.
And the interpretation is accordingly annihilation two particles in states \(\delta\), \(\gamma\) and creation of two particles in states \(\alpha\), \(\beta\) with transion amplitude \(\bra{\phi_{\alpha} \phi_{\beta}} A_2 \ket{\phi_{\gamma} \phi_{\delta}}\).
An example of a two-particle operator is the Coulomb interaction
\begin{align*}
    \frac{1}{2} \sum_{i,j=1,\,...\,N}^{i\neq j} \frac{e^2}{4\pi \varepsilon_0} \frac{1}{|\vec{r}_i - \vec{r}_j|^2}
\end{align*}
\end{itemize}

\section{References}
\label{sec:org5fec3f1}
\end{document}
