:properties:
:ID:       754b4376-b02f-48a9-ab55-7afa04000606
:end:
#+title: Dimensionless density parameter
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

Let's consider the average density of the electron gas $n \coloneqq 1/r_{0}^{3}$ with $r_{0}$ denoting the average electron spacing.
The average potential energy is typically measured in units of $e^2/r_0$ and the kinetic energy in unit of $\hbar^2/m\,r^2_0$.
We can compare this two energy scales by considering the dimensionless density parameter
\begin{align*}
    r_s \coloneqq \frac{e^2}{r_0} \frac{m\,r_0^2}{\hbar} = \frac{r_0}{a_0}
\end{align*}
where $a_0 = \hbar/e^2m$ is the Bohr radius.
We can interprate $r_s$ as the radius of a sphere that on average contains one electron.
