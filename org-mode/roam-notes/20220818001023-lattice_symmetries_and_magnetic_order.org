:PROPERTIES:
:ID:       5497288f-1a44-4f5b-9851-a8c6ba7584ae
:END:
#+title: Lattice symmetries and magnetic order
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :physics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Lattice symmetries and regular magnetic orders in classical frustrated antiferromagnets [cite:@lattice-symmetries-magnetic-order-classical-frustrated-antiferromagnets]
** Introduction

- /In this study, we introduce and construct a family of spin configurations, dubbed “regular magnetic orders” (RMO’s)./
  /These configurations are those that respect all the symmetries of a given lattice modulo global spin transformations (rotations and/or spin flips)/
  /This property is obeyed by most usual Néel states./
- /In fact, rather surprisingly, we found that these states (together with spiral states) exhaust all the GS’s in a large range of parameters of the frustrated spin models we have investigated./
- /Once all the RMO’s have been constructed for given lattice and spin symmetries, one can directly compare their energies for a given microscopic Hamiltonian./
- /These states may also be used when analyzing experimental data on magnetic compounds where the lattice structure is known, but where the values (and range) of the magnetic interactions are not./
  /In such a case, the (equal-time) magnetic correlations—measured by neutron scattering can be directly compared to those of the RMO’s./
  /If these correlations match those of one RMO, this may be used, in turn, to get some information about the couplings./
- /Whereas the symmetric spin liquids do not break lattice symmetries (they are "liquids"), our RMO’s indeed break lattice symmetries but in a "weak" way./

** Theory
- /To construct an RMO compatible with some mapping $G \in \mathcal{G}^A$, one first chooses the direction of the spin on a site $i$./
  /Then, by applying all the transformations of $S_L$, we deduce the spin directions on the other sites./

** Frustrated antiferromagnets
*** Kagome lattice




#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
