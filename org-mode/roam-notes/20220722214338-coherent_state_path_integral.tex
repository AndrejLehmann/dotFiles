% Created 2022-07-31 Sun 16:33
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}\;}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\i}{\mathrm{i}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}}
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{Coherent state path integral}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Coherent state path integral},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.6)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\clearpage \tableofcontent \clearpage

\section{Coherent states \citeprocitem{1}{[1, Chap. 4.5]}, \citeprocitem{2}{[2]}, \citeprocitem{3}{[3]}}
\label{sec:org1dedd2b}

Define coherent state as eigenstate of a fermionic annihilation operator
\begin{align*}
    c_{\alpha} \ket{\eta} = \eta_\alpha \ket{\eta}
\end{align*}
(E.g. for \href{20210921215152-hubbard_model.org}{Hubbard model} there are \(4\) \href{20220412114729-grassmann_numbers.org}{Grassmann numbers} \(\{ \eta_{i\up}, \eta_{i\dn}, \eta^{ * }_{i\up}, \eta^{ * }_{i\dn} \}\) for each site \(i\) so \(\ket{\eta}\) we denote as \(\ket{ \{ \eta_{i\up} \eta_{i\dn} \eta^{ * }_{i\up} \eta^{ * }_{i\dn} \}}\).)
From that it follows immediately that
\begin{align*}
    c_{\alpha} c_{\alpha} \ket{\eta} = \eta_\alpha \eta_\alpha \ket{\eta} = 0
\end{align*}
since annihilating the same fermionic state is \(0\) and thus
\begin{align*}
    \eta_\alpha \eta_\alpha = 0
\end{align*}
Further we can deduce for coherent state of two fermions
\begin{align*}
    \eta_2 \eta_1 + \eta_2 \eta_1 = 0
\end{align*}
since \(\eta_{\alpha}\) are eigenvalues of \(c_{\alpha}\) and \(c_{\alpha} c_{\beta} + c_{\beta} c_{\alpha} = 0\).
Thus \(\eta_{\alpha}\) are \href{20220412114729-grassmann_numbers.org}{Grassmann numbers}.
And e.g. with \(\{ \eta_1,\, \eta_2,\, \eta^{*}_1,\, \eta^{ *}_2\}\) one can form algebra where the elements obey the rules of \href{20220412114729-grassmann_numbers.org}{Grassmann numbers} (numbers anti-commute and two of the same numbers are \(0\)).
The number of distinct products is then \(2^4\) (since one can only either choose or not choose each element).
The space of composite object e.g. for \(\{ \eta, \eta^{*} \}\) is spanned by \(\{1,\, \eta,\, \eta^{ *},\, \eta \eta^{ *}\}\) and the most general function of this is \(f(\eta,\eta^{ *}) = \alpha + \beta \, \eta + \gamma \, \eta^{ *} + \delta \, \eta \eta^{ *}\).
Especially is \(\e^{\eta} = 1 + \eta\), \(\e^{\eta + \eta^{ *}} = 1 + \eta + \eta^{ *}\) and \(\int \d \eta^{ *} \d \eta \; \e^{-a \eta^{ *} \eta} = a\).

The Grassmann coherent state is given by
\begin{align*}
    \ket{\eta} = \e^{-\sum_{\alpha} \eta_{\alpha} c^{\dagger}_{\alpha}} \ket{0} = \prod_{\alpha} (1 - \eta_{\alpha} c^\dagger_{\alpha})\ket{0}
\end{align*}
To proof this we first consider single state \(\alpha\)
\begin{align*}
    c_{\alpha} (1 - \eta_{\alpha} c^\dagger_{\alpha}) \ket{0} = \eta_{\alpha} \ket{0} = \eta_{\alpha} (1 - \eta_{\alpha} c^+_{\alpha}) \ket{0}
\end{align*}
where we used in the last equation that \(\eta_{\alpha} \eta_{\alpha} = 0\).
For multiple states we separate the state \(\alpha\) and use the above
\begin{align*}
    c_{\alpha} \ket{\eta} &= c_{\alpha} \prod_{\alpha} (1 - \eta_{\alpha}c^{\dagger}_{\alpha}) \ket{0} \\
                          &= \quad \prod_{\beta \neq \alpha} (1 - \eta_{\beta}c^{\dagger}_{\beta}) c_{\alpha} (1 - \eta_{\alpha}c_{\alpha}^{\dagger}) \ket{0} \\
                          &= \quad \prod_{\beta \neq \alpha} (1 - \eta_{\beta}c^{\dagger}_{\beta}) \eta_{\alpha} (1 - \eta_{\alpha}c_{\alpha}^{\dagger}) \ket{0} \\
                          &= \eta_{\alpha} \prod_{\alpha} (1 - \eta_{\alpha}c^{\dagger}_{\alpha}) \ket{0}
\end{align*}
where in the last equation we used that \(\eta_{\alpha}\) commutes with \(\eta_\beta c^\dagger_{\beta}, \beta \neq \alpha\).
The adjoint coherent state is correspondingly
\begin{align*}
    \bra{\eta} = \bra{0} \e^{-\sum_{\alpha} c_{\alpha} \eta_{\alpha}^{ * }}
\end{align*}
and
\begin{align*}
    \bra{\eta} c^{\dagger}_{\alpha} = \bra{\eta} \eta_{\alpha}^{* } \; .
\end{align*}
Similarly we can also derive the action of \(c_{\alpha}^{\dagger}\) on \(\ket{\eta}\)
\begin{align*}
    c^{\dagger}_{\alpha} \ket{\eta} &= c^{\dagger}_{\alpha} (1 - \eta_{\alpha}c^{\dagger}_{\alpha}) \prod_{\beta \neq \alpha} (1 - \eta_{\beta}c^{\dagger}_{\beta}) \ket{\eta} \\
                                    &= c^{\dagger}_{\alpha} \prod_{\beta \neq \alpha} (1 - \eta_{\beta}c^{\dagger}_{\beta}) \ket{\eta} \\
                                    &= - \frac{\partial}{\partial \eta_{\alpha}} (1 - \eta_{\alpha}c^{\dagger}_{\alpha}) \prod_{\beta \neq \alpha} (1 - \eta_{\beta}c^{\dagger}_{\beta}) \ket{\eta} \\
                                    &= - \frac{\partial}{\partial \eta_{\alpha}} \ket{\eta}
\end{align*}
and correspondingly the adjoint
\begin{align*}
    \bra{\eta} c_{\alpha}= \frac{\partial}{\partial \eta_{\alpha}} \bra{\eta} \;.
\end{align*}
Next we determine overlap of two coherent states
\begin{align*}
    \braket{\eta}{\eta'} &= \bra{0} \prod_{\alpha} (1 + \eta^{ * }_{\alpha} c^{\dagger}_{\alpha}) (1 - \eta^{ * }_{\alpha} c^{\dagger}_{\alpha})\ket{0} \\
                         &= \bra{0} \prod_{\alpha} \left( 1 - \eta'_{\alpha} c^{\dagger}_{\alpha} + \eta^{ * }_{\alpha} c_{\alpha} - \eta^{ * }_{\alpha} c_{\alpha} \eta'_{\alpha} c^{\dagger}_{\alpha} \right) \ket{0} \\
                         &= \bra{0} \prod_{\alpha} \left( 1 - \eta'_{\alpha} c^{\dagger}_{\alpha} + \eta^{ * }_{\alpha} c_{\alpha} + \eta^{ * }_{\alpha} \eta'_{\alpha} c_{\alpha} c^{\dagger}_{\alpha} \right) \ket{0} \\
                         &= \quad\; \prod_{\alpha} (1 + \eta^{ * }_{\alpha} \eta'_{\alpha}) = \e^{\sum_{\alpha} \eta^{ * }_{\alpha} \eta'_{\alpha}}
\end{align*}
And the matrix element is given by
\begin{align*}
    \bra{\eta} A(c_{\alpha}^{\dagger}, c_{\alpha}) \ket{\eta'} = \e^{\sum_{\alpha} \eta^{ * }_{\alpha} \eta'_{\alpha}} A(\eta^{ * }_{\alpha},\eta_{\alpha})
\end{align*}
where \(A(\eta^{*}_{\alpha},\eta_{\alpha})\) can be simply constructed from \(A(c_{\alpha}^\dagger, c_{\alpha})\) by substituting \(c^{\dagger}_{\alpha}\) by \(\eta^{\dagger}_{\alpha}\) and \(c_{\alpha}\) by \(\eta_{\alpha}\).
But note that e.g. the expectation value of number operator is not a real value
\begin{align*}
    \frac{\bra{\eta} \hat{N} \ket{\eta}}{\braket{\eta}{\eta}} = \sum_{\alpha} \eta_{\alpha}^{ * } \eta_{\alpha}
\end{align*}
Further we note that
\begin{align*}
\label{eq:braket-n-eta}
    & \braket{n_1, ...\, , n_k}{\eta} = \bra{0} c_k ...\, c_1 \ket{\eta} =\eta_k ...\, \eta_1 \braket{0}{\eta} = \eta_k ...\, \eta_1 \\
    & \braket{\eta}{n_1, ...\, , n_k} = \bra{\eta} c^{\dagger}_1 ...\, c^{\dagger}_k \ket{0} = \eta^{ * }_1 ...\, \eta^{ * }_k \braket{\eta}{0} = \eta^{ * }_1 ...\, \eta^{ * }_k \; .
\end{align*}
With that we can see that
\begin{align*}
\label{eq:braket-anti-commutation}
     \braket{n_1, ...\, , n_k}{\eta} \braket{\eta}{n_1, ...\, , n_k} &= \eta_k ...\, \eta_1  \eta^{ * }_1 ...\, \eta^{ * }_k \\
                                                                     &= \eta_1 \eta^{ * }_1 ...\, \eta_k \eta^{ * }_k \quad \text{(commute pairs of $\eta_i \eta^{ * }_i$)} \\
                                                                     &= (-\eta^{ * }_1 \eta_1) ...\, (-\eta^{ * }_k \eta_k) \\
                                                                     &= (-\eta^{ * }_1) ...\, (-\eta^{ * }_k) \eta_k ...\, \eta_1 \quad \text{(commute $\eta_i$ with pairs $\eta_j \eta^{ * }_j$)} \\
                                                                     &=  \braket{-\eta}{n_1, ...\, , n_k} \braket{n_1, ...\, , n_k}{\eta}
\end{align*}
where \(\bra{-\eta} = \bra{0} \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} c_{\alpha}}\).
We can use relations \ref{eq:braket-n-eta} to derive the closure relation
\begin{align*}
    \mathbb{1} = \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \; \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \ket{\eta} \bra{\eta}
\end{align*}
as shown in \citeprocitem{3}{[3]}.
For that we have to show that \(\braket{n_1, ...\,, n_k}{n'_1, ...\,, n'_k} = \bra{n_1, ...\,, n_k} \mathbb{1} \ket{n'_1, ...\,, n'_k}\)
\begin{align*}
    \bra{n_1, ...\,, n_k} \mathbb{1} \ket{n'_1, ...\,, n'_k} &= \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \braket{n_1, ...\,, n_k}{\eta} \braket{\eta}{n'_1, ...\,, n'_k} \\
                                                             &= \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \prod_{\alpha}(1 - \eta^{ * }_{\alpha} \eta_{\alpha}) \; \eta_k ...\, \eta_{1} \; \eta'^{ * }_1 ...\, \eta'^{ * }_{k} \\
                                                             &= \braket{n_1, ...\,, n_k}{n'_1, ...\,, n'_k}
\end{align*}
Alternatively one can also proof the closure relation via Schur's lemma \citeprocitem{1}{[1]}.
With this (overcomplete) closure relation we can define Grassmann coherent state representation \(\braket{\eta}{\psi} = \psi(\eta^{ * })\)
\begin{align*}
    \ket{\psi} = \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \psi(\eta^{ * }) \ket{\eta}
\end{align*}
At last we give the coherent state representation of creation and annihilation operators
\begin{align*}
    & \bra{\eta} c_{\alpha} \ket{\psi} = \frac{\partial}{\partial\eta_{\alpha}^{ * }} \psi(\eta^{ * }) \\
    & \bra{\eta} c^{\dagger}_{\alpha} \ket{\psi} = \eta^{ * }_{\alpha} \psi(\eta^{ * })
\end{align*}
and the anti-commutation relation
\begin{align*}
    \left\{ \frac{\partial}{\partial \eta^{ * }_{\alpha}}, \eta^{ * }_{\beta} \right\} = \delta_{\alpha\beta}
\end{align*}

In contrast to bosonic coherent states, that can be interpreted as ``most classical'' physical states, fermionic coherent states have no analogon and can not be interpreted as physical observable. \citeprocitem{3}{[3]}
But they provide a representation of path integrals.

\section{Gaussian integrals \citeprocitem{3}{[3]}}
\label{sec:orgedd41f2}

\emph{Matrix elements of the evolution operator give rise to Gaussian integrals in case of quadratic forms}
\emph{For a Hermitian operator \(H\), integrals of those form lead to}
\begin{align*}
    \int \prod_{i = 1}^N \d \eta_i^{ * } \d \eta_i \e^{-\eta_i^{ * } H_{ij} \eta_j + \theta^{ * }_i \eta_i + \theta_i \eta_i^{ * }} = \det(H) \e^{\theta^{* }_i H^{-1}_{ij} \theta_j}
\end{align*}
\emph{To prove this, one needs the transformation law under a change of variables and the formula for a Gaussian integral for Grassmann variables}
\emph{Gaussian integral for a single pair of conjugate Grassmann variables is}
\begin{align*}
    \int \d \eta^{* } \d \eta \e^{-\eta^{ * } a \eta} = \int \d \eta^{* } \d \eta (1 - \eta^{ * } a \eta) = a
\end{align*}

\section{Expectation value}
\label{sec:org97dc480}

\begin{align*}
    \Tr \hat{A} = \sum_n \bra{n} \hat{A} \ket{n} = \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \; \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \sum_n \braket{n}{\eta} \bra{\eta} \hat{A} \ket{n}
\end{align*}
where \(\ket{n}\) is short for \(\ket{n_1, ...\,, n_k}\).
In order to remove sum over \(\ket{n}\) by using resolution of identity \(\sum_n \ket{n} \bra{n} = \mathbb{1}\) we need to commute \(\braket{n}{\eta}\) to the right.
But one has to keep in mind the anti-commutation of Grassmann variables and use \ref{eq:braket-anti-commutation}.
So we obtain
\begin{align*}
\label{eq:expectation-value-coherent-states-represatation}
    \Tr \hat{A} = \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \; \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \bra{-\eta} \hat{A} \ket{\eta}
\end{align*}

\section{Partition function}
\label{sec:org49ec4bc}

\begin{align*}
    \mathcal{Z} = \Tr\e^{-\beta(\hat{H} - \mu \hat{N})} = \sum_n \bra{n} \e^{-\beta(\hat{H} - \mu \hat{N})} \ket{n}
\end{align*}
Using \ref{eq:expectation-value-coherent-states-represatation} we can represent the partition function in terms of coherent states
\begin{align*}
    \mathcal{Z} = \int \prod_{\alpha} \d \eta^{ * }_{\alpha} \d \eta_{\alpha} \; \e^{-\sum_{\alpha} \eta^{ * }_{\alpha} \eta_{\alpha}} \bra{-\eta} \e^{-\beta(\hat{H} - \mu \hat{N})} \ket{\eta}
\end{align*}

\clearpage

\section{References}
\label{sec:org4ec67c8}
\hypertarget{citeproc_bib_item_1}{[1] A. Altland and B. D. Simons, \textit{Condensed matter field theory}. Cambridge university press, 2010.}

\hypertarget{citeproc_bib_item_2}{[2] V. Shenoy, “Review many body field theory iii.” https://www.youtube.com/watch?v=M-dHK4OCer8\&t=3714s, 2014.}

\hypertarget{citeproc_bib_item_3}{[3] Miksch Björn, “Quantum field-theory of low dimensional systems, coherent states for fermions.” https://www.itp3.uni-stuttgart.de/downloads/talks/04.pdf, 2014.}
\end{document}
