:properties:
:ID:       64f63790-fdd3-42ca-a4a3-9239137c9c6e
:end:
#+title: Wannier functions
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export


* Wannier funtions [cite: [[file:~/literature/physics/WannierFunctions_Kunes.pdf][Wannier functions - Kunes]]]
** What for

- Connection between the calculations within first-principles band structure and the second quantization based model theories (LDA+DMFT).
- /considering only the short-range part of the electron-electron interaction explicitly and expressing the interaction in terms of localized orbitals reduces the number of non-zero terms considerably/ (tight binding)
- [[file:~/literature/physics/dualTheory/PhDThesis_DanielHirschmeier/Daniels_Thesis.pdf][Daniel's Ph.D. thesis]] :
    /The Wannier basis is adopted within the tight binding approximation,/
    /which is suited for systems where the electronic wave function of the solid is related to the electronic wave functions of single atoms./
    /This picture describes electrons, which are tightly bound to the ionic core as the name suggests and Coulomb interactions play an important role as a consequence./
    /It is hence an appropriate starting point for the display of the simplest and most frequently considered models for correlated electrons in solids, namely the Hubbard model and the Anderson impurity model which are introduced in the following sections./
    + Given a basis (Bloch states or Wannier functions) $\{\ket{\psi_{\alpha}}\}$ one can e.g. calculate the hopping matrix elements $t_{\alpha,\beta} = \bra{\psi_{\alpha}} \hat{\vec{p}}/2m + V(\hat{\vec{r}}) \ket{\psi_{\beta}$
    + In solids where the wave functions of the valence electrons are wide spread like the s- or p-shell electrons, the long ranged Coulomb interaction between the electrons is well screened and the electrons can be considered as non-interacting to a good approximation.
      Then the Bloch functions are well suited as orthogonal one particle basis.
      But when the wave functions of the valence electrons are rather localized, the Coulomb interaction is not well screened and can not be neglected.
    + Thus Wannier function are used within the tight binding approximation and the Hubbard model with electrons bound close to the ions and sizable Coulomb interaction between them.
      Exponentially localized Wannier functions is then a better choice for orthogonal one particle basis.
- Connection between Bloch states and their topological properties (topological insulators) and the spatial localization of Wannier functions.

*** Wannier functions as localized basis

- Wannier functions mediate the transition from atomic orbitals (localized) to [[id:6c509123-4f53-4b34-a707-69ed506ad76e][Bloch states]] (extended)
- Conceptually close to local physical quantities like impurities, screened Coulomb interaction, composite excitations and local electronic correlations in general.
- Wannier on semiconductor physics:
    "It would no doubt be more satisfactory for insulating crystals, to discuss the Hamiltonian using atomic functions rather than Bloch functions.
     But this line of attack has been hampered by the fact that atomic functions are not orthogonal".
- Given an isolated band finding exponentially localized Wannier functions is equivalent to finding Bloch states that are periodic and analytic in the Brillouin zone.
  Note that the eigenvalue problem does not specify Bloch states uniquely, but only up to an arbitrary $\vec{ k}$ -dependend phase factor.
- Exponentially localized Wannier functions can be found under rather general conditions, but generally they don't exist for every hermitian periodic Hamiltonian.

** Orthogonalizing atomic orbitals

Given the problem of a crystal with single atoms at lattice vectors $\vec{R}$ and each atom having a single valence orbital $v(\vec{r})$ thus yielding a basis $\{v(\vec{r} - \vec{R})\}_{\vec{R}}$ that is generally non-orthogonal.
So we are confronted with the task to orthogonalize the basis preserving the translational symmetry.
The classic Gram-Schmidt procedure would break the symmetry.
So we take the following procedure.
Fourier transform
\begin{align*}
    \phi(\vec{k},\vec{r}) = \frac{A(\vec{k})}{\sqrt{N}} \sum_{\vec{R}} \e^{i\vec{k} \vec{R}} v(\vec{r}-\vec{R})
\end{align*}
with the normalizing constant $A(\vec{k})$ defined as
\begin{align*}
    \frac{1}{A(\vec{k})^2} = \sum_{\vec{R}} \e^{i \vec{k}\vec{R}} \int d\vec{r} \; v^{* }(\vec{r}) \, v(\vec{r} - \vec{R}) \; .
\end{align*}
$\{ \phi(\vec{k},\vec{r}) \}_{\vec{k}}$ can be verified to be orthonormal.
Now taking the inverse Fourier transform we obtain the functions
\begin{align*}
    w(\vec{r},\vec{R}) = \frac{1}{\sqrt{N}} \sum_{\vec{k}} \e^{-i \vec{k} \vec{R}} \phi(\vec{k},\vec{r}) = \sum_{\vec{R'}} c(\vec{R} - \vec{R'}) \, v(\vec{r}-\vec{R'}) = w(\vec{r - R})
\end{align*}
with coefficients defined as
\begin{align*}
    c(\vec{R}) = \frac{1}{N} \sum_{\vec{k}} A(\vec{k}) \e^{-i \vec{k}\vec{R}} \; .
\end{align*}
Since $\{ \phi(\vec{k},\vec{r}) \}_{\vec{k}}$ is orthonormal $\{ w(\vec{r-R}) \}_{\vec{R}}$ is also orthonormal.
And if atomic orbitals $\{ v(\vec{r-R}) \}_{\vec{R}}$ are exponentially localized, $\{ w(\vec{r-R}) \}_{\vec{R}}$ are also exponentially localized.
So since orbital $\{ v(\vec{r-R}) \}_{\vec{R}}$ correspond to a single band we obtained an localized, translationally invariant orthonormal basis consisting of combinations of the original atomic functions centered at different lattice sites.
Note that we carried out an Fourier transform and it's inverse but did not obtain the bare original atomic orbitals.
This is due to $A(\vec{k})$ .
By investigating $A(\vec{k})$ in more detail we can see that it consists of overlaps of atomic orbitals.
It admixes the atomic orbitals from neighboring sites.
If there are no overlaps $A(\vec{k})=1$ (when the lattice constant is much larger than the Bohr radius $a \gg a_0$) $w(\vec{r-R}) = v(\vec{r-R})$ and thus we just use the linear combination of atomic orbitals.

Usually the atomic basis is not known and we need to construct the Wannier functions from Bloch states.

** Introducing Wannier functions
*** Isolated band
:PROPERTIES:
:ID:       60bc311b-0e19-4f3c-8e84-78c60ee8ddd3
:END:

Definition (Fourier series):
\begin{align*}
    w_n(\vec{r-R}) = \frac{V}{(2\pi)^d} \int_{\rm BZ} d\vec{k} \; \e^{-i\vec{k}\vec{R}} \; \psi_{n,\vec{k}}(\vec{r})
\end{align*}
where $d$ is dimension.
Wannier functions at different unit cells are orthogonal to each other due to the translational symmetry of the Bloch functions.
But the definition is not unique.
One can perform a gauge transformation $\psi_{n,\vec{k}}(\vec{r}) \to \e^{i\phi(\vec{k})}\psi_{n,\vec{k}}(\vec{r})$ and thus choose phases of the Bloch functions.
If the system is gaped $w_n$ will be exponentially localized right away. [cite:@electronic-structure-and-DFT]
This freedom allows us to find a basis of exponentially localized Wannier functions for the Hilbert space of the given band. [cite:@max-loc-wannier-func-for-composite-bands]
One can show that this is equivatent to finding Bloch states that are periodic and analytic in the $\vec{ k}$ -space.
The reverse transformation is then given by
\begin{align*}
    \psi_{n,\vec{k}}(\vec{r}) = \sum_{\vec{R}} \e^{i\vec{k}\vec{R}} \, w_n(\vec{r-R}) \, .
\end{align*}

*** Generalization to isolated composite bands

\begin{align*}
    \vec{w}(\vec{r-R}) = \frac{V}{(2\pi)^d} \int_{\rm BZ} d\vec{k} \; \vec{U}(\vec{k}) \vec{\psi}_{\vec{k}}(\vec{r})
\end{align*}
where we defined vectors
\begin{align*}
    \vec{w} = \begin{pmatrix} w_{n_{\rm min}} \\ \dots \\ w_{n_{\rm max}} \end{pmatrix} \;, \quad \vec{\psi_{\vec{k}}} = \begin{pmatrix} \psi_{n_{\rm min},\vec{k}} \\ \dots \\ \psi_{n_{\rm max},\vec{k}} \end{pmatrix} \, .
\end{align*}
And $U(\vec{k})$ is an unitary matrix acting on the band indices $n$ which is a generalization of the gauge transformation.
One can choose $U(\vec{k})$ such that $\vec{w}$ is exponentially localized [cite: @max-loc-wannier-func-for-composite-bands, @wannier90-package].
The quasi-inverse transformation is defined by
\begin{align*}
    \tilde{\psi}_{\vec{k}}(\vec{r}) = \sum_{\vec{R}} \e^{i\vec{k}\vec{R}} \, \vec{w}(\vec{r-R}) \; .
\end{align*}
The quasi Bloch functions $\tilde{\psi}_{\vec{k}}(\vec{r})$ are not eigenstates of the Hamiltonian but obey the translation property of the Bloch states $u_{n,\vec{k}}(\vec{r}) = u_{n,\vec{k}}(\vec{r+R})$.

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
