:PROPERTIES:
:ID:       d66a894b-6f84-4d82-9a68-feb543bb61fd
:END:
#+title: Markov-chain Monte Carlo and Metropolis Algorithm
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :numerics:math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Sources
- [[https://www.youtube.com/playlist?list=PLkHjVv59Oz56Yvki2LbjgD8qwt4evHWN-][Introduction to Markov Chains]] [cite:@Markov-chain-YT]
- [[https://www.youtube.com/playlist?list=PLkHjVv59Oz56gP4WciaL5kU_Y5CzYjxwv][Metropolis-Hastings Algorithm]] [cite:@Metropolis-Hastings-algorithm-YT]

* Further reading
- [[https://www.youtube.com/watch?v=sN_0iGWcyLI][Importance sampling, rejection sampling, Metropolis sampling, Gibbs sampling]]
- [[https://www.youtube.com/watch?v=Qr6tg9oLGTA][Slice sampling, Hamiltonian MC, overrelaxation, exact sampling]]

* Stochastic process

Indexed collection of random variables $\left\{ X_t : t \in T \right\}$.
The space of all possible $X_t$ is called the state space.

* Stationary stochastic process

[[Stochastic process]] is stationary if $P(X_{t+1} = j \,|\, X_t = i) = P(X_1 = j \,|\, X_0 = i), \;\forall\, t \in T$ and $i,j \in S$.
So the probability of transitioning from state $i$ to state $j$ is the same anywhere in the chain.

* Markov process

A [[Stochastic process][stochastic process]] that satisfies
\begin{align*}
    P(X_{t+1} = i_{t+1} | X_t = i_t, X_{t-1} = i_{t-1},\,...\,,X_0 = i_0) = P(X_{t+1} = i_{t+1} | X_t = i_t)
\end{align*}
Read $P(A|B)$ probability of event $A$ given event $B$.
And $X_t = i$ read as 'At time $t$, the random variable $X$ takes on value $i$'.
The probability of moving to state $i_{t+1}$ only depend on the current state $i_t$ (transition probabilities).

- Q: Are states $i_t$ in the physical context eigenstates?

* Accessible

State $j$ is accessible from state $i$ if the probability to go from $i$ to $j$ in some number of steps is not $0$.
We write $i \to j$

* Communicate

It holds [[Accessible][ $i \to j$ ]] and [[Accessible][ $j \to i$ ]] .
We write $i \leftrightarrow j$.

* communication class

States that [[Definition: communicate][communicate]] are said to be in the same communication class.
The state space is partitioned by communication classes.

* Period

Period $d(i)$ of the state $i$ is the greatest common divisor of all possible number of steps $n$ that return back to state $i$.
E.g. the number of steps are $\{2,4,6,\,...\}$ then period is $2$.
It holds that if $i \leftrightarrow j$ then $d(i) = d(j)$. So periodicity is a class property.
If for states $i$ in the class $d(i)=1$ then the class is called aperiodic.

* Markov chain

A [[Stationary stochastic process][stationary stochastic]] [[Markov process]].
If Markov chain is discrete the state space is countable.

- Q: So being stationary in the physical context means that the transition probabilities don't change with time.

* Irreducible

[[Markov chain]]  with one [[Communication class][communication class]] is called irreducible.
So all states are [[Communicate][communicating]].

* Probability of ever returning

Let $f^n_{ii} = P(X_0 = i , X_1 \neq i, X_2 \neq i,\,...\,X_n = i)$.
So $f^n_{ii}$ is the probability to return for the first time to state $i$ in $n$ steps.
Then the probability $f_{ii}$ of ever returning to state $i$ is
\begin{align*}
    f_{ii} = \sum^{\infty}_{n=1} f^n_{ii}
\end{align*}

* Expected time of returning

\begin{align*}
    \mu_{ii} = \sum_{n=1}^{\infty} n f_{ii}^n
\end{align*}

where $f^n_{ii}$ is [[Probability of ever returning][the probability to return for the first time to state $i$ in $n$ steps]].

* Transient

$f_{ii} < 1$ with $f_{ii}$ being the [[Probability of ever returning][probability of ever returning]].
Transient is a class property.

* Recurrent

$f_{ii} = 1$ with $f_{ii}$ being the [[Probability of ever returning][probability of ever returning]].
Recurrence is a class property.

** Positive recurrent

$\mu_{ii} < \infty$

** Null recurrent

$\mu_{ii} = \infty$

* Ergodicity

[[Markov chain]] is by definition ergodic if it is [[Period][aperiodic]] and [[Positive  recurrent][positive recurrent]].
So then there are no loops to be stuck in and we can surely expect the process eventually to return to any specific state.

* Stationary distribution

If a [[Markov chain]] is [[Irreducible][irreducible]] and [[Ergodicity][ergodic]] then $\pi_j = \lim_{n \to \infty} p^n_{ij}$, where $p^n_{ij}$ is the probability to transition from state $i$ to state $j$ in $n$ steps, exists, it is independent of $i$, it is the unique solution of the set of equations
#+name: eq:stationary-distribution
\begin{align*}
    \pi_j = \sum_{i} p_{ij} \pi_i \; \forall \, j
\end{align*}
$\sum \pi_j = 1$.
$\{\pi_j\}$ is called stationary distribution of the [[Markov chain]].

Analog [[eq:stationary-distribution][equation]] for continuous space is
\begin{align*}
    p(y) = \int p(x) \, T(x,y) \; \d x
\end{align*}
where $p(x)$ is a probability density function and $T(x,y)$ is transition density with $\int T(x,y) \,\d y = 1$.

* Metropolis Hasting Algorithm

** Idea

"So the method we employ is actually a modified Monte Carlo scheme, where, instead of choosing configurations randomly, then weighting them with $\exp(-E / k_{\rm B} T)$, we choose configurations with a probability $\exp( -E / k_{\rm B} T )$, and weight them evenly" [cite:@Metropolis-algorithm-original-paper]

Generate sample $X \sim f(x)$ (that stochastically represents the function of interest $f(x)$) from [[Markov chain]] with [[Stationary distribution][stationary distribution]] being the function of interest $f(x)$.

** The Metropolis Hastings algorithm

Use proposal density $q\,(y\,|\,x)$ with properties
1. easy to generate sampling from
2. same support as $f$
3. symmetric $q\,(x\,|\,y) = q\,(y\,|\,x)$ or it's functional form is known up to a normalizing constant

*** The algorithm

0. Start for $t=1$ with arbitrary $x \in X$
1. Propose a candidate $y \sim q(y\,|\,x_t)$
2. Accept/reject:
\begin{align*}
    X_{t+1} = \begin{cases}
                  y   &\text{with probability } \rho(x_t,y) \\
                  x_t &\text{with probability } 1-\rho(x_t,y)
              \end{cases}
\end{align*}
with
\begin{align*}
    \rho(x_t\,|\,y) = \min \left( 1, \frac{f(y)\,q(x_t\,|\,y)}{f(x_t)\,q(y\,|\,x_t)} \right)
\end{align*}
Not that the $x$ we start with is not important, since we will arrive at a stationary distribution.
Also the normalizing constant of $q$ cancels in $\rho$.
For symmetric $q$ the probability simplifies to $\rho(x_t\,|\,y) = \min \big( 1, f(y)/f(x_t) \big)$

**** Independence sampler $q(y\,|\,x_t) = q(y)$
\begin{align*}
    \rho(x_t\,|\,y) = \min \left( 1, \frac{f(y)\,q(x_t)}{f(x_t)\,q(y)} \right)
\end{align*}
So we have high acceptance rate if $f \approx q$.
But $f$ needs to be somewhat known in order to choose good $q$.

**** Random walk $y = X_t + Q$ with $Q \sim q$ and $q$ is symmetric

Proposed = current + random step.
We need not to know anything about $f$.
But the convergence depends on $q$.
If $q$ proposes only small steps the convergence will be slow.
If $q$ proposes big steps the rejection rate is large.
(This is even more complicated for high dimentional case. Then [in the case of a spherical $q$] you actually want to choose the step size as follows. Consider in each dimension for the width of $f$ where it has significant values and take the smallest width. So optimal step size is of order of the smallest length scale. Then about $50\%$ of proposals will be accepted. Otherwise the rate of rejections will be very large.)
Random walk has a rather slow progress.
We don't know for sure that we converged and can stop the process.


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
