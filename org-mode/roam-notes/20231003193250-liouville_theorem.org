:PROPERTIES:
:ID:       61013101-0afe-4fd1-9868-ec300e39b1eb
:END:
#+title: Liouville theorem
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :physics:statistical-physics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

The distribution function over the phase space when evolving in time keeps it's local density.
One can imagine it being stretched but not "compressed" so that the local density stays the same.

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
