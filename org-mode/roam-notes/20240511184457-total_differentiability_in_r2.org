:PROPERTIES:
:ID:       ab228441-7008-493c-98f8-afda79a79515
:END:
#+title: Total Differentiability in R2
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

$f: \mathbb{R}^{2} \to \mathbb{R}^{2}$  is called totally differentiable at $(x_0, y_0)^\mathrm{T} \in \mathbb{R}^{2}$ if there is [[id:4ec4cd56-e9e9-481a-8f23-5a77fbea3b99][Jacobian matrix]] $\mathrm{J} \in \mathbb{R}^{2 \times 2}$ and a map $\phi: \mathbb{R}^{2} \to \mathbb{R}^{2}$ with
\begin{align*}
  f \begin{pmatrix} \begin{pmatrix} x \\ y \end{pmatrix} \end{pmatrix} = \underbrace{ f \begin{pmatrix} \begin{pmatrix} x_0 \\ y_0 \end{pmatrix} \end{pmatrix} + \mathrm{J} \begin{pmatrix} x - x_0 \\ y - y_0 \end{pmatrix} }_{ \text{linear approximation} } + \phi \begin{pmatrix} \begin{pmatrix} x \\ y \end{pmatrix} \end{pmatrix}
\end{align*}
where
\begin{align*}
    \frac{\phi \begin{pmatrix} \begin{pmatrix} x \\ y \end{pmatrix} \end{pmatrix} }{\begin{Vmatrix} \begin{pmatrix} x \\ y \end{pmatrix} - \begin{pmatrix} x_0 \\ y_0 \end{pmatrix} \end{Vmatrix} } \; \xrightarrow{ \begin{pmatrix} x \\ y \end{pmatrix} \to \begin{pmatrix} x_0 \\ y_0 \end{pmatrix} } \; 0
\end{align*}
with Euclidean norm $\Vert \dots \Vert$

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
