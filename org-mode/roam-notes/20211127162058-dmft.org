:properties:
:ID:       e2cc5ab0-a415-4710-a762-9bb035d4cf86
:end:
#+title: DMFT
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

* DMFT - Potthoff
** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript12.pdf][Hubbard model in high dimensional limit]]

Let us consider as an example a $D$ -dimensional hyper cubic lattice (for large number of spacial dimensions is the form of lattice not relevant, what is relevant is the number of neighbors scales linearly with $D$) with nearest neighbor hopping $t>0$.
\begin{align*}
    H = -t \sum_i \sum_{j \in {\rm NN}(i)} \sum_{\sigma} \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} + \frac{U}{2} \sum_{i\sigma} \hat{n}_{i,\sigma} \hat{n}_{i,-\sigma}
\end{align*}
In order to keep physical quantities like kinetic energy or free DOS finite when $D \to \infty$ we need to rescale the hopping parameter $t=t^{ * }/\sqrt{D}$, where $t^{ * } = {\rm const}$.
No other scaling results in finite quantities for $D \to \infty$.
For other scalings the quantities become $0$ or $\infty$.
So there is no ambiguity.
One can show by an exhaustive discussion of diagrams that in order to obtain finite physical observables the non-local Green's function has to scale with $D$ as
\begin{align*}
    \left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle = O \left( \frac{1}{\sqrt{D}} \right)
\end{align*}
for $i$ and $j$ being nearest neighbors.
In order to make it plausible consider energy per site
\begin{align*}
    \frac{\left\langle H \right\rangle}{L} = \underbrace{ -t }_{ O(1/\sqrt{D}) } \;\; \sum_{j \in {\rm NN}(i)} \sum_{\sigma} \left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle + \underbrace{ \frac{U}{2} \sum_{\sigma} \left\langle \hat{n}_{i,\sigma} \hat{n}_{i,-\sigma} \right\rangle }_{ O(1) }
\end{align*}
Since the number of nearest neighbors is proportional to $D$ the term $\left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle$ should scale as $O(1/\sqrt{D})$ in order for $\langle H \rangle/L$ to stay finite for $D \to \infty$.
Thus also the interplay between the kinetic energy and interaction remains non-trivial.
But for the local Green's function in the limit $D \to \infty$ holds
\begin{align*}
    G_{ii}(\omega) = O(1)
\end{align*}
as for the scaling of the local self energy
\begin{align*}
    \Sigma_{ii}(\omega) = O(1)
\end{align*}
For the non-local self energy
\begin{align*}
    \Sigma_{ij}(\omega) = O \left( \frac{1}{\sqrt{D}^3} \right)
\end{align*}
where $i$ and $j$ are nearest neighbors.
Note the non-trivial energy dependence in contrast to [[id:02995f52-921c-43d5-b6fe-dd7befe17ba0][mean field theories]] and the $\vec{k}$ -independence for the local self-energy and Green's function.

The diagrams can be discussed starting from the equation of motion for free one particle Green's function
\begin{align*}
    \left( \omega - \mu \right) G^{(0)}_{ii} (\omega) = 1 + \sum_{j}  t_{ij} G^{(0)}_{ji}(\omega)
\end{align*}
But first we observe that since the free DOS is proportional to $\Im\;G^{(0)}_{ii}(\omega+i0^+)$ the free local one particle Green's function has to be of order $O(1)$ so that the free DOS is finite.
Then from the equation of motion we see that e.g. for nearest neighbors Green's function $\sum_{j}  t_{ij} G^{(0)}_{ji}(\omega) = -t \, q \, G_{\rm NN}^{(0)}(\omega)$ has to scale with $O(1/\sqrt{D})$ since $t$ scales as $O(1/\sqrt{D})$ and $q$ as $D$.
In the same way one can deduce for the general case
\begin{align*}
    G^{(0)}_{ij}(\omega) = O\left(\frac{1}{D^{\frac{1}{2}\parallel i - j \parallel}}\right)
\end{align*}
where $\parallel ... \parallel$ is the Manhattan metric.

So in conclusion in order to obtain finite self-energy and Green's function only local diagrams have to survive the limit $D \to \infty$.
Compared to [[id:02995f52-921c-43d5-b6fe-dd7befe17ba0][mean field theories]] which are also exact in the limit of infinite dimensions DMFT only neglects spacial fluctuation (thus $\vec{k}$ -independence) but retains the temporal fluctuations.

** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript14.pdf][SIAM]]

#+name: eq:H_SIAM_Potthoff
\begin{align*}
    H_{\rm SIAM} = &\quad\sum_{\sigma} \varepsilon_{\rm imp} \; \hat{c}^{\dagger}_{\sigma} \hat{c}_{\sigma} + \frac{U}{2} \sum_{\sigma} \hat{n}_{\sigma} \hat{n}_{-\sigma} \quad \text{(correlated impurity)} \\
                 &+ \sum_{\vec{k}} \hat{a}^{\dagger}_{\vec{k}\sigma} \hat{a}_{\hat{k}\sigma} \quad \text{(uncorrelated bath)} \\
                 &+ \sum_{\vec{k}\sigma} V_{\vec{k}}(\hat{a}^{\dagger}_{\vec{k}\sigma} \hat{c}_{\sigma} + \hat{c}^{\dagger}_{\sigma} \hat{a}_{\vec{k}\sigma}) \quad \text{(hybridisation)}
\end{align*}

For $V_{\vec{k}}=0$ the impurity and the bath are decoupled.
Then the energy spectrum of the impurity are just $\delta$ -peaks at $\varepsilon_{\rm imp} - \mu$ and $\varepsilon_{\rm imp} - \mu + U$.
(And the energy spectrum of the bath are bands obtained from the free dispersion.)
By turning $V_{\vec{ k}}$ on, the $\delta$ -peaks broaden.

In order to derive the interacting Green's function of the SIAM $G_{\rm SIAM}$ let's first consider the non-interacting case with $U=0$.
Then the Dyson equation is perturbative expansion in $V_{\vec{ k}}$
\begin{align*}
    G^{(0)}_{\rm SIAM}(\omega,\vec{k}) = G_{\rm imp}(\omega,\vec{k}) = G^{(0)}_{\rm imp}(\omega) \;\;+\;\; G^{(0)}_{\rm imp}(\omega)\;V_{\vec{k}}\;G^{(0)}_{\rm bath}(\omega,\vec{k})\;V_{\vec{k}}\;G^{(0)}_{\rm SIAM}(\omega,\vec{k})
\end{align*}
where
\begin{align*}
    G^{(0)}_{\rm imp}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp}}
\end{align*}
the free impurity Green's function and
\begin{align*}
    G^{(0)}_{\rm bath}(\omega,\vec{k}) = \frac{1}{\omega + \mu - \varepsilon_{\vec{k}}}
\end{align*}
the free bath Green's function.
This Dyson equation describes processes where the particle can hop from the impurity into bath, propagate in the bath and hop back.
Note that since we set $U=0$ there are no correlation effects induced by interaction $U$.
Here we can define hybridisation function
\begin{align*}
    \Delta(\omega) = \sum_{\vec{k}} V_{\vec{k}}\;G^{(0)}_{\rm bath}(\omega,\vec{k})\;V_{\vec{k}} = \sum_{\vec{k}} \frac{V_{\vec{k}}^2}{\omega + \mu - \varepsilon_{\vec{k}}}
\end{align*}
Then the solution of the non-interacting Dyson equation can be written as
\begin{align*}
    G^{(0)}_{\rm SIAM}(\omega) = G_{\rm imp}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp} - \Delta(\omega)}
\end{align*}
Now we can also write the Dyson equation for the interacting Green's function
\begin{align*}
    G_{\rm SIAM}(\omega) = G^{(0)}_{\rm SIAM}(\omega)\;+\;G^{(0)}_{\rm SIAM}(\omega)\;\Sigma_{\rm imp}(\omega)\;G_{\rm SIAM}(\omega)
\end{align*}
which is solved by
\begin{align*}
    G_{\rm SIAM}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp} - \Delta(\omega) - \Sigma_{\rm imp}(\omega)}
\end{align*}

** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript15.pdf][Mapping the Hubbard model in infinity spacial dimensions on the SIAM]]

The self energy of the Hubbard model in the limit $D \to \infty$, which is purely local, can be written as diagrammatic expansion with interacting Green's functions as propagators which are also purely local.
So the self energy written as functional is
\begin{align*}
    \Sigma^{\rm Hub} = \Sigma_{\rm loc}(\omega)\left[ G_{ii}(\omega) \right]
\end{align*}
The same is also true for SIAM
\begin{align*}
    \Sigma_{\rm imp} = \Sigma_{\rm loc}(\omega) \left[ G_{\rm SIAM}(\omega) \right]
\end{align*}
So in order to solve the Hubbard model in the limit of infinite dimensions one can solve the corresponding SIAM, which can be done numerically.
In order to map the $D \to \infty$ Hubbard model on the SIAM the parameters of the impurity problem we simply set $\varepsilon_{\rm imp} = t_{ii}$, set the free dispersion of the Hubbard model equal to the free dispersion of the bath, and determine the hybridisation self consistently.
The self consistent cycle is [[fig:DMFT-self-consistent-cycle]].
#+caption: DMFT self consistent cycle
#+attr_org: :width 800
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:DMFT-self-consistent-cycle
[[./images/DMFT-self-consistent-cycle.png]]
with the self consistency condition
\begin{align*}
    G_{ii}(\omega) = G_{\rm SIAM}(\omega)
\end{align*}
which ensures internal consistency of the theory.
The key approximation is here
\begin{align*}
   \Sigma_{ij}(\omega) = \delta_{ij} \Sigma_{ii}(\omega)
\end{align*}
The numerical solution of the SIAM is the challenging part of the loop.


* Hybridization expansion impurity solver - Philipp Werner [cite:@ctqmc-for-quantum-impurity-model-Philipp-Werner; @ctqmc-Philipp-Werner]

\begin{align*}
    H &= H^{\rm loc} + H^{\rm bath} + H^{\rm hyb} \\
    H^{\rm loc}  &= U n_{\up} n_{\dn} - \mu(n_{\up} + n_{\dn}) \\
    H^{\rm bath} &= \sum_{p,\sigma} \varepsilon_p c^{\dagger}_{p\sigma} c_{p\sigma} \\
    H^{\rm hyb}  &= \sum_{p,\sigma} (V^{ * }_{p\sigma} c^{\dagger}_{p\sigma} d_{\sigma} + {\rm h. c.}) \\
\end{align*}
where $p$ are quantum numbers of the bath.
(Compared to [[eq:H_SIAM_Potthoff][Potthoff's notation]] $\varepsilon_{\rm imp} = \mu$.)
We approach the solution of the problem by writing the partition function in [[id:5eca5437-1571-448c-a335-c30e8c02d0cc][Interaction / Dirac picture]], where the time evolution of operators is given by $H^{\rm loc}$ and $H^{\rm bath}$, and expand the partition function in powers of the hybridization part.
Then we sample the configuration of the expansion stochastically via a Monte Carlo procedure.
As mentioned in interaction represention the time dependence of an operator $O(\tau)$ is expressed as
\begin{align*}
    O(\tau) = e^{\tau (H^{\rm loc} + H^{\rm bath})} \; O \; e^{-\tau(H^{\rm loc} + H^{\rm bath})}
\end{align*}
The partition function is given by
\begin{align*}
    Z &= \Tr_{\!d} \, \Tr_{\!c} \; e^{-\beta(H^{\rm loc} + H^{\rm bath})} \; \mathcal{T}_{\tau} \, \exp\left(-\int_0^{\beta} \d \tau H^{\rm hyb}(\tau)\right) \\
      &= \sum_{n=0}^{\infty} \; \int_0^{\beta} ... \int_0^{\beta} \d \tau_1 \,...\; \d \tau_{2n} \; \frac{1}{2n!}\; \Tr_{\!d} \, \Tr_{\!c} \; e^{-\beta(H^{\rm loc} + H^{\rm bath})} \; \mathcal{T}_{\tau} \, (-H^{\rm hyb}_{\tau_{2n}}) \,...\, (-H^{\rm hyb}_{\tau_{1}})
\end{align*}
where $\Tr_{\!d}$ and $\Tr_{\!c}$ is trace over the impurity and bath degrees of freedom and $\mathcal{T}_{\tau}$ is time ordering operator for imaginary time $\tau$.
(There are no $\frac{1}{2n!}$ coefficients when the integrals are time ordered $\int_0^{\beta} ... \int_0^{\beta} \d \tau_1 \,...\; \d \tau_{2n} \to \int_0^{\beta} \d \tau_1 \,... \int_{\tau_{2n-1}}^{\beta} \d \tau_{2n}$.)
/The expansion orders are even because each hybridization operator changes the number of electrons on the impurity by plus or minus one so in order for the trace to be non-zero we need to get back to the same number of electrons on the impurity which is only possible for even number of hybridization operators./
To be more precise the number of the hybridization terms in the expansion containing $d$ operator ($V^{ * }_{p\sigma}c^{\dagger}_{p\sigma}d_{\sigma}$) should be equal to the number containing the $d^{\dagger}$ operator ($V_{p'\sigma}c_{p'\sigma}\,d^{\dagger}_{\sigma}$).
(So $n$ terms of each.)
/Furthermore since the time evolution given by $\e^{-\beta(H^{\rm loc}+H^{\rm bath})}$ does not contain any spin flip terms we need the same number of terms with $d$ as with $d^{\dagger}$ for each spin $\sigma$./
For the sampling we can see that the sum over the expansion order and the integrals $\sum_{n=0}^{\infty} \; \int_0^{\beta} ... \int_0^{\beta}$ define the configuration space $\mathcal{C}$ with configurations $\mathit{c}$ consisting of $2n$ time points on the imaginary time interval from $0$ to $\beta$
\begin{align*}
    \mathcal{C} = \Big\{ \underbrace{ \tau_{1\up} \,...\, \tau_{n_{\up}\up} }_{n_{\up}\times d_{\up}} \,;\, \underbrace{ \tau'_{1\up} \,...\, \tau'_{n_{\up}\up} }_{n_{\up}\times d^{\dagger}_{\up}} \Big\vert \underbrace{ \tau_{1\dn} \,...\, \tau_{n_{\dn}\dn} }_{n_{\dn}\times d_{\dn}} ; \underbrace{ \tau'_{1\dn} \,...\, \tau'_{n_{\dn}\dn} }_{n_{\dn}\times d^{\dagger}_{\dn}} \Big\}
\end{align*}
So there are $n_{\up}$ imaginary time points associated with $d_{\up}$, $n_{\up}$ time points associated with $d^{\dagger}_{\up}$ and so on.
The corresponding weight of the configuration $w_\mathit{c}$ is then the contribution $\d \tau_1 \,...\; \d \tau_{2n} \; \Tr_{\!d} \, \Tr_{\!c} \; e^{-\beta(H^{\rm loc} + H^{\rm bath})} \; \mathcal{T}_{\tau} \, (-H^{\rm hyb}_{\tau_{2n}}) \,...\, (-H^{\rm hyb}_{\tau_{1}})$ where the trace evaluates to a number.
So we can construct a Monte Carlo procedure which stochastically samples the time points according to their weight.
Without going to much into details we give a picture of how the sampling procedure can be imagined.
Consider an imaginary time interval from $0$ to $\beta$ and a given expansion order $n$ which determines the number of operators that create and annihilate electrons on the impurity at times on this interval.
\begin{align*}
    & 0\,| \hbox{\sout{$\hphantom{\qquad\qquad}$}} \!\!\! \overset{c_{\up,p'\up}}{ \underset{d^{\dagger}_{\up}}{\oplus} } \!\!\! \underbrace{ \hbox{\sout{$\hphantom{\qquad\qquad\qquad}$}} }_{ \underset{\e^{-\beta(H^{\rm loc}+H^{\rm bath})}}{\longrightarrow}} \!\! \overset{c^{\dagger}_{\dn,p\dn}}{ \underset{d_{\dn}}{\boxdot} } \! \hbox{\sout{$\hphantom{\qquad}$}} \!\! \overset{c^{\dagger}_{\up,p\up}}{ \underset{d_{\up}}{\odot} } \!\! \hbox{\sout{$\hphantom{\quad\qquad}$}} \!\! \overset{c_{\dn,p'\dn}}{ \underset{d^{\dagger}_{\dn}}{\boxplus} } \!\!\hbox{\sout{$\hphantom{\qquad}$}} |\,\beta
\end{align*}
where squares stand for electrons on the impurity with spin down, circles for spin up, dots for annihilation impurity operators and pluses for creation impurity operators.
Now note that each creation (annihilation) operator of the impurity is accompanied by an annihilation (creation) operator of the bath.
They are accordingly depicted above the markers, where the apostrophe at the bath quantum number is associated with impurity creation operator.
So this shows processes of electrons hopping from bath onto impurity and vice versa.
After the expansion in powers of hybridization there is no coupling between impurity and bath anymore.
Especially the time propagation has no coupling between impurity and bath.
So we can separate the impurity operators from the bath operators and accordingly separately calculate traces of the impurity and bath degrees of freedom.
Let us also depict it schematically
\begin{align*}
    & \Tr_{\!\!d}\; \Big[ | \hbox{\sout{$\hphantom{\quad}$}} \oplus \underbrace{ \hbox{\sout{$\hphantom{\quad\qquad}$}} }_{ \underset{\e^{-\Delta\tau H^{\rm loc}}}{\longrightarrow}} \boxdot \; \hbox{\sout{$\hphantom{\quad}$}} \odot \hbox{\sout{$\hphantom{\quad}$}} \boxplus \hbox{\sout{$\hphantom{\quad}$}} | \Big] \\
    & \Tr_{\!\!c}\; \Big[ \prod_{\sigma} \sum_{p\,p'} V^{* }_{p_1\sigma} V_{p'_1\sigma} \,...\, V^{* }_{p_n\sigma} V_{p'_n\sigma} \;\;| \hbox{\sout{$\hphantom{\quad}$}} \oplus \underbrace{ \hbox{\sout{$\hphantom{\qquad\qquad}$}} }_{ \underset{\e^{-\Delta\tau H^{\rm bath}}}{\longrightarrow}} \boxdot \; \hbox{\sout{$\hphantom{\quad}$}} \odot \hbox{\sout{$\hphantom{\quad}$}} \boxplus \hbox{\sout{$\hphantom{\quad}$}} | \Big]
\end{align*}
Besides the partition function of the bath $Z^{\rm bath}$ in the denominator, which we can provide simply by expanding the expression with $Z^{\rm bath} / Z^{\rm bath}$, the trace over bath degrees of freedom is an expectation value of a n-point correlation function of a non-interacting system.
Thus we can evalutate it via [[id:a2a95c6b-32a1-4c8f-8283-f6b1885a65a8][Wick's theorem]].
As we will see later this will result in a computation of an determinant.
Let us first consider the lowest order contribution with $n_{\sigma} = 1$ and $n_{-\sigma} = 0$
#+name: eq:trace-over-bath
\begin{align*}
    & \frac{1}{Z^{\rm bath}} \sum_{p_1\,p'_1} V^{* }_{p_1} V_{p'_1} \;\Tr_{\!\!c} \left[ \e^{-\beta H^{\rm bath}} \; \mathcal{T}_{\tau} \; c^{\dagger}_{p_1\sigma}(\tau_{1\sigma}) \, c_{p'_1\sigma}(\tau'_{1\sigma}) \right] \\
    & = \sum_{p_1}  \frac{|V_{p_1}|^2}{\e^{-\varepsilon_1\beta}+1} \times
  \begin{cases}
     \quad \e^{\;-\varepsilon_{p_1} \left( \beta - (\tau_{1\sigma} - \tau'_{1\sigma}) \right)} \quad \text{for} \; \tau_{1\sigma} > \tau'_{1\sigma} \\
         - \e^{\;-\varepsilon_{p_1} \left( \tau'_{1\sigma} - \tau_{1\sigma} \right)} \quad\qquad \text{for} \; \tau_{1\sigma} < \tau'_{1\sigma}
  \end{cases}
\end{align*}
where we used that bath is an [[id:0dab3807-61b1-4736-819a-4923adb84945][non-interacting system and thus it's partition function]] is given by $Z^{\rm bath} = \prod_{\sigma} \prod_p \left( \e^{-\varepsilon_p \beta} + 1 \right)$ and in order for the trace to be non-zero the two bath quantum numbers $p_1$ and $p'_1$ have to be equal.
The upper case in the expression above corresponds to occupation where the impurity is occupied on the whole time interval from $0$ to $\beta$ except for the time interval $(\tau_{1\sigma} - \tau'_{1\sigma})$.
And the lower case corresponds to occupation where the impurity is occupied only on the time interval $(\tau'_{1\sigma} - \tau_{1\sigma})$.
Also the minus sign in the lower case arises due to time ordering since we have to anticommute $c^{\dagger}_{p_1\sigma}(\tau_{1\sigma})$ and $c_{p'_1\sigma}(\tau'_{1\sigma})$.
When we Fourier transform [[eq:trace-over-bath]] we obtain
\begin{align*}
    \sum_{p_1} \frac{|V_{p_1}|^2}{\i \omega_n - \varepsilon_{p_1}} = \Delta(\i\omega_n)
\end{align*}
which is the hybridization function.
Thus [[eq:trace-over-bath]] is the hybridization function in imaginary time domain.
For the general case
\begin{align*}
    \frac{1}{Z^{\rm bath}} \Tr_{\!\!c} \left[ ... \right] = \prod_{\sigma} \det \mat{M}^{-1}_{\sigma}
\end{align*}
where $\left( \mat{M}^{-1}_{\sigma} \right)_{ij} = \Delta_{\sigma}(\tau'_i - \tau_j)$ and the times $\tau'_i$ and $\tau_j$ are associated with impurity creation and annihilation operators $d^{\dagger}(\tau'_i)$ and $d(\tau_j)$.
The weights are given by
\begin{align*}
    w_{\mathit{c}} = \Tr_{\!\!d} \left[ \e^{-\Delta\tau H^{\rm loc}} \mathcal{T} \prod_{\sigma} d_{\sigma}(\tau_{n_\sigma\sigma}) d^{\dagger}_{\sigma}(\tau'_{n_\sigma\sigma}) \,...\, d_{\sigma}(\tau_{1\sigma}) d^{\dagger}_{\sigma}(\tau'_{1\sigma})  \right] \times Z^{\rm bath} \prod_{\sigma} \det \mat{M}^{-1}_{\sigma} \; (\d \tau)^{2n_{\sigma}}
\end{align*}
The calculation of the impurity contribution $\Tr_{\!\!d}$ we can depict by the so following segment picture
\begin{align*}
    & \up \; 0\,| \hbox{\sout{$\hphantom{\hspace{0.7cm}}$}} \underset{d^{\dagger}_{\up}}{\oplus} \overbrace{ \rule[1.75pt]{2.1cm}{2.0pt} }^{l^{\up}_{1}} \underset{d_{\up}}{\odot} \, \hbox{\sout{$\hphantom{\hspace{0.7cm}}$}} \underset{d^{\dagger}_{\up}}{\oplus} \overbrace{ \rule[1.75pt]{1.4cm}{2.0pt} }^{l^{\up}_{2}} \underset{d_{\up}}{\odot}  \; \hbox{\sout{$\hphantom{\hspace{0.7cm}}$}} |\,\beta \\
    & \hspace{2.6cm} | \hbox{\sout{$\hphantom{\hspace{0.4cm}}$}} \; l^{U}_{1} \hbox{\sout{$\hphantom{\hspace{0.4cm}}$}} | \hspace{1.6cm} |\hbox{\sout{$\hphantom{\hspace{0.1cm}}$}} \, l^{U}_{2} \hbox{\sout{$\hphantom{\hspace{0.1cm}}$}} | \\
    & \dn \; 0\,| \hbox{\sout{$\hphantom{\hspace{1.4cm}}$}} \underset{d^{\dagger}_{\dn}}{\boxplus} \underbrace{ \rule[1.75pt]{3.8cm}{2.0pt} }_{l^{\dn}_{1}} \underset{d^{\dagger}_{\dn}}{\boxdot} \, \hbox{\sout{$\hphantom{\;\;\hspace{1.1cm}}$}} |\,\beta \\
\end{align*}
where thick lines represent time intervals where the impurity is occupied by an electron, which we call segments.
Recalling that $H^{\rm loc} = U n_{\up} n_{\dn} - \mu (n_{\up} + n_{\dn})$ we see the chemical potential contribution to the trace is given by $L^{\up} = \sum_i l^{\up}_i$ and $L^{\dn} = \sum_i l^{\dn}_i$ which are the total lengths of the segments for the spin up and spin down.
Similarly the interaction contribution is given by the sum of lengths of the overlaps between the spin up and spin down segments $L^U = \sum_i l_i^U$ since then the impurity is doubly occupied.
So we can simply calculate the impurity trace, which we name $w^{\rm loc}(\mathit{c})$ for a given configuration $\mathit{c}$, by
\begin{align*}
    \Tr_{\!\!d} \left[ \,...\, \right] = \mathit{s}\;\e^{\;\mu(L^{\up}+L^{\dn}) - \;UL^U} \stackrel{\rm def}= w^{\rm loc}(\mathit{c})
\end{align*}
where $\mathit{s}$ is permutation sign resulting from time ordering.

Now we can define our Monte Carlo sampling: Generation of all possible configuration of segments, which we perform by following updates.
Insertion of a segment:
\begin{align*}
    0\,| \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} \oplus \rule[1.75pt]{2.1cm}{2.0pt} \odot \, \hbox{\sout{$\hphantom{\hspace{3.55cm}}$}} |\,\beta \quad \to \quad 0\,| \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} \oplus \rule[1.75pt]{2.1cm}{2.0pt} \odot \, \hbox{\sout{$\hphantom{\hspace{0.7cm}}$}} \oplus \rule[1.75pt]{1.4cm}{2.0pt} \odot \; \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} |\,\beta
\end{align*}
and removal of a part of a segment:
\begin{align*}
    0\,| \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} \oplus \rule[1.75pt]{5.15cm}{2.0pt} \odot \, \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} |\,\beta \quad \to \quad 0\,| \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} \oplus \rule[1.75pt]{2.1cm}{2.0pt} \odot \, \hbox{\sout{$\hphantom{\hspace{0.7cm}}$}} \oplus \rule[1.75pt]{1.4cm}{2.0pt} \odot \; \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} |\,\beta
\end{align*}
Insertion of a segment corresponds to adding an operator pair $d^{\dagger}$, $d$ and removal to adding a $d$, $d^{\dagger}$ pair.
This updates to the configuration have to fulfill the detailed balance condition
\begin{align*}
    w(\mathit{c}) \, p(\mathit{c} \to \mathit{c'}) = w(\mathit{c'}) \, p(\mathit{c'} \to \mathit{c})
\end{align*}
where $p$ is probability of an update that we can split into proposal probability $p^{\rm prop}$ and acceptance probability $p^{\rm acc}$.
So we can also write the detailed balance condition as
\begin{align*}
    w(\mathit{c}) \, p^{\rm \,prop}(\mathit{c} \to \mathit{c'}) \, p^{\rm \, acc}(\mathit{c} \to \mathit{c'})= w(\mathit{c'}) \, p^{\rm \,prop}(\mathit{c'} \to \mathit{c}) \, p^{\rm \,acc}(\mathit{c'} \to \mathit{c})
\end{align*}
We can deduct this probabilities from defining the procedures of the updates.
For insertion of a segment we can choose randomly a time point from $0$ to $\beta$ for $d^{\dagger}$.
If it is inside a segment we reject it.
But if is not we can specify the size a time interval $l^{\rm max}$ where we can put $d$ operator to complete the pair, which is limited by the occurrence of a segment later in time
\begin{align*}
    0\,| \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} \oplus \underbrace{ \hbox{\sout{$\hphantom{\hspace{2.1cm}}$}} }_{l^{\rm max}} \oplus \, \rule[1.75pt]{1.4cm}{2.0pt} \odot \; \hbox{\sout{$\hphantom{\hspace{0.5cm}}$}} |\,\beta
\end{align*}
This procedure gives the proposal probability for the update of $n_{\sigma}$ segments to $n_{\sigma}+1$ segments (the configuration is characterized by the number of segments)
\begin{align*}
    p^{\rm \,prop}(n_{\sigma} \to n_{\sigma} + 1) = \frac{\d \tau}{\beta} \frac{\d \tau}{l^{\rm max}}
\end{align*}
The proposal probability for the opposite update, the removal of a random segment, is
\begin{align*}
    p^{\rm \,prop}(n_{\sigma} + 1 \to n_{\sigma} ) = \frac{1}{n_{\sigma} + 1}
\end{align*}
We can calculate the ratio of acceptance probabilities $R$ from ratios of proposal probabilities and weights
#+name: eq:acceptance-ratio-R
\begin{align*}
    R \stackrel{\rm def}= \frac{p^{\rm \, acc}(n_{\sigma} \to n_{\sigma} + 1)}{p^{\rm \, acc}(n_{\sigma} + 1 \to n_{\sigma})} = \underbrace{ \frac{p^{\rm \, prop}(n_{\sigma} + 1 \to n_{\sigma})}{p^{\rm \, prop}(n_{\sigma} \to n_{\sigma} + 1)} }_{\frac{\beta l^{\rm max}}{(n_{\sigma} + 1)(\d \tau)^{2}}} \underbrace{ \frac{w_{\mathit{c}}(n_{\sigma} + 1)}{w_{\mathit{c}}(n_{\sigma})} }_{ \frac{\det \mat{M}^{-1}_{n_{\sigma} + 1}}{\det \mat{M}^{-1}_{n_{\sigma}}} (\d \tau)^2 \frac{w^{\rm loc}(n_{\sigma} + 1)}{w^{\rm loc}(n_{\sigma})}}
\end{align*}
where the index of matrix $\mat{M}$ denotes the configuration (number of segments).
Now we can set acceptance probability for an insertion to $\min [R,1]$.
Analogously we can determine acceptance ration for the removal of a part of a segment, which results in the same equation [[eq:acceptance-ratio-R]]

Note that since the time is not discretized (continuous time) we need not to carefully control the discretization errors.
Also note that we expand in powers of hybridization but $V$ does not need to be small but can be of any value.
The value of $V$ does determine which perturbation orders contribute the most though.
For large $V$ high perturbations orders are more important than low orders and vice versa.
But the stochastic sampling does consider that.

* Towards a Predictive Theory of Strongly-Correlated Electron Materials - G. Kotliar [cite:@towards-a-predictive-theory-of-strongly-correlated-electron-materials]

- For DMFT to be exact in the limit of infinite dimensions means that it can describe physics correctly where $U$ and the kinetic energy are both of order $1$.
  So it can describe the physics of strong correlations.

* Dynamical mean-field theory [cite:@DMFT-from-infinite-dim-to-real-materials chap. 12.1.2]

- /[...] we can picture the situation as a given lattice site immersed in a structureless 'bath' of conduction electrons./
  /We can imagine that such a description becomes more and more accurate when the coordination number grows large./
  /Electrons can hop from the bath onto the impurity and back./
  /Because of the mutual repulsion when two electrons occupy the same site, the dynamics will strongly depend on the time spent on the impurity./
  /The Weiss field and Green function will therefore still have a non-trivial energy dependence./
  /[...] different lattice sites are completely decoupled in the DMFT description./
  /The motion of electrons in different parts of the lattice is no longer correlated./
  /We say that DMFT neglects spatial correlations./

- /The electronic self-energy may be expressed as a functional of the Green function./
  /The central approximation of DMFT is to assume that this functional is purely local and is a functional of the local Green function only: $\Sigma = \Sigma[G^{\rm loc}]$./
  /Under this assumption, we may write the DMFT lattice Green function in the form/
  \begin{align*}
    G^{\rm DMFT}(\vec{k},\nu) = \frac{1}{\i\nu + \mu - \epsilon_{\vec{k}} - \Sigma[G^{\rm loc}]}
  \end{align*}
  /The right-hand side is a functional of the local Green function $G^{\rm loc} = \frac{1}{N_{\vec{k}}} \sum_{\vec{k}} G^{\rm DMFT}(\vec{k},\nu)$/.
  /This is a complicated self-consistent problem. The unknown local Green function determines the self-energy, which in turn fixes the local Green function./
  /Even if we knew $G^{\rm loc}$, we still had to sum all diagrams for the local self-energy./
  /As shown by Georges and Kotliar, we can introduce a local effective quantum impurity model as a tool to accomplish this./

* Dynamical mean field theory [cite:@electrodynamics-corr-el-materials]

- /This technique is based on the one-particle Green’s function and is unique in its ability to treat quasiparticle excitations and atomiclike excitations on the same footing./
  /The dynamic transfer of spectral weight between the two is the driving force for the metal insulator transition in Hubbard-like models as well as in transition-metal oxides./
- /The accuracy of DMFT is based on the accuracy of the local approximation for the electron self-energy./
  /It becomes exact in the limit of infinite lattice coordination (large dimension) and is very accurate in describing the properties of numerous three-dimensional materials./
- /Diagrammatically, the DMFT approximation can be viewed as an approximation which sums up all local Feynman diagrams./
  /Hence, the mapping to the Anderson impurity problem can be viewed as a trick to sum all local diagrams./

* Introduction [cite:@intro-DMFT-and-correlated-materials]

Instead viewing the material in terms of bands, we view it as a collection of atoms on a lattice.
Then we focus on one atom and replace all other atoms by a reservoir of electrons.
An atom can be in different many-body states $\ket{0},\ket{\up},\ket{\dn},\ket{\up\dn}$.
And the reservoir supplies the atom with electron to make the transitions between states possible.
The medium (reservoir) has to be determined in a self consisting way since all atoms are equivalent (similar to mean field approach e.g. for [[id:29532b7d-0357-40be-9e0b-5248944e6881][Ising model]]).

- DMFT is exact in the band limit, in the atomic limit and in the large dimensional limit (large coordination number)
- DMFT assumes locality in an irreducible quantity (the self energy is local, not the Green's function)
- Good when the correlation length is short (large doping, high temperatures, strong frustrations)
- DFT+U is like DMFT but if one uses Hartree-Fock as the solver and the obtained self energy is frequency independent.
  But then we loose effects like satelites (Hubbard bands) and mass renormalization ($m_{\rm eff} \neq m_{\rm LDA}$) due to missing the frequency independence and temperature dependence.
- LDA+DMFT: We can combine LDA and DMFT by making the lattice Green's function in the LDA loop equal to the one in the DMFT loop.

#+begin_export latex
    \clearpage
#+end_export

* Further reading

[[id:6fa8d683-fa29-4404-94de-779fd794b15c][LDA+DMFT]]

* References
#+print_bibliography:
