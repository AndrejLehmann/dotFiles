:PROPERTIES:
:ID:       abf199ca-8dae-4f43-bad4-c900c4e1a773
:END:
#+title: Intuition Behind Matrices
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Linear Transformations and Matrices | Chapter 3, Essence of linear algebra [cite:@lin-trafo-and-matrices-3blue1brown-YT] :matrix_columns:

A matrix denotes a linear transformation.
You can view this as transforming or moving all vectors in the vector space.
The linear property ensures that origin does not move and all straight lines remain straight under transformation.
If you consider grid lines specifically, they remain parallel and evenly spaced.
Note that due to linearity for any vector $\vec{v}$ holds
\begin{align*}
    &\vec{v} =
    \begin{bmatrix}
        v_{1}\\
        v_{2}
    \end{bmatrix}
    = v_1 \cdot \vec{\hat{\imath}} + v_2 \cdot \vec{\hat{\jmath}} \\
    &L(\vec{v}) = v_1 \cdot L(\vec{\hat{\imath}}) + v_2 \cdot L(\vec{\hat{\jmath}})
               = v_1 \cdot
                \begin{bmatrix}
                    \hat{\imath}_{1}'\\
                    \hat{\imath}_{2}'
                \end{bmatrix}
                +v_2 \cdot
                \begin{bmatrix}
                    \hat{\jmath}_{1}'\\
                    \hat{\jmath}_{2}'
                \end{bmatrix}
\end{align*}
where $\vec{\hat{\imath}}$ and $\vec{\hat{\jmath}}$ are basis vectors and $\hat{\imath}_{1}'$, $\hat{\imath}_{2}'$, $\hat{\jmath}_{1}'$, $\hat{\jmath}_{2}'$ are coordinates of transformed $\vec{\hat{\imath}}$ and $\vec{\hat{\jmath}}$.
So we only need to know how $\vec{\hat{\imath}}$ and $\vec{\hat{\jmath}}$ are transformed to deduce the transformed version of $\vec{v}$.
And the last expression is actually a matrix-vector multiplication
\begin{align*}
    v_1 \cdot
        \begin{bmatrix}
            \hat{\imath}_{1}'\\
            \hat{\imath}_{2}'
        \end{bmatrix}
    +v_2 \cdot
        \begin{bmatrix}
            \hat{\jmath}_{1}'\\
            \hat{\jmath}_{2}'
        \end{bmatrix}
    =
        \begin{bmatrix}
            \hat{\imath}_{1}' & \hat{\jmath}_{1}'\\
            \hat{\imath}_{2}' & \hat{\jmath}_{2}'
        \end{bmatrix}
        \begin{bmatrix}
            \hat{v}_{1}\\
            \hat{v}_{2}
        \end{bmatrix}
\end{align*}
So you can denote a linear transformation by writing the transformed $\vec{\hat{\imath}}$ and $\vec{\hat{\jmath}}$ as columns.

* Matrix multiplication as composition | Chapter 4, Essence of linear algebra [cite:@matrix-multiplication-as-composition-3blue1brown-YT] :associativity:

Consider the associative rule of matrix multiplication $(\mat{C} \mat{B}) \mat{A} = \mat{C} (\mat{B} \mat{A})$.
If you apply each side on any vector the rule is obviously true
\begin{align*}
    (\mat{C} \mat{B}) \mat{A}\,\vec{x} = \mat{C} (\mat{B} \mat{A})\,\vec{x}
\end{align*}

* The determinant | Chapter 6, Essence of linear algebra [cite:@matrix-determinant-3blue1brown-YT] :inverse:null_space:rank:column_space:

The determinant determines the factor by which any area of the vector space scales.
Negative values mean that area is flipped by transformation.
One can see that if you observe how any area continuously becomes smaller by the transformation then becomes 0 then negative.
And the value of 0 means that transformation squeezes any area to a lower dimension (e.g. volume to an area or an area to a line).

* Inverse matrices, column space and null space | Chapter 7, Essence of linear algebra [cite:@inverse-matrices-column-space-null-space-3blue1brown-YT]

Following the intuition behind the determinants, you can see that if the determinant is 0, you can not invert the transformation, since infinitely many vectors were squeezed to a point of that lower dimension by the transformation.
So it is not an injective mapping.
Also the columns become linearly dependent, since they denote where the basis vectors land after transformation and now number of these vectors is larger than the dimensionality of the resulting space.
The number of dimensions of the column space (space spanned by the column of the matrix) is the rank of the matrix.
The matrix is full rank if by the transformation the column space is equal to the number of dimensions of the original vector space.
So the transformation does not reduce the dimensionality of the space.
The null space are all the vectors that are mapped to 0 by the transformation.
With the above intuition it's obvious that $\det(\mat{A} \mat{B}) = \det(\mat{A})\cdot \det(\mat{B})$.

* Change of basis | Chapter 13, Essence of linear algebra :matrix_transformation:

Consider basis vectors $\vec{a}_{1}$ and $\vec{a}_{2}$ and different basis vectors $\vec{b}_{1}$ and $\vec{b}_{2}$ for the same vector space.
Denoted in the coordinate system $A$, where $\vec{a}_{1}$ and $\vec{a}_{2}$ are orthonormal, these vectors are
\begin{align*}
    &\vec{a}^{A}_1 =
        \begin{bmatrix}
            1\\
            0
        \end{bmatrix} ,\,
    \vec{a}^{A}_2 =
        \begin{bmatrix}
            0\\
            1
        \end{bmatrix} \\
    &\vec{b}^{A}_1 =
        \begin{bmatrix}
            2\\
            1
        \end{bmatrix} ,\,
    \vec{b}^{A}_2 =
        \begin{bmatrix}
            -1\\
            1
        \end{bmatrix}
\end{align*}
Now consider vector $\vec{v}^{B}$ that is $[-1,\;2]^{T}$ in the coordinate system $B$ where $\vec{b}_{1}$ and $\vec{b}_{2}$ are orthonormal.
What is $\vec{v}$ in the coordinate system $A$?
\begin{align*}
    \vec{v}^{A} &= -1 \cdot \vec{b}^{A}_{1} + 2 \cdot \vec{b}^{A}_{2} \\
            &= -1 \cdot
                \begin{bmatrix}
                    2\\
                    1
                \end{bmatrix} +
               2 \cdot
                \begin{bmatrix}
                    -1\\
                    1
                \end{bmatrix} \\
            &=
                \underbrace{\begin{bmatrix}
                    2 & -1\\
                    1 & 2
                \end{bmatrix}}_{\bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]}
                \underbrace{\begin{bmatrix}
                    -1\\
                    2
                \end{bmatrix}}_{\vec{v}^{B}}
             =
                \begin{bmatrix}
                    -4\\
                    1
                \end{bmatrix}
\end{align*}
So the matrix $\bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]$ transforms vector notation from coordinate system $B$ to $A$.
And accordingly $\bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]^{-1}$ transforms vector notation from $A$ to $B$.
Now consider a matrix $\mat{R}^{A}$ with columns denoted in the coordinate system $A$.
What is this matrix denoted in $B$ ?be
With the previous deductions we can write
\begin{align*}
    \mat{R}^{B} \vec{v}^{B} = \bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]^{-1}
    \mat{R}^{A}
    \underbrace{\bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr] \vec{v}^{B}}_{\vec{v}^{A}}
\end{align*}
Thus
\begin{align*}
    \mat{R}^{B} =
    \bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]^{-1}
    \mat{R}^{A}
    \bigl[ \vec{b}^{A}_{1}\;\;\vec{b}^{A}_2 \bigr]
\end{align*}

* Eigenvectors and eigenvalues | Chapter 14, Essence of linear algebra [cite:@eigenvectors-eigenvalues-3blue1brown-YT]

Consider eigenvalue equation $\mat{A} \vec{v} = \lambda \vec{v}$.
So the eigenvectors $\vec{v}$ are vectors that are scaled by $\lambda$ when transformed by $\mat{A}$.
We can rewrite it as $(\mat{A} - \lambda\mat{I})\vec{v} = 0$.
So the matrix $(\mat{A} - \lambda\mat{I})$ maps a (non-zero) vector $\vec{v}$ to $0$.
According to previous deductions for determinants we know that $\det(\mat{A} - \lambda\mat{I})=0$ in order to do so.
Via this equation we can find eingenvalues $\lambda$.
And by inserting them into the eigenvalue equation $\mat{A} \vec{v} = \lambda \vec{v}$ we can find the eigenvectors.
We can change the representation of $\mat{A}$ to a eigenbasis representation.
Let $E$ be coordinate system with eigenvectors of $\mat{A}$ as orthonormal basis and $\vec{v}_{1}$ and $\vec{v}_{2}$ eigenvectors of $\mat{A}$ represented in the original coordinate system.
According to previous deductions about change of basis we can obtain representation of $\mat{A}$ in $E$ via
\begin{align*}
    \mat{A}^{E} =
    \bigl[ \vec{v}_{1}\;\;\vec{v}_2 \bigr]^{-1}
    \mat{A}
    \bigl[ \vec{v}_{1}\;\;\vec{v}_2 \bigr]
\end{align*}
Since $\mat{A}^{E}$ has eigenvectors as basis it just scales it's basis vectors by $\lambda_1$ and $\lambda_1$, where  $\lambda_1$ and $\lambda_1$ are the according eigenvalues of $\vec{v}_{1}$ and $\vec{v}_{2}$.
Thus $\mat{A}^{E}$ has to be diagonal matrix with $\lambda_1$ and $\lambda_1$ on its diagonal, since this is what a diagnoal matrix does.

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
