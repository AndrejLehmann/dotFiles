:PROPERTIES:
:ID:       899e6b61-1621-4ed4-b3b9-54e3df1739c8
:END:
#+title: C: function with no arguments
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:c:

#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* The right way to define a C function with no arguments [cite:@how-to-define-c-functions-with-no-arguments-YT]

#+begin_src C
int foo(){ return 1; }
int bar(void){ return 1; }
int main(int argc, char  **argv){
    int result = foo(12);
    int result = bar(12);
}
#+end_src

When src_c{foo(12)} is called the compiler produces a warning.
When src_c{bar(12)} is called the compiler produces an error.
This because strictly speaking src_c{f()} means that the function /can/ take 0 or more arguments.
src_c{f(void)} explicitly declares that the function should take 0 arguments.


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
