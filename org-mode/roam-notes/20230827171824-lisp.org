:PROPERTIES:
:ID:       5c552537-6a74-4a31-8a8f-354d7993bf15
:END:
#+title: lisp
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:lisp:AI:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Paradigms of Artificial Intelligence Programming - Peter Norvig
** Introduction to Lisp

/Many other languages make a distinction between statements and expressions./
/An expression, like =2 + 2=, has a value, but a statement, like =x = 2 + 2=, does not./
/Statements have effects, but they do not return values./
/In Lisp, there is no such distinction: every expression returns a value./
/It is true that some expressions have effects, but even those expressions also return values./

src_elisp{(append '(Pat Kim) '(Robin Sandy))}
/This expression appends together two lists of names./
/The unusual part is the quote mark =(')=, which serves to block the evaluation of the following expression, returning it literally./
/If we just had the expression =(Pat Kim)=, it would be evaluated by considering =Pat= as a function and applying it to the value of the expression =Kim=./
/This is not what we had in mind. The quote mark instructs Lisp to treat the list as a piece of data rather than as a function call./
/In Lisp, a single quote is used to mark the beginning of an expression. Since we always know how long a single expression is - either to the end of an atom or to the matching parenthesis of a list - we don't need an explicit punctuation mark to tell us where the expression ends./

Consider following examples
#+BEGIN_SRC elisp
'John ; => JOHN
'(John Q Public) ; => (JOHN Q PUBLIC)
'2 ; => 2
2 ; => 2
'(+ 2 2) ; => (+ 2 2)
(+ 2 2) 4
John ; => *Error: JOHN is not a bound variable*
(John Q Public) ; => *Error: JOHN is not a function*
#+END_SRC

/Note that ='2= evaluates to =2= because it is a quoted expression, and =2= evaluates to =2= because numbers evaluate to themselves./
/Same result, different reason./
/In contrast, ='John= evaluates to =John= because it is a quoted expression, but evaluating =John= leads to an error, because evaluating a symbol means getting the value of the symbol, and no value has been assigned to =John=./

/Symbols in Common Lisp are not case sensitive./
/By that I mean that the inputs =John=, =john=, and =jOhN= all refer to the same symbol, which is normally printed as =JOHN=./
/There is a wide variety of characters that are allowed in symbols./

** Special Forms

/In evaluating an expression like =(setf x (+ 1 2))=, we set the variable named by the symbol =x= to the value of =(+ 1 2)=, which is =3=./
/If =setf= were a normal function, we would evaluate both the symbol =x= and the expression =(+ 1 2)= and do something with these two values, which is not what we want at all./
/This is called a special form because it does something special: if it did not exist, it would be impossible to write a function that assigns a value to a variable./
/The term /special form is used confusingly to refer both to symbols. like =setf= and expressions that start with them, like =(setf x 3)=.The philosophy of Lisp is to provide a small number of special forms to do the things that could not otherwise be done, and then to expect the user to write everything else as functions./

/It turns out that the quote mark is just an abbreviation for another special form. The expression 'x is equivalent to =(quote x)=, a special form expression that evaluates to x./
/The special form operators used in this chapter are:/
|---------------+----------------------------------------------|
| =defun=         | define function                              |
| =defparameter=  | define special variable                      |
| =setf=          | set variable or field to new value           |
| =let=           | bind local variable(s)                       |
| =case=          | choose one of several alternatives           |
| =if=            | do one thing or another, depending on a test |
| =function (#')= | refer to a function                          |
| =quote (')=     | introduce constant data                      |
|---------------+----------------------------------------------|

/The symbol =nil= and the form =()= are completely synonymous; they are both representations of the empty list./

** Lists

#+BEGIN_SRC elisp
p ; => (JOHN Q PUBLIC)
(cons 'Mr p) ; => (MR JOHN Q PUBLIC)
(cons (first p) (rest p)) ; => (JOHN Q PUBLIC)
(setf town (list 'Anytown 'USA)) ; => (ANYTOWN USA)
(list p 'of town 'may 'have 'already 'won!) ; => ((JOHN Q PUBLIC) OF (ANYTOWN USA) MAY HAVE ALREADY WON!)
(append p '(of) town '(may have already won!)) ; => (JOHN Q PUBLIC OF ANYTOWN USA MAY HAVE ALREADY WON!)
p ; => (JOHN Q PUBLIC) ; was not modified
#+END_SRC


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
