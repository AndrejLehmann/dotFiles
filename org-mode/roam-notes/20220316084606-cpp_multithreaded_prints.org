:PROPERTIES:
:ID:       a3ab2b78-1a9c-45f8-af3b-5dcebb9e9481
:END:
#+title: Cpp: multithreaded prints
#+setupfile: ~/gems-backup/org-mode/general-latex-header.org
#+setupfile: ~/gems-backup/org-mode/math-latex-header.org
#+bibliography: references.bib
#+cite_export: csl ../citation-styles/ieee.csl
#+filetags: :programming:cpp:

* Prints [cite:@cpp-multithreaded-prints]

C++ Core Guideline SL.io.50: Avoid src_cpp{std::endl}.
src_cpp{std:endl} is writing src_cpp{'\n'} plus src_cpp{os.flush()}.
But flushing is expensive.
Usually output is buffered so flushing is used less frequently.
Use src_cpp{'\n'} instead.

For multithreaded prints (since multiple threads try to write to the same place at the same time and thus mangled lines will be written) do

#+begin_src cpp
#include <iostream>
#include <thread>
#include <fstream>
#include <syncstream> // C++20
//#include <mutex> // for C++11 through C++17

//std::mutex write_mutex; // for C++11 through C++17


void worker(int start, int stop, std::ostream &os) {
    for (int i = start; i < stop; ++i) {

//        * C++11 - C++17 solutions
//        Instead of using src_cpp{lock()} and src_cpp{unlock()} use src_cpp{lock_guard} for C++11 or src_cpp{scoped_lock} for C++17.
//        When src_cpp{mutex} is locked and an expeption is thrown, it won't unlock.
//        src_cpp{scoped_lock} can handle multiple locks.
//
//        const std::lock_guard<std::mutex> lock{write_mutex}; // C++11
//        const std::scoped_lock lock{write_mutex}; // C++17
//        os << "thread: " << std::this_thread::get_id() << "; work: " << i;
//        os << '\n';

//      * C++20 solution
        std::osyncstream out{os};
        out << "thread: " << std::this_thread::get_id() << "; work: " << i;
        out << '\n';
    }
}

void example(std::ostream &os) {
//  jthread is C++20 and it does not need src_cpp{join}
    std::jthread t1(worker, 10000, 20000, std::ref(os));
    std::jthread t2(worker, 20000, 30000, std::ref(os));

//    C++11 through C++17
//    std::thread t1(worker, 10000, 20000, std::ref(os));
//    std::thread t2(worker, 20000, 30000, std::ref(os));
//    t2.join();
//    t1.join();
}


int main() {
    std::ofstream file{"out.txt"};
    example(file);
    return 0;
}
#+end_src

But still src_cpp{mutex} lowers the performance, since treads are fighting for the src_cpp{mutex}.
Consider to implement own buffering, like writing 1000 lines into a local string stream and write to the file with src_cpp{mutex} every 1000 lines.
Or one can have a single thread for printing and other threads just submitting data to the printing thread through a thread save queue.

#+begin_export latex
    \clearpage
#+end_export

* References
#+print_bibliography:
