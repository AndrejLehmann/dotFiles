:properties:
:ID:       ae9680da-70bb-4844-9aa6-d840b17e516b
:end:
#+title: Schr"odinger <-> Heisenberg picture
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :theoretical-physics:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

\begin{align*}
   |\psi^{\rm S}_t\rangle = \hat{U}(t) \, |\psi^{\rm S}_0\rangle
\end{align*}

\begin{align*}
   \langle a \rangle &= \langle \psi^{\rm S}_t|\hat{A}^{\rm S}(t)|\psi^{\rm S}_t\rangle = \langle \psi^{\rm S}_t|\underbrace{\hat{U}(t)\hat{U}^{\dagger }(t)}_{1}\,\hat{A}^{\rm S}(t)\,\underbrace{\hat{U}(t)\hat{U}^{\dagger }(t)}_{1}\,|\psi^{\rm S}_t\rangle \\
   &= \langle \hat{U}^{\dagger }(t)\psi^{\rm S}_t|\hat{U}^{\dagger }(t)\,\hat{A}^{\rm S}(t)\,\hat{U}(t)\,|\hat{U}^{\dagger }(t)\psi^{\rm S}_t\rangle
   = \langle \psi^{\rm S}_0|\hat{U}^{\dagger }(t)\,\hat{A}^{\rm S}(t)\,\hat{U}(t)|\psi^{\rm S}_0\rangle \\ \\
   &= \langle \psi^{\rm H}|\hat{A}^{\rm H}(t)|\psi^{\rm H}\rangle
\end{align*}

So the operator \(\hat A^{\rm H}(t)\) in the Heisenberg picture is given by the operator \(\hat A^{\rm S} (t)\) in the Schr"odinger picture via

\begin{align*}
   \hat A^{\rm H}(t) = \hat U^{\dagger}(t)\,\hat A^{\rm S}(t)\,\hat U(t)
\end{align*}

In general the operator \(\hat A\) can be time dependent in the Heisenberg picture as well as in the Schr"odinger picture, e.g. for Hamiltonian with a time dependent potential.

\begin{align*}
   \frac{\mathrm{d}}{\mathrm{d}t} \hat A^{\rm H}(t) = {\partial \over \partial t}  \hat A^{\rm H}(t) +{i \over \hbar}[\hat H^{\rm H}(t) \, , \, \hat A^{\rm H}(t)]
\end{align*}

where \({\partial \over \partial t} \hat A^{\rm H}(t)\) is short for \(U^{\dagger}(t) \left( {\partial \over \partial t}  \hat A^{\rm S}(t) \right) U(t)\) .

If the Hamilton operator in the Schr"odinger picture \(\hat H^{\rm S}\) is not time dependent:

\begin{align*}
   \hat H^{\rm H}(t) = \hat H^{\rm S} \, .
\end{align*}
(correct? I think so since $\hat H^{\rm H}(t) = \hat U^{\dagger}(t)\,\hat H^{\rm S}\,\hat U(t) = \e ^{iHt} H^{\rm S} \e ^{-iHt} =  \e ^{iHt} \e ^{-iHt} H^{\rm S} = H^{\rm S}$)

The observable \(\hat A\) is conserved, if

\begin{align*}
   \frac{\mathrm{d}}{\mathrm{d}t} \hat A^{\rm H}(t)=0 \, .
\end{align*}

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
