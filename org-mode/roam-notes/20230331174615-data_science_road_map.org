:PROPERTIES:
:ID:       d1ff6e80-4a6b-4854-91f2-3fd2266fad3d
:END:
#+title: Data science road map
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :data-science:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* resources
** [[https://statquest.org/page/13/][StatQuest]]
** [[https://work.caltech.edu/telecourse.html][Learning from Data - Lecture with Exercises]]
*** Solutions to exercises
[[https://github.com/ppaquay/Learning-from-Data-Solutions][web]]
[[file:~/gems-backup/org-mode/data-science/Learning-from-Data-Solutions/][local]]
** [[https://www.youtube.com/watch?v=eTxyviU0Ddo][Everything you need to learn Data science for free]]
*** Highlights
[[https://work.caltech.edu/telecourse.html][Learning from data]]
*** Paid
**** Data science courses

1. [[https://bit.ly/3hcLjbY][Data Quest]] (my favourite)
2. [[https://bit.ly/39erDk8][Data Camp]]
3. [[https://bit.ly/3hbUHdd][365 Data Science]]

**** Python courses

1. [[https://bit.ly/2QXMpxJ][Exploratory Data Analysis with Python and Pandas]]
2. [[http://bit.ly/2OwUA09][Complete Python Programmer Bootcamp]]

**** Data Science Interview Preparation

[[https://bit.ly/30ul0nX][StrataScratch]]

*** Free
**** Python
***** Basics

[[https://lectures.quantecon.org/py/][Introduction to Python, The Scientific Libraries, Advanced Python Programming and the Pandas Section of Data and Empirics]]

Chapters 1-4 in this [[https://github.com/jakevdp/PythonDataScienceHandbook/blob/8a34a4f653bdbdc01415a94dc20d4e9b97438965/notebooks/Index.ipynb][Python Data Science Handbook]]

Then this [[https://pandas.pydata.org/pandas-docs/stable/getting_started/index.html#getting-started][tutorial Pandas]]

Here are some excellent [[https://github.com/wesm/pydata-book][pandas code examples]]

***** Practice python projects

https://github.com/practical-tutorials/project-based-learning#python
https://projecteuler.net

***** More python

Work through as many of the examples as you fancy in Chapters 6 and 7 [[https://scipython.com/book/][here]]

**** Math
***** Basics

****** Linear algebra

[[https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab][Essence of Linear Algebra]]
[[https://www.khanacademy.org/math/linear-algebra][Khan Academy]]
[[https://betterexplained.com/articles/linear-algebra-guide/][An Intuitive Guide to Linear Algebra]]
[[https://physics.bgu.ac.il/~gedalin/Teaching/Mater/am.pdf][Introduction to Methods of Applied Mathematics]]
[[http://www.physics.miami.edu/~nearing/mathmethods/mathematical_methods-one.pdf][Mathematical Tools for Physics]]
[[https://www.math.ubc.ca/~carrell/NB.pdf][Fundamentals of linear algebra]]
[[https://math.byu.edu/~klkuttle/EssentialLinearAlgebra.pdf][Linear Algebra And Analysis]]

****** Calculus

[[https://www.youtube.com/watch?v=WUvTyaaNkzM&list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr][Essence of Calculus]]
[[https://www.khanacademy.org/math/calculus-1][khanacademy - Calculus - 1]]
[[https://www.khanacademy.org/math/calculus-2][khanacademy - Calculus - 2]]
[[https://www.khanacademy.org/math/multivariable-calculus][Multivariable calculus]]

***** Data exploration

[[https://github.com/StephenElston/ExploringDataWithPython/][Exploring data with python]]

[[https://www.kaggle.com/c/titanic#description][kaggle - titanic]]

***** Probability and statistics

[[https://www.khanacademy.org/math/statistics-probability][khanacademy]]
[[https://greenteapress.com/thinkstats/thinkstats.pdf][Think Stats: Probability and Statistics for Programmers]]
[[https://bookboon.com/en/applied-statistics-ebook][Applied statistics]]
[[http://www.wzchen.com/probability-cheatsheet/][Introduction to probability]]

***** Statistical learning

[[https://www.statlearning.com][An Introduction to Statistical Learning (Essential)]]
[[https://work.caltech.edu/telecourse.html][Learning from data]]
[[https://web.stanford.edu/~hastie/ElemStatLearn/][Elements of Statistical Learning (Extremely useful)]]

**** Python and data science

Chapter 5 in  [[https://github.com/jakevdp/PythonDataScienceHandbook/blob/8a34a4f653bdbdc01415a94dc20d4e9b97438965/notebooks/Index.ipynb][Python Data Science Handbook]]

[[https://scikit-learn.org/stable/tutorial/index.html][scikit-learn Tutorials]]

**** Data structures and algorithms in python

[[https://www.udacity.com/course/data-structures-and-algorithms-in-python--ud513][Intro to Data Structures and Algorithms by Grow With Google]]

[[https://runestone.academy/runestone/books/published/pythonds/index.html][Problem Solving with Algorithms and Data Structures using Python]]

**** Tensorflow

https://developers.google.com/machine-learning/crash-course/

**** SQL

https://www.khanacademy.org/computing/computer-programming/sql

**** GIT and version control

https://git-scm.com/book/en/v2

**** Take this Harvard class

https://cs109.github.io/2015/index.html

**** R

[[https://www.r-bloggers.com/2015/12/how-to-learn-r-2/][Tutorials for learning R]]

**** Supplementary material

[[https://docs.python.org/3/tutorial/index.html][The Python Tutorial]]
[[https://www.reddit.com/r/Python/][Python reddit]]
[[https://www.reddit.com/r/datascience/][Data science reddit]]
[[https://stackoverflow.com/questions/tagged/python][Stackoverflow python]]
[[https://datascience.stackexchange.com/][Stackexchange data science]]
[[https://jupyter.org/][jupyter]]

[[http://www.openbookproject.net/thinkcs/python/english3e/][How to think like a computer scientist]]

[[https://onextrapixel.com/start-jekyll-blog-github-pages-free/][Write a blog]]

SLACK GROUPS: https://kagglenoobs.herokuapp.com/ https://datadiscourse.herokuapp.com/

* projects

[[https://www.kaggle.com/datasets][kaggle - open data sets & community analysis]]
[[https://www.youtube.com/watch?v=eAgv2-XBIac][Ideas - YouTube]]

* important skills
- curious
  + Me: try to understand how the world works
    - filter noise
- argumentative/communicative
  + explain your insides as a story
    - Me: story telling in movies

#+begin_export latex
  \clearpage
#+end_export


* References
#+print_bibliography:
