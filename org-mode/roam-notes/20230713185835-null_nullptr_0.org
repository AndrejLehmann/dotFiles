:PROPERTIES:
:ID:       a98d8026-6fb9-4b69-a04f-fb7ec1378e46
:END:
#+title: NULL, nullptr, 0
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:C:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* =NULL=, =nullptr=, 0 [cite:@nullptr-stackoverflow]

Imagine you have the following two function declarations:

#+begin_src C
void func(int n);
void func(char *s);
func( NULL ); // guess which function gets called?
#+end_src

Although it looks like the second function will be called - you are, after all, passing in what seems to be a pointer - it's really the first function that will be called!
The trouble is that because =NULL= is 0, and 0 is an integer, the first version of func will be called instead.
This is the kind of thing that, yes, doesn't happen all the time, but when it does happen, is extremely frustrating and confusing.
If you didn't know the details of what is going on, it might well look like a compiler bug. A language feature that looks like a compiler bug is, well, not something you want.

Enter =nullptr=.
In C++11, =nullptr= is a new keyword that can (and should!) be used to represent =NULL= pointers; in other words, wherever you were writing =NULL= before, you should use =nullptr= instead.
It's no more clear to you, the programmer, (everyone knows what =NULL= means), but it's more explicit to the compiler, which will no longer see 0s everywhere being used to have special meaning when used as a pointer.

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
