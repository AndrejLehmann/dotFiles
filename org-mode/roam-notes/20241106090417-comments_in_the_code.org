:PROPERTIES:
:ID:       e577fdd9-226b-4b85-a95c-183a5d0342d0
:END:
#+title: Comments in the code
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

- well readable code without comments > bad readable code with correct comments > bad readable code without comments
- What the code does and most importantly why
- Write the general concept, then you don't have to change the comment if you refactor
- Write in first-person perspective. Passive form has the disadvantage that you often do not know who is acting. It should always be clearly formulated who is doing something: the described code, the calling code, a specific object or the user.
- You need comments if:
  + the code exhibits unexpected or contraintuitive behavior
  + no time to rewrite
  + you have already tried an obvious solution and have failed - comment out and write why
  + the solution could seem unnecessarily complicated to the viewer but is actually smart
  + you already know or suspect that you are doing something wrong or you don't test the code - use TODO
  + complicated control flow
  + code snippets from somebody else - note the source. (If it's a lot of code, put it into it's own file and don't change it.)
- Any function that is not entirely trivial should be equipped with one or two descriptive sentences at the beginning. If you need more than these sentences, something is wrong with the function
- Keywords:
  + TODO: not broken, but can be improved (write why)
  + FIXME: broken
  + XXX: highly dangerous implementation, temporarely commented out


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
