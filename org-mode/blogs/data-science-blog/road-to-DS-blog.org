#+TITLE: Road to Data Science
#+AUTHOR:
#+OPTIONS: num:nil
#+OPTIONS: tex:dvipng
#+EXPORT_FILE_NAME: /Users/andrejthealien/gems-backup/org-mode/data-science/road-to-DS-blog/test-blog.html
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup

* The End of the Beginning

Like making up your mind, having your things packed and a route planed is the end of beginning a journey,
this blog is the end of the questions "Should I learn data science?" and "How should I do that?" and it is the start of my learning journey.

** Who am I?

I am a Ph.D. student in a research group of condensed matter theory in Germany.
After studying physics for about 9 years and while still working on my Ph.D. thesis I decided that I will some day put theoretical physics to my hobbies and start now to learn data science as my long term goal.

** Why Data Science?

Since many years it is a lot of fun for me to learn how the world works.
So in my spare time I've been watching channels on YouTube like Vox and Veritasium or lectures like "Justice: What's The Right Thing To Do?" and "Behavioral Evolution".
And some day I watch a Video by Vox named 'Why underdogs do better in hockey than basketball'.
This was the first time I heard of some thing that, as I later found out, is data science.
In that video there was a guy who managed to find out what sports involve more luck than others.
But not only that he even managed to put a number on luck and skill for every sport.
And he did that 'just' by using data and statistical methods.
I remember thinking to myself that I have no idea how to solve such problem by physical, mathematical or computational thinking.
I seemed to me that there is a way of trying to analytically (not philosophically) understand the world that I didn't know about.
In condensed matter physics you have an understanding of the basic rules of how things work.
You can mathematically them pin down.
It is just computationally not feasible to compute them exactly.
So you make a model that captures the most important features and try to compute approximately that.
But what if you don't even have that.
All you have is data (and in modern times you can have a lot of it).
Can you still gain some insides, understanding or predictions?
That question rested for quite a while somewhere in the back of my mind
but finally it led me to the decision to learn data science.

**  Why this blog?

I decided to share my experiences publicly for two reasons.
1. practice on story telling
2. maybe it will be of some use to somebody who also will make a similar journey.

** Test codeblock

#+begin_src python
print( "TEST" )
#+end_src

** Test Latex

Inline: $2/5$ text \(e^{-i 2 \pi} = 1\)

Equation:
\begin{align*}
    \frac{2}{5}
\end{align*}

Maybe latex images have to be saved in a specific directory likes ./img .
