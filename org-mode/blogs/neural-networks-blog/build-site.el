;;; Sources: [[https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/]], [[https://www.youtube.com/watch?v=AfkrzFodoNw&t=183s]]

;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.

;;; Code:

;; Set the package installation directory so that packages aren't stored in the ~/.emacs.d/elpa path
;; and separated from personal packages.
;; Reliably installs dependencies e.g. for use from a remote computer.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages")) ;; Separate directory for packages
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)

;; Refresh the package archive so that htmlize can be found
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'use-package) ;; is missing in the original source
(package-install 'htmlize)

(use-package simple-httpd
  :ensure t)

;; Load the publishing system
(require 'ox-publish)

;; Customize the HTML output
(setq org-html-validation-link nil            ;; Don't show validation link
      org-html-head-include-scripts nil       ;; Use our own scripts
      org-html-head-include-default-style nil ;; Use our own styles
      org-html-head "<link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.min.css\" />") ;; Just as a fall back. It will be overwritten locally in =index.org= by =#+SETUPFILE:=

;; Define the publishing project
(setq org-publish-project-alist
      (list ;; list with projects
       (list "org-site:main"  ;; list with configurations for the specific project
             :recursive t     ;; consider also the files in subdirectories
             :base-directory "./content"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :with-author nil           ;; Don't include author name
             :with-creator t            ;; Include Emacs and Org versions in footer
             :with-toc nil              ;; Include a table of contents
             :section-numbers nil       ;; Don't include section numbers
             :time-stamp-file nil)))    ;; Don't include time stamp in file

;; Dont use cached files but regenerate everything from scratch.
;; You need to regenerate all files when you change the configuration.
;; It's handy to just allways regenerate.
(org-publish-all t)

(message "Build complete!")

;;; build-site.el ends here
