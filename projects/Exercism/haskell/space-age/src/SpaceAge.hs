--module SpaceAge (Planet(..), ageOn) where
module Main where

data Planet
  = Mercury
  | Venus
  | Earth
  | Mars
  | Jupiter
  | Saturn
  | Uranus
  | Neptune

{-
Given an age in seconds, calculate how old someone would be on:

   - Mercury: orbital period 0.2408467 Earth years
   - Venus: orbital period 0.61519726 Earth years
   - Earth: orbital period 1.0 Earth years, 365.25 Earth days, or 31557600 seconds
   - Mars: orbital period 1.8808158 Earth years
   - Jupiter: orbital period 11.862615 Earth years
   - Saturn: orbital period 29.447498 Earth years
   - Uranus: orbital period 84.016846 Earth years
   - Neptune: orbital period 164.79132 Earth years

So if you were told someone were 1,000,000,000 seconds old, you should
be able to say that they're 31.69 Earth-years old.
-}

earthYearsFromSeconds :: Float -> Float
earthYearsFromSeconds seconds = seconds / 60.0 / 60.0 / 24.0 / 365.25

ageOn :: Planet -> Float -> Float
ageOn planet earthSeconds =
  earthYears / case planet of
    Mercury -> 0.2408467
    Venus -> 0.61519726
    Earth -> 1.0
    Mars -> 1.8808158
    Jupiter -> 11.862615
    Saturn -> 29.447498
    Uranus -> 84.016846
    Neptune -> 164.79132
  where
    earthYears = earthYearsFromSeconds earthSeconds

main :: IO ()
main = print (ageOn Venus 189839836)
