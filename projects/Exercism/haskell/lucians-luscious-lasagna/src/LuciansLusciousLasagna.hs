module LuciansLusciousLasagna (elapsedTimeInMinutes, expectedMinutesInOven, preparationTimeInMinutes) where

expectedMinutesInOven = 40

preparationTimeInMinutes numOfLayers = 2*numOfLayers

elapsedTimeInMinutes numOfLayers minutesInOven = preparationTimeInMinutes numOfLayers + minutesInOven
